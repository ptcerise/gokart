$(document).ready( function () {
    $('#table-pilot').DataTable();

    $('#table-championship').DataTable({
    	"order": [[ 4, "desc" ]],
    	"bSort" : false
    });

    $('#table_racepro').DataTable();
    $('#table_racecasual').DataTable();
} );

// -------------------------- KART ------------------------------

$(".edit-kart").click(function(){
	var id = $(this).data("id");
	var img = $(this).data("img");
	var name = $(this).parents("div.per-kart").find(".kart-name h4").text();
	var spec0 = $(this).data("spec0");
	var spec1 = $(this).data("spec1");
	var spec2 = $(this).data("spec2");
	var spec3 = $(this).data("spec3");
	var spec4 = $(this).data("spec4");
	var spec5 = $(this).data("spec5");
	var spec6 = $(this).data("spec6");

	$("#update_kart").find("input[name='kart_id']").val(id);
	$("#update_kart").find("#imgPrevKart").attr("src", img);
	$("#update_kart").find("input[name='kart_name']").val(name);
	$("#update_kart").find("input[name='spec_requirement']").val(spec0);
	$("#update_kart").find("input[name='spec_features']").val(spec1);
	$("#update_kart").find("input[name='spec_frame']").val(spec2);
	$("#update_kart").find("input[name='spec_engine_volume']").val(spec3);
	$("#update_kart").find("input[name='spec_engine']").val(spec4);
	$("#update_kart").find("input[name='spec_power']").val(spec5);
	$("#update_kart").find("input[name='spec_max_speed']").val(spec6);
});

function readURLKart(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imgPrevKart').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInpKart").change(function(){
    readURLKart(this);
});

// --------------------------------------------------------------

//------------------------------TRACK----------------------------


$("input[name='track_img']").change(function(){
	var parent = $(this).parents("div.tracks");
    readURLTrack(this);

    function readURLTrack(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            parent.find('img.img-track').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
});

//--------------------------------------------------------------

//------------------------------TRACK----------------------------


$("input#gallery_upl").change(function(){
	var parent = $(this).parents("div.panel-body");
    readURLGallery(this);

    function readURLGallery(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            parent.find('img#imgPrev').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
});

//--------------------------------------------------------------


//------------------------------MEMBER/PILOT----------------------------

$(".edit-pilot").click(function(){
	var id = $(this).data("id");
	var img = $(this).data("img");
	var name = $(this).parents("tr").find("td.pilot-name").text();
	var email = $(this).parents("tr").find("td.pilot-email").text();
	var phone = $(this).parents("tr").find("td.pilot-phone").text();
	var pinbb = $(this).parents("tr").find("td.pilot-pinbb").text();
	var address = $(this).parents("tr").find("td.pilot-address").text();
	var postcode = $(this).parents("tr").find("td.pilot-postcode").text();
	var city = $(this).parents("tr").find("td.pilot-city").text();

	$("#modal_pilot").find("input[type='password']").removeAttr("required");

	$("#modal_pilot").find("img#imgPrev").attr("src", img);
	$("#modal_pilot").find("input[name='pilot_id']").val(id);
	$("#modal_pilot").find("input[name='user_name']").val(name);
	$("#modal_pilot").find("input[name='user_email']").val(email);
	$("#modal_pilot").find("input[name='profile_phone']").val(phone);
	$("#modal_pilot").find("input[name='profile_pin_bb']").val(pinbb);
	$("#modal_pilot").find("input[name='profile_address']").val(address);
	$("#modal_pilot").find("input[name='profile_postcode']").val(postcode);
	$("#modal_pilot").find("input[name='profile_city']").val(city);
});

$(".btn-add-pilot").click(function(){
	$("#modal_pilot").find("img#imgPrev").attr("src", js_base_url('assets/img/pilot-default.jpg'));
	$("#modal_pilot").find("input").val("");
	$("#modal_pilot").find("input[type='password']").attr("required","");
});


$("#imgInp").change(function(){
    readURLTrack(this);

    function readURLTrack(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imgPrev').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
});

//--------------------------------------------------------------

$("a[title='Add Event']").click(function(){
	$("#modal_event").find("input").val("");
});

$("#show_pass").click(function(){
	if($("#show_pass").is(":checked")){
		$("input[name='user_pass']").attr("type","text");
	}else{
		$("input[name='user_pass']").attr("type","password");
	}
});


//-------------GALLERY---------------
$("a.edit-gallery").click(function(){
	var id = $(this).data("id");
	var desc_en = $(this).parents("tr").find("td.content_en").text();
	var desc_in = $(this).parents("tr").find("td.content_in").text();

	$("#update_gallery").find("input[name='img_id']").val(id);
	$("#update_gallery").find("input[name='desc_en']").val(desc_en);
	$("#update_gallery").find("input[name='desc_in']").val(desc_in);
});

//SUBMIT BUTTON WITH VALIDATION
$("button#btn_save_pilot").click(function(){
	var pwd1 = $("#modal_pilot").find("input[name='user_pass']").val();
	var pwd2 = $("#modal_pilot").find("input[name='user_pass_confirm']").val();
	$("#modal_pilot").find("button#btn_save_pilot").attr("type","submit");

	if(pwd2 != pwd1){
		$("#modal_pilot").find("input[name='user_pass_confirm']").css("border","solid 1px #ff0000");
		alert("Password confirmation wrong !");
		$("#modal_pilot").find("input[name='user_pass_confirm']").focus();
		$("#modal_pilot").find("button#btn_save_pilot").attr("type","button");
	}else{
		$("form#form_save_pilot").submit();
	}
});

tinymce.init({
    selector:'textarea',
    plugins: "link",
    menubar: "none",
    default_link_target: "_blank",
    inline_styles : false,
});
//$('textarea').wysihtml5({
//    toolbar: {
//	    "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
//	    "emphasis": true, //Italics, bold, etc. Default true
//	    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
//	    "html": false, //Button which allows you to edit the generated HTML. Default false
//	    "link": true, //Button to insert a link. Default true
//	    "image": false, //Button to insert an image. Default true,
//	    "color": false, //Button to change color of font  
//	    "blockquote": false, //Blockquote  
//	    //"size": <buttonsize> //default: none, other options are xs, sm, lg
//	  }
//});

//DATE PICKER
$(function(){
   $(".datepicker").datepicker({
   		dateFormat: "yyyy/mm/dd",
   });
});

$('a.edit-event').click(function(){
    var id = $(this).data("id");
    var event_nm = $(this).parents("tr").find("td.event-name").text();
    var event_dt = $(this).parents("tr").find("td.event-date").text();

    $("#modal_event").find("input[name='event_id']").val(id);
    $("#modal_event").find("input[name='event_name']").val(event_nm);
    $("#modal_event").find("input[name='event_date']").val(event_dt);
});

$("#btn_inp_racer").click(function(){
	$("tr.to-copy:first").siblings().remove();
	var show = $("#racer_count").val();

	for(i=0; i<show-1; i++){
		$("tr.to-copy:first").clone().appendTo("#entry");
		$("tr.to-copy:last").find("input").val("");
	}

	$("input[type='radio']").click(function(){
		var id = $(this).parents("tr").find(".select-pilot option:selected").val();
		$(this).val(id);
	});

	$("input[type='number']").val(0);
});

$('a.edit-champ').click(function(){
	$("#modal_champ").css('z-index','1200');
    var id = $(this).data("id");
    var nm = $(this).parents("tr").find("td.champ-pilot").text();
    var race = $(this).parents("tr").find("td.champ-race-count").text();
    var point = $(this).parents("tr").find("td.champ-point").text();
    var podium = $(this).parents("tr").find("td.champ-podium").text();
    var year = $(this).parents("tr").find("td.champ-year").text();

    $("#modal_champ").find("input[name='champ_id']").val(id);
    $("#modal_champ").find("select[name='champ_pilot']").val(nm);
    $("#modal_champ").find("input[name='champ_race_count']").val(race);
    $("#modal_champ").find("input[name='champ_point']").val(point);
    $("#modal_champ").find("input[name='champ_podium']").val(podium);
    $("#modal_champ").find(".modal-title").html("Championship <strong>" + year + "</strong>");
});

$('a.edit-race').click(function(){
	$("#modal_race").css('z-index','1200');
	var id = $(this).data("id");
	var nm = $(this).parents("tr").find("td.race-pilot").text();
	var point = $(this).parents("tr").find("td.race-point").text();
	point = parseInt(point);
	//alert(point);
	var rt_ms = $(this).parents("tr").find("td.race-time span.msec").text();
	rt_ms = parseInt(rt_ms);
	var rt_s = $(this).parents("tr").find("td.race-time span.sec").text();
	rt_s = parseInt(rt_s);
	var bl_ms = $(this).parents("tr").find("td.race-best-lap span.msec").text();
	bl_ms = parseInt(bl_ms);
	var bl_s = $(this).parents("tr").find("td.race-best-lap span.sec").text();
	bl_s = parseInt(bl_s);
	var info = $(this).data("info");
	var type = $(this).data("type");

	//$("#modal_race").find(".modal-body").html(id + " | " + nm + " | " + race_time + " | " + best_lap);
	$("#modal_race").find("input[name='race_id']").val(id);
	$("#modal_race").find("#race_pilot").text(nm);
	$("#modal_race").find("input[name='race_point']").val(point);
	$("#modal_race").find("input[name='race_time_ms']").val(rt_ms);
	$("#modal_race").find("input[name='race_time_s']").val(rt_s);
	$("#modal_race").find("input[name='race_best_lap_ms']").val(bl_ms);
	$("#modal_race").find("input[name='race_best_lap_s']").val(bl_s);
	$("#modal_race").find("select[name='race_info']").val(info);
	$("#modal_race").find("input[name='race_type']").val(type);
});

$('.edit-user').click(function(){
	var id = $(this).data("userid");
	var usrnm = $(this).parents("tr").find("td.user_name").text();
	var usrfnm = $(this).parents("tr").find("td.user_fname").text();
	var email = $(this).parents("tr").find("td.user_email").text();
	var level_n = $(this).parents("tr").find("td.user_level").text();

	if(level_n == "Admin"){
		var level = 1;
	}else{
		var level = 2;
	}

	$("#modal_user").find("input[type='password']").removeAttr("required");

	$("#modal_user").find("input[name='user_id']").val(id);
	$("#modal_user").find("input[name='user_name']").val(usrnm);
	$("#modal_user").find("input[name='user_full_name']").val(usrfnm);
	$("#modal_user").find("input[name='user_email']").val(email);
	$("#modal_user").find("select[name='user_level']").val(level);
});

$('.btn-close-modal').click(function(){
	$("#modal_user").find("input[name='user_id']").val("");
	$("#modal_user").find("input[name='user_name']").val("");
	$("#modal_user").find("input[name='user_full_name']").val("");
	$("#modal_user").find("input[name='user_email']").val("");
	$("#modal_user").find("select[name='user_level']").val("");
	$("#modal_user").find("input[type='password']").attr("required","required");
});