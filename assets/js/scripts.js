$(function() {

    var $body = $(document);
    $body.bind('scroll', function() {
        // "Disable" the horizontal scroll.
        if ($body.scrollLeft() !== 0) {
            $body.scrollLeft(0);
        }
    });

}); 

$(document).ready(function(){$(".alert").addClass("in").fadeOut(4500);

/* swap open/close side menu icons */
$('[data-toggle=collapse]').click(function(){
  	// toggle icon
  	$(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");
});
});


/* FOR SMOOTH SCROLL */
$(function() {
  $('a[href$="_anchor"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 750);
        return false;
      }
    }
  });

});

/* FOOTER */
var h = 0;
  $('.footer-border').each(function() {
      if ($(this).height()>h) {
          h= $(this).height();
      };
  });
  $('.footer-border').each(function() {
      $(this).css('height', h+'px');
  });


/* INPUT ANIMATION */
$(window).load(function(){

  $(".input-effect input").focusout(function(){
    if($(this).val() != ""){
      $(this).addClass("has-content");
    }else{
    $(this).removeClass("has-content");
    }
  });

});

/* THE VIDEO */
function play_video(){
  /* Move the video from bg to popup */
  $('#vid-popup').append( $('#vid-background>source') );
  var vid_popup = document.getElementById('vid-popup');
  var vid_bg = document.getElementById('vid-background');
  vid_popup.play();
  if (vid_popup.requestFullscreen) {
      vid_popup.requestFullscreen();
    } else if (vid_popup.mozRequestFullScreen) {
      vid_popup.mozRequestFullScreen();
    } else if (vid_popup.webkitRequestFullscreen) {
      vid_popup.webkitRequestFullscreen();
    }
  vid_bg.pause();
}

function stop_video(){
  /* Return the video from popup to bg */
  $('#vid-background').append( $('#vid-popup>source') );

  var vid_popup = document.getElementById('vid-popup');
  var vid_bg = document.getElementById('vid-background');
  vid_popup.pause();
  vid_bg.play();
}

$(document).keyup(function(e) {
  if (e.keyCode === 27) $('#close-vid-btn').click();   // esc
});



/* PROFILE */

$("span.cancel_and_save").hide();

$(document).on("click", "a.edit", function(){
  $("span.edit_btn").hide();
  $("span.cancel_and_save").show();
  $("input").removeProp("readonly");
  $("#form_info").find("input:first").focus();
  $("#form_info").find("label").css("color", "#00a14c");
});

/* revert changes on cancel */
$(function() {
    //put all defaults in a data attribute
    $("#form_info").find("input").each(function(curIdx, curO) {
        $(curO).attr('value', $(curO).val());
    });
    
    $("a.cancel_edit").click(function() {
       $("#form_info").find("input").each(function(curIdx, curO) {
           $(curO).val($(curO).attr("value"));
       });

       $("span.edit_btn").show();
       $("span.cancel_and_save").hide();
       $("#form_info").find("input").attr("readonly","");
       $("#form_info").find("label").css("color", "rgba(255, 255, 255, 0.6)");
    });
});

$(function(){
  $('a.save_info').click(function(){
    $("#form_info").submit();
  });
});

/* HOME KARTS-DETAIL */

function view_detail($id){
    $($id).removeClass("animated fadeOutDown");
    $($id).addClass("animated fadeInUp");
    $($id).css("visibility", "visible");
}

function close_detail($id){
    $($id).addClass("animated fadeOutDown");
}

$(document).ready(function(){

  //ON LANG INDO
  var lang = $("#langflag img").attr("alt");
  if(lang == "EN"){
    $("ul.nav-bottom li").css("margin-right","4%");
  }

});

//MEMBER LOGIN
$(document).ready(function() {
    $("#signin_alert").hide();
    $("#signin_btn").click(function(event) {
        event.preventDefault();
        //This var is get val from input form
        var e = $("input[name='email']").val();
        var p = $("input[name='password']").val();
        jQuery.ajax({
            type: "POST",
            url: js_base_url('sign_in/auth'),
            dataType: 'json',
            //this data is sent to controller, it's like array.
            data: {email: e, password: p },
            success: function(show) {
                if (show.result == 1){
                    window.location.replace(js_base_url('profile'));
                }else{
                    $("#signin_alert").show();
                }
            }
        });
    });
});

//EVENTS SHOWMORE
$(document).ready(function(){
  var total_tr =  $(".next-events").find("table tr").size();
  if(total_tr < 6){
    $(".showmore-event").hide();
    $(".next-events").css("height","500px")
    var max_show = total_tr;
  }else{
    max_show = 4;
  }

  $(".next-events").find("table tr:lt("+ max_show +")").show();
  $(".next-events").find("table tr:gt("+ max_show +")").hide();

  $(".showmore-event").click(function(){
    max_show = max_show+5;
    if(total_tr < max_show){
      $(".showmore-event").hide();
      $(".next-events").css("height","500px");
    }
    $(".next-events").find("table tr:lt("+ max_show +")").show();
    $(".next-events").find("table tr:gt("+ max_show +")").hide();
  });
});

//CHAMPIONSHIP SHOWMORE
$(document).ready(function(){
  var total_tr =  $(".champ-tbl").find("table tr").size();
  if(total_tr < 8){
    $(".showmore-champ").hide();
    var max_show = total_tr;
  }else{
    max_show = 7;
  }
  $(".champ-tbl").find("table tr:lt("+ max_show +")").show();
  $(".champ-tbl").find("table tr:gt("+ max_show +")").hide();

  $(".showmore-champ").click(function(){
    max_show = max_show+4;
    if(total_tr < max_show){
      $(".showmore-champ").hide();
    }
    $(".champ-tbl").find("table tr:lt("+ max_show +")").show();
    $(".champ-tbl").find("table tr:gt("+ max_show +")").hide();
  });

});

$(document).ready(function(){
  $(".last-race").find("p.race-date:first").siblings().hide();

  var d = $(".pro").find("p.race-date:visible").data("date");
  $(".pro").find("table[data-date='"+ d +"']").siblings().hide();
  $(".pro").find("table[data-date='"+ d +"'] tr:gt(4)").hide();
  var rows = $(".pro").find("table[data-date='"+ d +"'] tr").size();
  if(rows <= 5){
    $(".pro").find("a.last-showmore").hide();
  }else{
    $(".pro").find("a.last-showmore").show();
  }

  var d2 = $(".casual").find("p.race-date:visible").data("date");
  $(".casual").find("table[data-date='"+ d2 +"']").siblings().hide();
  $(".casual").find("table[data-date='"+ d2 +"'] tr:gt(4)").hide();
  var rows = $(".casual").find("table[data-date='"+ d2 +"'] tr").size();
  if(rows <= 5){
    $(".casual").find("a.last-showmore").hide();
  }else{
    $(".casual").find("a.last-showmore").show();
  }

  $("a.prev").click(function(event){
    event.preventDefault();
    $(this).parents("div.last-race").find("p.race-date:visible").next().show().siblings().hide();
    var race_type = $(this).data("type");
    var race_date = $(this).parents("div.last-race").find("p.race-date:visible").data("date");
    $(this).parents('.last-race').find("table[data-date='"+ race_date +"']").show().siblings().hide();
    $(this).parents(".last-race").find("table[data-date='"+ race_date +"'] tr:gt(4)").hide();
    var count_tr = $(this).parents(".last-race").find("table[data-date='"+ race_date +"'] tr:hidden").size();

    if(count_tr > 0){
      $(this).parents(".last-race").find("a.last-showmore").show();
    }
    //alert(race_type + " | " + race_date);
  
  });

  $("a.next").click(function(event){
    event.preventDefault();
    $(this).parents("div.last-race").find("p.race-date:visible").prev().show().siblings().hide();
    var race_type = $(this).data("type");
    var race_date = $(this).parents("div.last-race").find("p.race-date:visible").data("date");
    $(this).parents('.last-race').find("table[data-date='"+ race_date +"']").show().siblings().hide();
    $(this).parents(".last-race").find("table[data-date='"+ race_date +"'] tr:gt(4)").hide();
    var count_tr = $(this).parents(".last-race").find("table[data-date='"+ race_date +"'] tr:hidden").size();

    if(count_tr > 0){
      $(this).parents(".last-race").find("a.last-showmore").show();
    }
    //alert(race_type + " | " + race_date);

  });

  $("a.last-showmore").click(function(){
    var show = $(this).parents("article").find("table:visible tr:visible").size();
    $(this).parents("article").find("table:visible tr:hidden").show();
    $(this).hide();
  });

  //var last_height = $(".last-race").height();
  //alert(last_height);

});

$(document).ready(function(){
    $(".col-effect textarea").val("");
    $(".input-effect textarea").focusout(function(){
      if($(this).val() != ""){
        $(this).addClass("has-content");
      }else{
      $(this).removeClass("has-content");
      }
    });

    $(".input-effect select").focusout(function(){
      if($(this).val() == "No category"){
        $(this).removeClass("has-content");
      }else{
      $(this).addClass("has-content");
      }
    });
});

//SEARCH
$(document).ready(function(){
  $("#search_result").find("div.panel-pilot").hide();

  $("input[name='search_friend']").change(function(){
    var keyword = $(this).val().toLowerCase();
    var check = $("#search_result").find("div[data-name*='"+keyword+"']").size();
    
    if(check == 0){
      $("#search_result").find("div.panel-pilot").hide();
      $("#search_result").find(".panel").prepend("<div class='panel-body panel-pilot no-result text-center'><i class='fa fa-search' aria-hidden='true'></i><br>NO RESULT !</div>");
    }else{
      $("#search_result").find("div.panel-pilot").hide();
      $("#search_result").find("div[data-name*='"+keyword+"']").show();
    }

    if(keyword == ""){
      $("#search_result").find("div.panel-pilot").hide();
    }
  });
});


$(document).ready(function(){
  var main = $(".my-race");
  var date = main.find(".race-date").data("date");
  var mine = main.data("id");
  main.find("tr[data-id='"+mine+"']").css("font-weight","bold");

  main.find("tbody[data-date='"+date+"']").show().siblings().hide();
  var get_index = main.find("tbody:visible tr[data-id='"+mine+"']").index();

  if(get_index == 3 || get_index < 3){
    main.find("tbody:visible tr:nth-child(4)").nextAll().hide();
  }else{
    main.find("tbody:visible tr:gt("+ get_index +")").hide();
    main.find("tbody:visible tr:nth-child("+ get_index +")").hide().prevUntil("tr:nth-child(3)").hide();
    main.find("tr[data-id='"+mine+"']").css("border-top","solid 2px #00a14c");
  }
  main.find("tbody.bottom tr").show();

  main.find("a.nav").click(function(){
    var d = $(".my-race").find(".race-date:visible").data("date");
    main.find("tbody[data-date='"+d+"']").show().siblings().hide();
    var get_index = main.find("tbody:visible tr[data-id='"+mine+"']").index();
    main.find(".panel-body:nth-child(2)").css("height","245px");

    if(get_index == 3 || get_index < 3){
    main.find("tbody:visible tr:nth-child(4)").nextAll().hide();
    }else{
      main.find("tbody:visible tr:gt("+ get_index +")").hide();
      main.find("tbody:visible tr:nth-child("+ get_index +")").hide().prevUntil("tr:nth-child(3)").hide();
      main.find("tr[data-id='"+mine+"']").css("border-top","solid 2px #00a14c");
    }

    var count_tr = main.find("tbody:visible tr:hidden").size();

    if(count_tr > 0){
      main.find("a.last-showmore").show();
    }

    main.find("tbody.bottom tr").show();
  });

  main.find("a.last-showmore").click(function(){
    var show = main.find("table:visible tr:visible").size();
    main.find("table:visible tr").show();
    $(this).hide();
    $(this).parents(".my-race").find(".panel-body:nth-child(2)").css("height","285px");
    main.find("tr[data-id='"+mine+"']").css("border-top","none");
  });
});

$('.divider button.btn.green').css('z-index','2');