<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');   
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('gokart_frontend',$fe_lang);
            } else {
                $this->lang->load('gokart_frontend','english');
            }
    } 

    public function index()
    {   

        $data['open'] = $this->Access->readtable('general','',array('general_section'=>'operational_time'))->row()->general_title_en;
        $data['close'] = $this->Access->readtable('general','',array('general_section'=>'operational_time'))->row()->general_title_in;
        # BANNER - VIDEO
        $data['video_banner'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'video'))->row();
        $data['video_poster'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'video_poster'))->row();
        $data['banner_text'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'banner_text'))->row();

        #KART SECTION
        $data['kart_content'] = $this->Access->readtable('general', '', array('general_page'=>'home', 'general_section'=>'kart_content'))->row();
        $data['karts'] = $this->Access->readtable('kart')->result();

        #TRACK SECTION
        $data['track_content'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'track_content'))->row();
        $data['tracks'] = $this->Access->readtable('media','',array('media_page'=>'home','media_section'=>'track_img'))->result();

        #DIVIDER
        $data['div1'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'divider1'))->row();
        $data['div2'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'divider2'))->row();
        $data['div3'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'divider3'))->row();
        $data['div4'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'divider4'))->row();

        //THE COMMUNITY
        
        // #EVENT
        $date_now = date('Y-m-d H:i:s');
        $data['events'] = $this->db->query('SELECT * FROM event WHERE event_date >= "'.$date_now.'" ORDER BY event_date ASC')->result();
        // #CHAMPIONSHIP
        $data['champ'] = $this->db->query("SELECT * FROM championship GROUP BY champ_date ORDER BY champ_date DESC LIMIT 1")->row();

        // #ALL STAR DRIVER
        // 3 TOP PILOT
        $data['top_rank'] = $this->db->query("SELECT * FROM statistic, race WHERE temp_id=race_pilot_id ORDER BY race_point DESC, race_best_lap ASC LIMIT 3")->result();

        // #LAST RACE
        $data['last_pro'] = $this->db->query("SELECT race_date FROM race WHERE race_type='pro' GROUP BY race_date ORDER BY race_date DESC")->result();
        $data['last_casual'] = $this->db->query("SELECT race_date FROM race WHERE race_type='casual' GROUP BY race_date ORDER BY race_date DESC")->result();

        $fastest_lap_pro = $this->db->query("SELECT * FROM race  WHERE race_type='pro' ORDER BY race_best_lap ASC")->row();
        $fastest_lap_casual = $this->db->query("SELECT * FROM race  WHERE race_type='casual' ORDER BY race_best_lap ASC")->row();

        $data['the_fastest_pro'] = $this->db->query("SELECT * FROM user WHERE user_id='".$fastest_lap_pro->race_pilot_id."'")->row()->user_name;
        $data['the_fastest_casual'] = $this->db->query("SELECT * FROM user WHERE user_id='".$fastest_lap_casual->race_pilot_id."'")->row()->user_name;

        $data['fastest_time_pro'] = $fastest_lap_pro->race_best_lap;
        $data['fastest_time_casual'] = $fastest_lap_casual->race_best_lap;

        //GALLERY
        $data['gallery'] = $this->db->query("SELECT * FROM media WHERE media_section='gallery' ORDER BY media_date DESC")->result();

        //SOCMED
        $data['socmed_tripadvisor'] = $this->Access->readtable('general','',array('general_section'=>'socmed_tripadvisor'))->row()->general_content_en;
        $data['socmed_facebook'] = $this->Access->readtable('general','',array('general_section'=>'socmed_facebook'))->row()->general_content_en;
        $data['socmed_twitter'] = $this->Access->readtable('general','',array('general_section'=>'socmed_twitter'))->row()->general_content_en;
        $data['socmed_instagram'] = $this->Access->readtable('general','',array('general_section'=>'socmed_instagram'))->row()->general_content_en;
        $data['socmed_youtube'] = $this->Access->readtable('general','',array('general_section'=>'socmed_youtube'))->row()->general_content_en;

        $data['lang'] = $this->session->userdata('fe_lang');
        $data['language'] = $this->session->userdata('fe_lang');
        $view['location'] = $this->Access->readtable('general','',array('general_section'=>'location'))->row()->general_content_en;
        $view['content']   = $this->load->view('gokart/v_home',$data,TRUE);
        $this->load->view('gokart/v_master',$view);

    }
    // function cek(){
    //     session_destroy();
    // }
}
