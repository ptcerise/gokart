<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign_in extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('gokart_frontend',$fe_lang);
            } else {
                $this->lang->load('gokart_frontend','english');
            }
    } 
    
    public function index()
    {
        if($this->session->userdata('pilot_status_login') == TRUE){
            redirect('profile');
        }

        //SOCMED
        $data['socmed_tripadvisor'] = $this->Access->readtable('general','',array('general_section'=>'socmed_tripadvisor'))->row()->general_content_en;
        $data['socmed_facebook'] = $this->Access->readtable('general','',array('general_section'=>'socmed_facebook'))->row()->general_content_en;
        $data['socmed_twitter'] = $this->Access->readtable('general','',array('general_section'=>'socmed_twitter'))->row()->general_content_en;
        $data['socmed_instagram'] = $this->Access->readtable('general','',array('general_section'=>'socmed_instagram'))->row()->general_content_en;
        $data['socmed_youtube'] = $this->Access->readtable('general','',array('general_section'=>'socmed_youtube'))->row()->general_content_en;
        
        $data['lang'] = $this->session->userdata('fe_lang');
        $view['location'] = $this->Access->readtable('general','',array('general_section'=>'location'))->row()->general_content_en;
        $view['content']   = $this->load->view('gokart/v_sign_in',$data,TRUE);
        $this->load->view('gokart/v_master',$view);

    }

    function auth(){
        $email = $this->input->post('email');
        $pass = $this->input->post('password');

        $check = $this->Access->readtable('user','',array('user_email'=>$email, 'user_pass'=>md5($pass), 'user_level'=>'3'))->row();

        if(count($check) == 1){
            $login_session = array(
                    'pilot_status_login' => TRUE,
                    'pilot_id' => $check->user_id,
                    'pilot_name' => $check->user_name,
                );
            $this->session->set_userdata($login_session);
        }

        if(count($check) == 0){
            $response = 0;
        }else{
            $response = 1;
        }

        $data = array(
                'result'=>$response,
            );

        echo json_encode($data);
    }

    
    function sign_out(){
        $logout_session = array(
                'pilot_status_login' => FALSE,
                'pilot_id' => '',
                'pilot_name' => '',
            );
        $this->session->unset_userdata($logout_session);
        $this->session->sess_destroy();
        redirect('sign_in');
    }

}
