<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');
            $this->load->library('email');
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('gokart_frontend',$fe_lang);
            } else {
                $this->lang->load('gokart_frontend','english');
            }
    }
    
    public function index()
    {
        $data['lang'] = $this->session->userdata('fe_lang');
        $data['content'] = $this->Access->readtable('general','',array('general_section'=>'content','general_page'=>'Contact_us'))->row();

        $data['phone'] = $this->Access->readtable('general','',array('general_section'=>'phone_number','general_page'=>'Contact_us'))->row();

        $data['address'] = $this->Access->readtable('general','',array('general_section'=>'address','general_page'=>'Contact_us'))->row();

        //SOCMED
        $data['socmed_tripadvisor'] = $this->Access->readtable('general','',array('general_section'=>'socmed_tripadvisor'))->row()->general_content_en;
        $data['socmed_facebook'] = $this->Access->readtable('general','',array('general_section'=>'socmed_facebook'))->row()->general_content_en;
        $data['socmed_twitter'] = $this->Access->readtable('general','',array('general_section'=>'socmed_twitter'))->row()->general_content_en;
        $data['socmed_instagram'] = $this->Access->readtable('general','',array('general_section'=>'socmed_instagram'))->row()->general_content_en;
        $data['socmed_youtube'] = $this->Access->readtable('general','',array('general_section'=>'socmed_youtube'))->row()->general_content_en;

        $view['content'] = $this->load->view('gokart/v_contact_us',$data,TRUE);

        //CURRENT PAGE
        $view['current'] = 'contact_us';
        $view['location'] = $this->Access->readtable('general','',array('general_section'=>'location'))->row()->general_content_en;
        $this->load->view('gokart/v_master', $view);
    }

    function send_message()
    {   
        $contact_name = $this->input->post('Name');
        $contact_email = $this->input->post('Email');
        $contact_subject = $this->input->post('Subject');
        $contact_message = $this->input->post('Message');

        $new_message = array(
                            'contact_name' => $this->input->post('Name'),
                            'contact_email' => $this->input->post('Email'),
                            'contact_phone' => $this->input->post('Phone'),
                            'contact_category' => $this->input->post('Category'),
                            'contact_subject' => $this->input->post('Subject'),
                            'contact_message' => $this->input->post('Message'),
                            );
        $this->db->trans_begin();
        $this->db->set('contact_date', 'NOW()', FALSE);
        $this->Access->inserttable('contact',$new_message);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = "Message send successful!";
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }

        // SEND TO info@kidsfun.co.id
            #Configure the E-mail content
            $config = array();
            $config['charset'] = 'utf-8';
            $config['useragent'] = 'Yogyagokart';
            $config['protocol']= "smtp";
            $config['mailtype']= "html";
            $config['smtp_host']= "ssl://yogyagokart.com";//smtp
            $config['smtp_port']= "465";
            $config['smtp_timeout']= "10";
            $config['smtp_user']= "notification@yogyagokart.com"; // sender email
            $config['smtp_pass']= "yogyagokart2016"; // sender password
            $config['crlf']="\r\n";
            $config['newline']="\r\n";
            $config['wordwrap'] = TRUE;
            $config['validate'] = TRUE;
            $config['priority'] = 2;
            $this->email->initialize($config);

            $this->email->from($contact_email,$contact_name);
            $this->email->to('info@kidsfun.co.id');
            $this->email->reply_to($contact_email);
            $this->email->subject($contact_subject);
            $this->email->message($contact_message);

            if($this->email->send()){ #Send the E-mail
                echo "<script type='text/javascript'>alert('Berhasil mengirim!');</script>";
                $_SESSION['info_send'] = $notif;
                $this->session->mark_as_flash('info_send');
            }else{
                echo "<script type='text/javascript'>alert('Gagal mengirim!');</script>";
                $_SESSION['info_send'] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Failed</div>';
                $this->session->mark_as_flash('info_send');
                
            }#Send E-mail END

            //$_SESSION['info_send'] = $notif;
            //$this->session->mark_as_flash('info_send');
            redirect('contact_us#form');
        
    }
}