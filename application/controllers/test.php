<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	function om(){
		$data['general'] = $this->db->where('general_page', 'home')
				->where('general_section','dividers_long')
				->get('general')->result();
		$this->load->view('test_om',$data);
	}

	private function skema($data)
	{
		echo '<pre>';
		print_r($data);
		echo '</pre>';
	}

	function send(){
		skema($_POST);
		apa();
		foreach ($_POST['general_id'] as $key=>$value) {
			echo "id=".$_POST['general_id'][$key]."itu contentnya =".$_POST['general_content'][$key].'<br>';
			
			$this->db->where('general_id',$_POST['general_id'][$key]);
			$this->db->update('general',['general_content'=>$_POST['general_content'][$key]]);
		}
		// $id = $this->input->post('general_id');
		// $kontent = $this->input->post('general_content');
		// 	$this->db->where('general_id',$id);
		// 	$this->db->update('general',['general_content'=>$kontent]);
	}
}
