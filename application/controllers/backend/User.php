<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('gokart_backend',$be_lang);
        } else {
            $this->lang->load('gokart_backend','english');
        }
    }
    
    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }elseif($this->session->userdata('user_level') != 1){
            redirect('backend/home');
        }

        $data['db_access'] = $this->Access;
        $data['user'] = $this->db->query("SELECT * FROM user WHERE user_level!=3")->result();

        $data['current'] = "user";
        $view['content'] = $this->load->view('backend/v_user',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }#END OF FUNCTION index()

    public function save_user(){
        $id = $this->input->post('user_id');
        $user_name = $this->input->post('user_name');
        $user_full_name = $this->input->post('user_full_name');
        $user_email = $this->input->post('user_email');
        $user_pass = $this->input->post('user_pass');
        $user_level = $this->input->post('user_level');

        #check if there any password change
        if($user_pass == ""){
            $save_data = array(
                'user_name'=>$user_name,
                'user_email'=>$user_email,
                'user_full_name'=>$user_full_name,
                'user_level'=>$user_level,
                );
        }else{
            $save_data = array(
                'user_name'=>$user_name,
                'user_email'=>$user_email,
                'user_pass'=>md5($user_pass),
                'user_full_name'=>$user_full_name,
                'user_level'=>$user_level,
                );
        }

        if($id == ""){
            # this will be create
            $this->db->trans_begin();
            $this->Access->inserttable('user',$save_data);
            $this->db->trans_complete();
        }else{
            # this will be update
            $this->db->trans_begin();
            $this->Access->updatetable('user',$save_data, array('user_id'=>$id));
            $this->db->trans_complete();
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $error_alert = "Input data error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('notif'=>$notif));
            $this->session->mark_as_flash("notif");
            redirect(base_url('backend/user'));
        }else{
            $success_alert = "Input data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
            $this->session->set_userdata(array('notif'=>$notif));
            $this->session->mark_as_flash("notif");
            redirect(base_url('backend/user'));
        }
    }# func save_user

    public function delete_user($id){
        $this->db->trans_begin();
        $this->Access->deletetable('user',array('user_id'=>$id));
        $this->db->trans_complete();
        redirect(base_url('backend/user'));
    }

}
