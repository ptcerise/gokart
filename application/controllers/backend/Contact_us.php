<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');      
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('gokart_backend',$be_lang);
            } else {
                $this->lang->load('gokart_backend','english');
            }
    }

    function index() {
        
        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'content','general_page'=>'contact_us'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'content','general_page'=>'contact_us'))->row();

        $data['message'] = $this->db->query('select * from contact order by contact_date DESC')->result();

        $data['phone'] = $this->Access->readtable('general','',array('general_section'=>'phone_number','general_page'=>'contact_us'))->row();
        $data['address'] = $this->Access->readtable('general','',array('general_section'=>'address','general_page'=>'contact_us'))->row();
        $data['location'] = $this->Access->readtable('general','',array('general_section'=>'location','general_page'=>'contact_us'))->row();

        $data['lang']    = $this->session->userdata('be_lang');
        $data['current'] = "contact_us";

        //SOCMED
        $data['socmed_tripadvisor'] = $this->Access->readtable('general','',array('general_section'=>'socmed_tripadvisor'))->row()->general_content_en;
        $data['socmed_facebook'] = $this->Access->readtable('general','',array('general_section'=>'socmed_facebook'))->row()->general_content_en;
        $data['socmed_twitter'] = $this->Access->readtable('general','',array('general_section'=>'socmed_twitter'))->row()->general_content_en;
        $data['socmed_instagram'] = $this->Access->readtable('general','',array('general_section'=>'socmed_instagram'))->row()->general_content_en;
        $data['socmed_youtube'] = $this->Access->readtable('general','',array('general_section'=>'socmed_youtube'))->row()->general_content_en;

        $view['script']  = $this->load->view('backend/script/contact_us','',TRUE);
        $view['content'] = $this->load->view('backend/contact_us/v_contact_us',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // --------------- update contact us content ---------------------
    function edit_content()
    {
        $content = array(
                'general_content_en' => $this->input->post('div_contact_en'),
                'general_content_in' => $this->input->post('div_contact_in')
            );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$content,array('general_section' => 'content','general_page'=>'contact_us'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/contact_us#content');
    }

    // --------------- update info contact ---------------------
    function info()
    {
        $phone_info = array(
                'general_content_en' => $this->input->post('phone')
            );
        $address_info = array(
                'general_content_en' => $this->input->post('address')
            );
        $location = array(
                'general_content_en' => $this->input->post('location')
            );

        $this->db->trans_begin();
        $this->Access->updatetable('general',$phone_info,array('general_section' => 'phone_number','general_page'=>'contact_us'));
        $this->Access->updatetable('general',$address_info,array('general_section' => 'address','general_page'=>'contact_us'));
        $this->Access->updatetable('general',$location,array('general_section' => 'location','general_page'=>'contact_us'));
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_contact'] = $notif;
        $this->session->mark_as_flash('info_contact');
        redirect('backend/contact_us#info');
    }

    // --------------- detail message ---------------------
    function view_message()
    {
        $contact_id = $this->input->post('contact_id');
        $status = array(
                'contact_status' => '1',
            );
        $this->Access->updatetable('contact',$status,array('contact_id'=>$contact_id));

        $data['view_message'] = $this->Access->readtable('contact','',array('contact_id'=>$contact_id))->row();

        $this->load->view('backend/contact_us/v_contact_msg',$data);
    }

    // --------------- delete message ---------------------
    function delete_message($contact_id)
    {
        $this->db->trans_begin();
        $this->Access->deletetable('contact',array('contact_id'=>$contact_id));
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $success    = $this->lang->line("delete");
            $notif      = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_message'] = $notif;
        $this->session->mark_as_flash('info_message');
        redirect('backend/contact_us#message');
    }

    # ---------------- SOCIAL MEDIA ----------------------
    function save_socmed(){
        $tripadvisor = array('general_content_en'=>$this->input->post('socmed_tripadvisor'));
        $facebook = array('general_content_en'=>$this->input->post('socmed_facebook'));
        $twitter = array('general_content_en'=>$this->input->post('socmed_twitter'));
        $instagram = array('general_content_en'=>$this->input->post('socmed_instagram'));
        $youtube = array('general_content_en'=>$this->input->post('socmed_youtube'));

        $this->db->trans_begin();
        $this->Access->updatetable('general',$tripadvisor,array('general_section'=>'socmed_tripadvisor'));
        $this->Access->updatetable('general',$facebook,array('general_section'=>'socmed_facebook'));
        $this->Access->updatetable('general',$twitter,array('general_section'=>'socmed_twitter'));
        $this->Access->updatetable('general',$instagram,array('general_section'=>'socmed_instagram'));
        $this->Access->updatetable('general',$youtube,array('general_section'=>'socmed_youtube'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $failed    = "Update data failed!";
            $notif      = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$failed.'</div>';
        }
        else
        {
            $success    = "Update data success!";
            $notif      = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $this->session->set_userdata(array('socmed'=>$notif));
        $this->session->mark_as_flash('socmed');
        redirect('backend/contact_us#socmed');

    }
}