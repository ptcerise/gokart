<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Race extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('gokart_backend',$be_lang);
        } else {
            $this->lang->load('gokart_backend','english');
        }
    }
    
    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }
        // $data['pilot'] = $this->Access->readtable('user','',array('user_level'=>'3'))->result();
        // $data['pilot2'] = $this->Access->readtable('user','',array('user_level'=>'3'))->result();
        
        $data['pilot'] = $this->db->query("SELECT * FROM user WHERE user_level='3' ORDER BY user_full_name ASC")->result();
        $data['pilot2'] = $this->db->query("SELECT * FROM user WHERE user_level='3' ORDER BY user_full_name ASC")->result();

        $data['pro_race'] = $this->db->query("SELECT * FROM race WHERE race_type='pro'")->result();

        $data['casual_race'] = $this->db->query("SELECT * FROM race WHERE race_type='casual'")->result();

        $data['current'] = "race";
        $view['content'] = $this->load->view('backend/v_race',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    public function save_records(){
        $race_date = $this->input->post('race_date');
        $expl = explode("/", $race_date);
        $newdate = $expl[2]."-".$expl[0]."-".$expl[1];

        $race_winner = $this->input->post('race_winner');
        $race_type = $this->input->post('race_type');

        #put these to array
        $race_pilot_id = $this->input->post('race_pilot_id');
        $race_time_s = $this->input->post('race_time_s');
        $race_time_ms = $this->input->post('race_time_ms');
        $race_best_lap_s = $this->input->post('race_best_lap_s');
        $race_best_lap_ms = $this->input->post('race_best_lap_ms');
        $race_point = $this->input->post('race_point');
        $race_info = $this->input->post('race_info');

        $post_count = count($race_pilot_id);
        #echo $post_count;
        $rt_ms = strlen($race_time_ms);
        $bl_ms = strlen($race_best_lap_ms);

        if($rt_ms==1){
            $race_time_ms = $race_time_ms*100;
        }else if($rt_ms==2){
            $race_time_ms = $race_time_ms*10;
        }else {
            $race_time_ms;
        }

        if($bl_ms==1){
            $race_best_lap_ms = $race_best_lap_ms*100;
        }else if($bl_ms==2){
            $race_best_lap_ms = $race_best_lap_ms*10;
        }else {
            $race_best_lap_ms;
        }

        for($i=0;$i<$post_count;$i++){

            $race_time = ($race_time_s[$i] * 1000) + ($race_time_ms[$i]);
            $race_best_lap = ($race_best_lap_s[$i] * 1000) + ($race_best_lap_ms[$i]);

            if($race_pilot_id[$i] == $race_winner){
                $save_data = array(
                    'race_pilot_id'=>$race_pilot_id[$i],
                    'race_time'=>$race_time,
                    #'race_time_m'=>$race_time_m[$i],
                    #'race_time_s'=>$race_time_s[$i],
                    'race_best_lap'=>$race_best_lap,
                    #'race_best_lap_m'=>$race_best_lap_m[$i],
                    #'race_best_lap_s'=>$race_best_lap_s[$i],
                    'race_type'=>$race_type,
                    'race_date'=>$newdate,
                    'race_info'=>$race_info[$i],
                    'race_win'=>1,
                    'race_point'=>$race_point[$i],
                );
            }else{
                $save_data = array(
                    'race_pilot_id'=>$race_pilot_id[$i],
                    'race_time'=>$race_time,
                    #'race_time_m'=>$race_time_m[$i],
                    #'race_time_s'=>$race_time_s[$i],
                    'race_best_lap'=>$race_best_lap,
                    #'race_best_lap_m'=>$race_best_lap_m[$i],
                    #'race_best_lap_s'=>$race_best_lap_s[$i],
                    'race_type'=>$race_type,
                    'race_date'=>$newdate,
                    'race_info'=>$race_info[$i],
                    'race_point'=>$race_point[$i],
                );
            }
            $this->db->trans_begin();
            $this->Access->inserttable('race', $save_data);
            $this->db->trans_complete();

            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $error_alert = "Update data error!";
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
                $this->session->set_userdata(array('notif_race'=>$notif));
                $this->session->mark_as_flash("notif_race");
                redirect(base_url('backend/race'));
            }

            #print_r($save_data);echo "<hr>";
        }

        $success_alert = "Update data success!";
        $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
        $this->session->set_userdata(array('notif_race'=>$notif));
        $this->session->mark_as_flash("notif_race");

        ################################ STATISTIC REVIEW ###################################
        
        #----------------------------- UPDATING TOTAL WINS ---------------------------------#
        $all_member = $this->Access->readtable('user','',array('user_level'=>'3'))->result();
        
        foreach ($all_member as $key => $value) {
            #define new variable for more effective use
            $id = $value->user_id;
            $pilot = $value->user_name;

            #checking availibility in 'statistic' table
            $check_stat = $this->Access->readtable('statistic','',array('temp_id'=>$id))->num_rows();
            if($check_stat == 0){ #create new data if it's '0'
                $newstat = array(
                        'stat_pilot'=>$pilot,
                        'temp_id'=>$id,
                    );
                $this->Access->inserttable('statistic',$newstat);
            }

            #counting total win in all races
            // $total_wins = $this->Access->readtable('race','',array('race_pilot_id'=>$id, 'race_win'=>'1'))->num_rows();

            // $win_stat = array(
            //         'stat_win'=>$total_wins,
            //         'stat_date'=>date('Y-m-d H:i:s'),
            //     );
            // $this->Access->updatetable('statistic',$win_stat, array('temp_id'=>$id));
            #saving total points
            $total_points = $this->db->query("SELECT SUM(race_point) AS total_points FROM race WHERE race_pilot_id='$id'")->row()->total_points;
            $point_stat = array(
                    'stat_point'=>$total_points,
                    'stat_date'=>date('Y-m-d H:i:s'),
                );
            $this->Access->updatetable('statistic',$point_stat, array('temp_id'=>$id));
            #echo "<script>alert(".$total_wins.");</script>";
        }

        #-------------------------- DEFINE RANK BY TOTAL WINS ------------------------------#
        $all_stat = $this->db->query('SELECT * FROM statistic ORDER BY stat_point DESC')->result();
        foreach ($all_stat as $key => $value) {
            $id = $value->temp_id;
            $rank = $key+1;
            #echo "<script>alert('".$rank."|".$value->stat_pilot." - ".$value->stat_win."');</script>";
            $upd_rank = array(
                    'stat_rank'=>$rank,
                );
            $this->Access->updatetable('statistic',$upd_rank, array('temp_id'=>$id));
        }

        #################################### END REVIEW #######################################

        redirect(base_url('backend/race'));

        #echo $race_winner;
    }# func save record

    public function delete_data($id){
        $this->Access->deletetable('race', array('race_id'=>$id));

        ################################ STATISTIC REVIEW ###################################
        
        #----------------------------- UPDATING TOTAL WINS ---------------------------------#
        $all_member = $this->Access->readtable('user','',array('user_level'=>'3'))->result();
        
        foreach ($all_member as $key => $value) {
            #define new variable for more effective use
            $id = $value->user_id;
            $pilot = $value->user_name;

            #checking availibility in 'statistic' table
            $check_stat = $this->Access->readtable('statistic','',array('temp_id'=>$id))->num_rows();
            if($check_stat == 0){ #create new data if it's '0'
                $newstat = array(
                        'stat_pilot'=>$pilot,
                        'temp_id'=>$id,
                    );
                $this->Access->inserttable('statistic',$newstat);
            }

            #counting total win in all races
            // $total_wins = $this->Access->readtable('race','',array('race_pilot_id'=>$id, 'race_win'=>'1'))->num_rows();

            // $win_stat = array(
            //         'stat_win'=>$total_wins,
            //         'stat_date'=>date('Y-m-d H:i:s'),
            //     );
            // $this->Access->updatetable('statistic',$win_stat, array('temp_id'=>$id));
            #saving total points
            $total_points = $this->db->query("SELECT SUM(race_point) AS total_points FROM race WHERE race_pilot_id='$id'")->row()->total_points;
            $point_stat = array(
                    'stat_point'=>$total_points,
                    'stat_date'=>date('Y-m-d H:i:s'),
                );
            $this->Access->updatetable('statistic',$point_stat, array('temp_id'=>$id));
            #echo "<script>alert(".$total_wins.");</script>";
        }

        #-------------------------- DEFINE RANK BY TOTAL WINS ------------------------------#
        $all_stat = $this->db->query('SELECT * FROM statistic ORDER BY stat_point DESC')->result();
        foreach ($all_stat as $key => $value) {
            $id = $value->temp_id;
            $rank = $key+1;
            #echo "<script>alert('".$rank."|".$value->stat_pilot." - ".$value->stat_win."');</script>";
            $upd_rank = array(
                    'stat_rank'=>$rank,
                );
            $this->Access->updatetable('statistic',$upd_rank, array('temp_id'=>$id));
        }

        #################################### END REVIEW #######################################

        redirect(base_url('backend/race'));
    }# func delete data

    public function update_race(){
        $id = $this->input->post('race_id');
        $race_time_s = $this->input->post('race_time_s');
        $race_time_ms = $this->input->post('race_time_ms');
        $race_best_lap_s = $this->input->post('race_best_lap_s');
        $race_best_lap_ms = $this->input->post('race_best_lap_ms');
        $race_info = $this->input->post('race_info');
        $race_type = $this->input->post('race_type');
        $race_point = $this->input->post('race_point');

        $rt_ms = strlen($race_time_ms);
        $bl_ms = strlen($race_best_lap_ms);
        if($rt_ms==1){
            $race_time_ms = $race_time_ms*100;
        }else if($rt_ms==2){
            $race_time_ms = $race_time_ms*10;
        }else {
            $race_time_ms;
        }

        if($bl_ms==1){
            $race_best_lap_ms = $race_best_lap_ms*100;
        }else if($bl_ms==2){
            $race_best_lap_ms = $race_best_lap_ms*10;
        }else {
            $race_best_lap_ms;
        }

        $race_time = ($race_time_s * 1000) + ($race_time_ms);
        $race_best_lap = ($race_best_lap_s * 1000) + ($race_best_lap_ms);

        $save_data = array(
                'race_time'=>$race_time,
                #'race_time_m'=>$race_time_m[$i],
                #'race_time_s'=>$race_time_s[$i],
                'race_best_lap'=>$race_best_lap,
                #'race_best_lap_m'=>$race_best_lap_m[$i],
                #'race_best_lap_s'=>$race_best_lap_s[$i],
                'race_info'=>$race_info,
                'race_point'=>$race_point,
            );
        #print_r($save_data);echo "<hr>".$id;
        
        $this->db->trans_begin();
        $this->Access->updatetable('race',$save_data,array('race_id'=>$id));
        $this->db->trans_complete();

        ################################ STATISTIC REVIEW ###################################
        
        #----------------------------- UPDATING TOTAL WINS ---------------------------------#
        $all_member = $this->Access->readtable('user','',array('user_level'=>'3'))->result();
        
        foreach ($all_member as $key => $value) {
            #define new variable for more effective use
            $id = $value->user_id;
            $pilot = $value->user_name;

            #checking availibility in 'statistic' table
            $check_stat = $this->Access->readtable('statistic','',array('temp_id'=>$id))->num_rows();
            if($check_stat == 0){ #create new data if it's '0'
                $newstat = array(
                        'stat_pilot'=>$pilot,
                        'temp_id'=>$id,
                    );
                $this->Access->inserttable('statistic',$newstat);
            }

            #counting total win in all races
            // $total_wins = $this->Access->readtable('race','',array('race_pilot_id'=>$id, 'race_win'=>'1'))->num_rows();

            // $win_stat = array(
            //         'stat_win'=>$total_wins,
            //         'stat_date'=>date('Y-m-d H:i:s'),
            //     );
            // $this->Access->updatetable('statistic',$win_stat, array('temp_id'=>$id));
            #saving total points
            $total_points = $this->db->query("SELECT SUM(race_point) AS total_points FROM race WHERE race_pilot_id='$id'")->row()->total_points;
            $point_stat = array(
                    'stat_point'=>$total_points,
                    'stat_date'=>date('Y-m-d H:i:s'),
                );
            $this->Access->updatetable('statistic',$point_stat, array('temp_id'=>$id));
            #echo "<script>alert(".$total_wins.");</script>";
        }

        #-------------------------- DEFINE RANK BY TOTAL WINS ------------------------------#
        $all_stat = $this->db->query('SELECT * FROM statistic ORDER BY stat_point DESC')->result();
        foreach ($all_stat as $key => $value) {
            $id = $value->temp_id;
            $rank = $key+1;
            #echo "<script>alert('".$rank."|".$value->stat_pilot." - ".$value->stat_win."');</script>";
            $upd_rank = array(
                    'stat_rank'=>$rank,
                );
            $this->Access->updatetable('statistic',$upd_rank, array('temp_id'=>$id));
        }

        #################################### END REVIEW #######################################

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $error_alert = "Update data error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('notif_race_'.$race_type=>$notif));
            $this->session->mark_as_flash("notif_race_".$race_type);
            redirect(base_url('backend/race'));
        }else{
            $success_alert = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
            $this->session->set_userdata(array('notif_race_'.$race_type=>$notif));
            $this->session->mark_as_flash("notif_race_".$race_type);
            redirect(base_url('backend/race'));
        }
    }# func update race
}
