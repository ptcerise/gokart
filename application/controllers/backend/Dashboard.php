<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();   
    }
    
    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        $data['current'] = "dashboard";
        $view['content'] = $this->load->view('backend/v_dashboard',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

}
