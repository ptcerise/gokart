<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('gokart_backend',$be_lang);
        } else {
            $this->lang->load('gokart_backend','english');
        }
    }
    
    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        $data['gallery'] = $this->db->query("SELECT * FROM media WHERE media_section='gallery' ORDER BY media_date DESC")->result();

        $data['current'] = "gallery";
        $view['content'] = $this->load->view('backend/v_gallery',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    public function save_images(){
        $desc_en = $this->input->post('desc_en');
        $desc_in = $this->input->post('desc_in');

        $id = $this->input->post('img_id');
        $old_file = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'gallery', 'media_id'=>$id))->row()->media_url;

        if($id == ""){
            if(isset($_FILES['upl_img']['name'])){
                $media_url  =   $_FILES['upl_img']['name'];
                $break      =   explode('.', $media_url);
                $ext        =   strtolower($break[count($break) - 1]);
                $date       =   date('dmYHis');
                $media_url  =   'img_'.$date.'.'.$ext;
                $upl_img    = $media_url;
                $path       =   './assets/upload/gallery';

                if( ! file_exists( $path ) ){
                    $create = mkdir($path, 0777, TRUE);
                    $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                    if( ! $create || ! $createTemp ) return;
                }

                $this->piclib->get_config($media_url, $path, 2048000);
                if( $this->upload->do_upload('upl_img') ){
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1024 || $height < 768 ){
                        unlink( realpath( APPPATH.'../assets/upload/gallery/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/gallery/thumbnail/'.$media_url ));
                        $img_less = "The image resolution is below the minimum resolution allowed, please make sure the resolution is 1024x768px";
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$img_less.'</div>';
                    }else{
                        $r_width = $width * 0.4;
                        $r_height = $height * 0.4;
                        $this->piclib->resize_image($source_path, $width, $height, $r_width, $r_height);
                            if( $this->image_lib->resize() ){
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, $r_width, $r_height, $path.'/thumbnail');
                                $this->image_lib->resize();

                                unlink( realpath( APPPATH.'../assets/upload/gallery/'.$old_file ));
                                unlink( realpath( APPPATH.'../assets/upload/gallery/thumbnail/'.$old_file ));
                                $save_data = array(
                                    'media_content_en'=>$desc_en,
                                    'media_content_in'=>$desc_in,
                                    'media_url'=>$media_url,
                                    'media_page'=>'home',
                                    'media_section'=>'gallery',
                                    'media_date'=>date('Y-m-d H:i:s'),
                                    );
                                $this->db->trans_begin();
                                $this->Access->inserttable('media',$save_data);
                                $this->db->trans_complete();

                                if($this->db->trans_status() === FALSE){
                                    $this->db->trans_rollback();
                                    $error_alert = "Upload image error!";
                                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
                                }else{
                                    $success_alert = "Upload image success!";
                                    $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
                                }
                            }
                    }
                    
                }
            }
            $this->session->set_userdata(array('upload_images'=>$notif));
            $this->session->mark_as_flash("upload_images");
            redirect(base_url('backend/gallery#edit_image'));
        }else{

            $save_data = array(
                'media_content_en'=>$desc_en,
                'media_content_in'=>$desc_in,
                'media_page'=>'home',
                'media_section'=>'gallery',
                'media_date'=>date('Y-m-d H:i:s'),
                );

            $this->db->trans_begin();
            $this->Access->updatetable('media',$save_data,array('media_id'=>$id));
            $this->db->trans_complete();

            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $error_alert = "Update data error!";
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
                $this->session->set_userdata(array('edit'=>$notif));
                $this->session->mark_as_flash("edit");
                redirect(base_url('backend/gallery#edit_image'));
            }else{
                $success_alert = "Update data success!";
                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
                $this->session->set_userdata(array('edit'=>$notif));
                $this->session->mark_as_flash("edit");
                redirect(base_url('backend/gallery#edit_image'));

            }
        }

    } # func save image

    public function delete_image($id){
        $img_to_del = $this->Access->readtable('media','',array('media_section'=>'gallery','media_id'=>$id))->row()->media_url;
        unlink( realpath( APPPATH.'../assets/upload/gallery/'.$img_to_del ));
        unlink( realpath( APPPATH.'../assets/upload/gallery/thumbnail/'.$img_to_del ));
        $this->db->trans_begin();
        $this->Access->deletetable('media',array('media_id'=>$id));
        $this->db->trans_complete();
        redirect(base_url('backend/gallery#edit_image'));
    }# func delete image
}
