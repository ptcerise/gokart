<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('gokart_backend',$be_lang);
        } else {
            $this->lang->load('gokart_backend','english');
        }
    }
    
    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        $data['event'] = $this->Access->readtable('event')->result();

        $data['current'] = "event";
        $view['content'] = $this->load->view('backend/v_event',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    public function save_event(){
        $id = $this->input->post('event_id');
        $event_name = $this->input->post('event_name');
        $event_date = $this->input->post('event_date');

        $expl = explode("/", $event_date);
        #print_r($expl);
        $newdate = $expl[2]."-".$expl[0]."-".$expl[1];
        $save_data = array(
                'event_name'=>$event_name,
                'event_date'=>$newdate,
            );

        if($id == ""){
            $this->db->trans_begin();
            $this->Access->inserttable('event',$save_data);
            $this->db->trans_complete();
        }else{
            $this->db->trans_begin();
            $this->Access->updatetable('event',$save_data,array('event_id'=>$id));
            $this->db->trans_complete();
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $error_alert = "Update data error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('notif_event'=>$notif));
            $this->session->mark_as_flash("notif_event");
            redirect(base_url('backend/event'));
        }else{
            $success_alert = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
            $this->session->set_userdata(array('notif_event'=>$notif));
            $this->session->mark_as_flash("notif_event");
            redirect(base_url('backend/event'));
        }

    }# func save event

    public function delete_event($id){
        $this->Access->deletetable('event',array('event_id'=>$id));
        redirect('backend/event');
    }

}
