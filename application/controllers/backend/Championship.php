<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Championship extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('gokart_backend',$be_lang);
            } else {
                $this->lang->load('gokart_backend','english');
            }
    }
    
    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        //$data['pilot'] = $this->Access->readtable('user','',array('user_level'=>'3'))->result();
        $data['pilot'] = $this->db->query("SELECT * FROM user WHERE user_level='3' ORDER BY user_full_name ASC")->result();
        $data['champ'] = $this->db->query("SELECT * FROM championship GROUP BY champ_date ORDER BY champ_date DESC")->result();

        $data['current'] = "championship";
        $view['content'] = $this->load->view('backend/v_championship',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    public function save_records(){
        $champ_date = $this->input->post('champ_date');
        $expl = explode("/", $champ_date);
        $newdate = $expl[2]."-".$expl[0]."-".$expl[1];

        $champ_pilot = $this->input->post('champ_pilot');
        $champ_race_count = $this->input->post('champ_race_count');
        $champ_point = $this->input->post('champ_point');
        $champ_podium = $this->input->post('champ_podium');
        $data_count = count($champ_pilot);

        #print_r($champ_pilot);echo "<br>";
        #print_r($champ_race_count);echo "<br>";
        #print_r($champ_point);echo "<br>";
        #echo $champ_date."<br>";
        
        for($i=0; $i < $data_count; $i++){
            $save_data = array(
                    'champ_pilot'=>$champ_pilot[$i],
                    'champ_race_count'=>$champ_race_count[$i],
                    'champ_point'=>$champ_point[$i],
                    'champ_podium'=>$champ_podium[$i],
                    'champ_date'=>$newdate,
                );
            #print_r($save_data);echo "<br>";
            $this->db->trans_begin();
            $this->Access->inserttable('championship', $save_data);
            $this->db->trans_complete();

            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $error_alert = "Update data error!";
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
                $this->session->set_userdata(array('notif_champ'=>$notif));
                $this->session->mark_as_flash("notif_champ");
                redirect(base_url('backend/championship'));
            }

        }

        $success_alert = "Update data success!";
        $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
        $this->session->set_userdata(array('notif_champ'=>$notif));
        $this->session->mark_as_flash("notif_champ");
        redirect(base_url('backend/championship'));

    }# func save records

    public function update_champ(){
        $id = $this->input->post("champ_id");
        $champ_pilot = $this->input->post("champ_pilot");
        $champ_race_count = $this->input->post("champ_race_count");
        $champ_point = $this->input->post("champ_point");
        $champ_podium = $this->input->post("champ_podium");

        $save_data = array(
                'champ_pilot'=>$champ_pilot,
                'champ_race_count'=>$champ_race_count,
                'champ_point'=>$champ_point,
                'champ_podium'=>$champ_podium,
            );

        #echo $id;print_r($save_data);
        $this->db->trans_begin();
        $this->Access->updatetable('championship', $save_data,array('champ_id'=>$id));
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $error_alert = "Update data error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('notif_update'=>$notif));
            $this->session->mark_as_flash("notif_update");
            redirect(base_url('backend/championship'));
        }else{
            $this->db->trans_rollback();
            $error_alert = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('notif_update'=>$notif));
            $this->session->mark_as_flash("notif_update");
            redirect(base_url('backend/championship'));
        }
    }

    public function delete_data($id){
        $this->Access->deletetable('championship', array('champ_id'=>$id));
        redirect(base_url('backend/championship'));
    }# func delete data

}
