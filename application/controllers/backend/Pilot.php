<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pilot extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('gokart_backend',$be_lang);
        } else {
            $this->lang->load('gokart_backend','english');
        }

        ################################ STATISTIC REVIEW ###################################
        
        #----------------------------- UPDATING TOTAL WINS ---------------------------------#
        $all_member = $this->Access->readtable('user','',array('user_level'=>'3'))->result();
        
        foreach ($all_member as $key => $value) {
            #define new variable for more effective use
            $id = $value->user_id;
            $pilot = $value->user_name;

            #checking availibility in 'statistic' table
            $check_stat = $this->Access->readtable('statistic','',array('temp_id'=>$id))->num_rows();
            if($check_stat == 0){ #create new data if it's '0'
                $newstat = array(
                        'stat_pilot'=>$pilot,
                        'temp_id'=>$id,
                    );
                $this->Access->inserttable('statistic',$newstat);
            }

            #counting total win in all races
            // $total_wins = $this->Access->readtable('race','',array('race_pilot_id'=>$id, 'race_win'=>'1'))->num_rows();

            // $win_stat = array(
            //         'stat_win'=>$total_wins,
            //         'stat_date'=>date('Y-m-d H:i:s'),
            //     );
            // $this->Access->updatetable('statistic',$win_stat, array('temp_id'=>$id));
            #saving total points
            $total_points = $this->db->query("SELECT SUM(race_point) AS total_points FROM race WHERE race_pilot_id='$id'")->row()->total_points;
            $point_stat = array(
                    'stat_point'=>$total_points,
                    'stat_date'=>date('Y-m-d H:i:s'),
                );
            $this->Access->updatetable('statistic',$point_stat, array('temp_id'=>$id));
            #echo "<script>alert(".$total_wins.");</script>";
        }

        #-------------------------- DEFINE RANK BY TOTAL WINS ------------------------------#
        $all_stat = $this->db->query('SELECT * FROM statistic ORDER BY stat_point DESC')->result();
        foreach ($all_stat as $key => $value) {
            $id = $value->temp_id;
            $rank = $key+1;
            #echo "<script>alert('".$rank."|".$value->stat_pilot." - ".$value->stat_win."');</script>";
            $upd_rank = array(
                    'stat_rank'=>$rank,
                );
            $this->Access->updatetable('statistic',$upd_rank, array('temp_id'=>$id));
        }

        #################################### END REVIEW #######################################
    }
    
    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        $data['db_access'] = $this->Access;
        $data['user'] = $this->Access->readtable('user','',array('user_level'=>3))->result();

        $data['current'] = "member";
        $view['content'] = $this->load->view('backend/v_pilot',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }#END OF FUNCTION index()

    public function save_pilot(){
        $usr_id = $this->input->post("pilot_id");

        if(empty($usr_id)){
            $user_name = $this->input->post("user_name");
            $user_email = $this->input->post("user_email");
            $user_pass = $this->input->post("user_pass");
            $profile_phone = $this->input->post("profile_phone");
            $profile_pin_bb = $this->input->post("profile_pin_bb");
            $profile_address = $this->input->post("profile_address");
            $profile_postcode = $this->input->post("profile_postcode");
            $profile_city = $this->input->post("profile_city");

            # SAVE TO TABLE 'USER' FIRST
            $this->db->trans_begin();
            $save_user = array(
                "user_name"=>$user_name,
                "user_email"=>$user_email,
                "user_pass"=>md5($user_pass),
                "user_full_name"=>$user_name,
                "user_level"=>3,
            );

            $this->Access->inserttable('user', $save_user);
            
            $this->db->trans_complete();
            # GET THE ID OF LATEST ROW IN TABLE 'USER'
            $getid = $this->db->query("SELECT * FROM user ORDER BY user_id DESC LIMIT 1")->row()->user_id;

            $this->db->trans_begin();
                # THEN PUT THE ID FOR 'TEMP ID' ON TABLE 'PROFILE'
                $save_profile = array(
                    "profile_phone"=>$profile_phone,
                    "profile_pin_bb"=>$profile_pin_bb,
                    "profile_address"=>$profile_address,
                    "profile_postcode"=>$profile_postcode,
                    "profile_city"=>$profile_city,
                    "temp_id"=>$getid,
                );

                $this->Access->inserttable('profile', $save_profile);

                $save_stat = array(
                    "stat_pilot"=>$user_name,
                    "temp_id"=>$getid,
                );
                $this->Access->inserttable('statistic',$save_stat);

            $this->db->trans_complete();
            
            if(isset($_FILES['profile_photo']['name'])){
                $media_url  =   $_FILES['profile_photo']['name'];
                $break      =   explode('.', $media_url);
                $ext        =   strtolower($break[count($break) - 1]);
                $date       =   date('dmYHis');
                $media_url  =   'pilot_'.$date.'.'.$ext;
                $profile_photo = $media_url;
                $path       =   './assets/upload/pilot';

                if( ! file_exists( $path ) ){
                    $create = mkdir($path, 0777, TRUE);
                    $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                    if( ! $create || ! $createTemp ) return;
                }

                $this->piclib->get_config($media_url, $path, 1024);
                if( $this->upload->do_upload('profile_photo') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 250 || $height < 320 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/pilot/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/pilot/thumbnail/'.$media_url ));
                        $img_less = "The image resolution is below the minimum resolution allowed, please make sure the resolution is 250x320px";
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$img_less.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'landscape' || $orientation == 'square')
                        {
                            unlink( realpath( APPPATH.'../assets/upload/pilot/'.$media_url ));
                            unlink( realpath( APPPATH.'../assets/upload/pilot/thumbnail/'.$media_url ));
                            $orientation = "The image orientation must be 'Portrait'";
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$orientation.'</div>';     
                        }else{
                            $this->piclib->resize_image($source_path, $width, $height, 250, 320);
                            if( $this->image_lib->resize() ){
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 250, 320, $path.'/thumbnail');
                                $this->image_lib->resize();
                                
                                unlink( realpath( APPPATH.'../assets/upload/home/'.$usr_id ));
                                unlink( realpath(APPPATH.'../assets/upload/upload/thumbnail/'.$usr_id ));
                                }
                            }
                        }
                    }
                
            }else{
                $profile_photo = "";
            }
        
        }else{
            $user_name = $this->input->post("user_name");
            $user_email = $this->input->post("user_email");
            $user_pass = $this->input->post("user_pass");
            $profile_phone = $this->input->post("profile_phone");
            $profile_pin_bb = $this->input->post("profile_pin_bb");
            $profile_address = $this->input->post("profile_address");
            $profile_postcode = $this->input->post("profile_postcode");
            $profile_city = $this->input->post("profile_city");

            # SAVE TO TABLE 'USER' FIRST
            $this->db->trans_begin();
            if($user_pass == ""){
                $save_user = array(
                    "user_name"=>$user_name,
                    "user_email"=>$user_email,
                    "user_full_name"=>$user_name,
                );
            }else{
                $save_user = array(
                    "user_name"=>$user_name,
                    "user_email"=>$user_email,
                    "user_pass"=>md5($user_pass),
                    "user_full_name"=>$user_name,
                );
            }

            $this->Access->updatetable('user', $save_user,array('user_id'=>$usr_id));
            $this->db->trans_complete();
            # GET THE ID OF LATEST ROW IN TABLE 'USER'
            # 
            if(!empty($_FILES['profile_photo']['name'])){
                if(isset($_FILES['profile_photo']['name'])){
                    $media_url  =   $_FILES['profile_photo']['name'];
                    $break      =   explode('.', $media_url);
                    $ext        =   strtolower($break[count($break) - 1]);
                    $date       =   date('dmYHis');
                    $media_url  =   'pilot_'.$date.'.'.$ext;
                    $profile_photo = $media_url;
                    $path       =   './assets/upload/pilot';

                    if( ! file_exists( $path ) ){
                        $create = mkdir($path, 0777, TRUE);
                        $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                        if( ! $create || ! $createTemp ) return;
                    }

                    $this->piclib->get_config($media_url, $path, 1024000);
                    if( $this->upload->do_upload('profile_photo') )
                    {
                        $image = array('upload_data' => $this->upload->data());
                        $source_path = $image['upload_data']['full_path'];
                        $width = $image['upload_data']['image_width'];
                        $height = $image['upload_data']['image_height'];
                        
                        if( $width < 250 || $height < 320 )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/pilot/'.$media_url ));
                            unlink( realpath( APPPATH.'../assets/upload/pilot/thumbnail/'.$media_url ));
                            $img_less = "The image resolution is below the minimum resolution allowed, please make sure the resolution is 250x320px";
                            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$img_less.'</div>';
                        }else{
                            $orientation = $this->piclib->orientation($source_path);
                            if( $orientation == 'landscape' || $orientation == 'square')
                            {
                                unlink( realpath( APPPATH.'../assets/upload/pilot/'.$media_url ));
                                unlink( realpath( APPPATH.'../assets/upload/pilot/thumbnail/'.$media_url ));
                                $orientation = "The image orientation must be 'Portrait'";
                                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$orientation.'</div>';     
                            }else{
                                $this->piclib->resize_image($source_path, $width, $height, 250, 320);
                                if( $this->image_lib->resize() ){
                                    $this->image_lib->clear();
                                    $this->piclib->resize_image($source_path, $width, $height, 250, 320, $path.'/thumbnail');
                                    $this->image_lib->resize();
                                    
                                    unlink( realpath( APPPATH.'../assets/upload/home/'.$usr_id ));
                                    unlink( realpath(APPPATH.'../assets/upload/upload/thumbnail/'.$usr_id ));
                                    $this->db->trans_begin();
                                        # THEN PUT THE ID FOR 'TEMP ID' ON TABLE 'PROFILE'
                                        $save_profile = array(
                                            "profile_photo"=>$profile_photo,
                                            "profile_phone"=>$profile_phone,
                                            "profile_pin_bb"=>$profile_pin_bb,
                                            "profile_address"=>$profile_address,
                                            "profile_postcode"=>$profile_postcode,
                                            "profile_city"=>$profile_city,
                                            "temp_id"=>$usr_id,
                                        );

                                        $this->Access->updatetable('profile', $save_profile,array('temp_id'=>$usr_id));

                                    $this->db->trans_complete();

                                    if($this->db->trans_status() === FALSE){
                                        $this->db->trans_rollback();
                                        $error_alert = "Update data error!";
                                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
                                    }else{
                                        $success_alert = "Update data success!";
                                        $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                $this->db->trans_begin();
                    # THEN PUT THE ID FOR 'TEMP ID' ON TABLE 'PROFILE'
                    $save_profile = array(
                        "profile_phone"=>$profile_phone,
                        "profile_pin_bb"=>$profile_pin_bb,
                        "profile_address"=>$profile_address,
                        "profile_postcode"=>$profile_postcode,
                        "profile_city"=>$profile_city,
                        "temp_id"=>$usr_id,
                    );

                    $this->Access->updatetable('profile', $save_profile,array('temp_id'=>$usr_id));

                $this->db->trans_complete();

                if($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $error_alert = "Update data error!";
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
                }else{
                    $success_alert = "Update data success!";
                    $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
                }
            }
           
        }

        $this->session->set_userdata(array('notif'=>$notif));
        $this->session->mark_as_flash("notif");
        redirect(base_url('backend/pilot'));
        
    }#END OF FUNCTION save_pilot()

    public function delete_pilot($id){
        $this->db->trans_begin();
        $this->Access->deletetable('user',array('user_id'=>$id));
        $this->Access->deletetable('profile',array('temp_id'=>$id));
        $this->Access->deletetable('statistic',array('temp_id'=>$id));
        $this->db->trans_complete();
        redirect(base_url('backend/pilot'));
    }#END OF FUNCTION delete_pilot()

}
