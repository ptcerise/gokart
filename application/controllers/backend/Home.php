<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('gokart_backend',$be_lang);
        } else {
            $this->lang->load('gokart_backend','english');
        }
    }
    
    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        #Operational Time
        $data['open'] = $this->Access->readtable('general','',array('general_section'=>'operational_time'))->row()->general_title_en;
        $data['close'] = $this->Access->readtable('general','',array('general_section'=>'operational_time'))->row()->general_title_in;

        #BANNER
        $data['video_banner'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'video'))->row();
        $data['video_poster'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'video_poster'))->row();
        $data['banner_text'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'banner_text'))->row();

        #KART SECTION
        $data['kart_content'] = $this->Access->readtable('general', '', array('general_page'=>'home', 'general_section'=>'kart_content'))->row();
        $data['karts'] = $this->Access->readtable('kart')->result();

        #TRACK SECTION
        $data['track_content'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'track_content'))->row();
        $data['tracks'] = $this->Access->readtable('media','',array('media_page'=>'home','media_section'=>'track_img'))->result();
        
        #DIVIDER
        $data['div1'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'divider1'))->row();
        $data['div2'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'divider2'))->row();
        $data['div3'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'divider3'))->row();
        $data['div4'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'divider4'))->row();

        $data['current'] = "home";
        $data['lang'] = $lang = $this->session->userdata('fe_lang');
        $view['content'] = $this->load->view('backend/v_home',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }# func index

    public function save_opening(){
        $open = $this->input->post('open');
        $close = $this->input->post('close');

        $save = array(
            'general_title_en'=>$open,
            'general_title_in'=>$close,
            'general_date'=>date('Y-m-d H:i:s'),
            );

        $this->db->trans_begin();
        $this->Access->updatetable('general',$save,array('general_section'=>'operational_time'));
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $error_alert = "Update data error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('notif_operational'=>$notif));
            $this->session->mark_as_flash("notif_operational");
            redirect('backend/home');
        }else{
            $success_alert = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
            $this->session->set_userdata(array('notif_operational'=>$notif));
            $this->session->mark_as_flash("notif_operational");
            redirect('backend/home');
        }
    }

    public function save_video(){
        $id = $this->input->post('video_id');

        if($id != ''){
            $old_file = $this->Access->readtable('media','',array('media_page'=>'home','media_section'=>'video','media_id'=>$id))->row()->media_url;
        }

        if(isset($_FILES['video']['name'])){

            $media_video= $_FILES['video']['name'];
            $dt = date('dmYHis');
            $ex = pathinfo($media_video, PATHINFO_EXTENSION);
            $configVideo['upload_path'] = './assets/upload/video';
            $configVideo['allowed_types'] = 'avi|flv|wmv|mp4|mkv';
            $configVideo['max_size'] = 8192;
            $configVideo['overwrite'] = TRUE;
            $configVideo['remove_spaces'] = TRUE;
            $video_name = 'video_'.$dt.'.'.$ex;
            $configVideo['file_name'] = $video_name;

            $uploaddata = $video_name;

            if( ! file_exists( $configVideo['upload_path'] ) ){
                $create = mkdir($configVideo['upload_path'], 0777, TRUE);
                if( ! $create )
                    return;
            }

            $this->load->library('upload',$configVideo);
            $this->upload->initialize($configVideo);
            if(!$this->upload->do_upload('video')) {
                unlink( realpath( APPPATH.'../assets/upload/video/'.$video_name ));
                $error = $this->upload->display_errors();
                $notif = '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
                $send_notif = array('notif_video'=>$notif);
                $this->session->set_userdata($send_notif);
                $this->session->mark_as_flash('notif_video');
                redirect('backend/home#video');
            }else{
                $this->upload->data();
                $save_data = array(
                        'media_url'=>$video_name,
                        'media_page'=>'home',
                        'media_section'=>'video',
                        'media_date'=>date('Y-m-d H:i:s'),
                    );

                if($id == ""){
                    # this will be insert
                    $this->db->trans_begin();
                    $this->Access->inserttable('media',$save_data);
                    $this->db->trans_complete();
                }else{
                    # this willbe update
                    $this->db->trans_begin();
                    $this->Access->updatetable('media',$save_data,array('media_id'=>$id));
                    $this->db->trans_complete();
                    unlink( realpath( APPPATH.'../assets/upload/video/'.$old_file ));
                }

                if($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $error_alert = "Update data error!";
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
                    $this->session->set_userdata(array('notif_video'=>$notif));
                    $this->session->mark_as_flash("notif_video");
                    redirect('backend/home#video');
                }else{
                    $success_alert = "Update data success!";
                    $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
                    $this->session->set_userdata(array('notif_video'=>$notif));
                    $this->session->mark_as_flash("notif_video");
                    redirect('backend/home#video');
                }
            }
        }

    }# func save video

    public function save_video_poster(){
        $id = $this->input->post('video_poster_id');

        $gettoId = $this->db->query("SELECT * FROM media WHERE media_page = 'home' AND media_section = 'video_poster' AND media_id = '".$id."' ")->row()->media_url;

        $media_url  =   $_FILES['media_url']['name'];
        $break      =   explode('.', $media_url);
        $ext        =   strtolower($break[count($break) - 1]);
        $date       =   date('dmYHis');
        $media_url  =   'video_banner_'.$date.'.'.$ext;
        $path       =   './assets/upload/home';

        if( ! file_exists( $path ) ){
            $create = mkdir($path, 0777, TRUE);
            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createTemp )
                return;
        }
        
        $this->piclib->get_config($media_url, $path);
        if( $this->upload->do_upload('media_url') ){
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 ){
                unlink( realpath( APPPATH.'../assets/upload/home/'.$media_url ));
                unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$media_url ));
                $image_1920px = "Minimum image size 1920 x 939 px or bigger in the same ratio";
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1920px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait'){
                    unlink( realpath( APPPATH.'../assets/upload/home/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$media_url ));
                    $lands_square = "Image orientation must be 'landscape'";
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                }else{
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() ){
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                        
                        unlink( realpath( APPPATH.'../assets/upload/home/'.$gettoId ));
                        unlink( realpath(APPPATH.'../assets/upload/home/thumbnail/'.$gettoId ));

                        $media_url = array(
                            'media_url'=> $media_url);

                        $this->db->trans_begin();
                        $this->db->set('media_date', 'NOW()', FALSE);
                        $this->db->where('media_id', $id)->update('media',$media_url); 
                        $this->db->trans_complete();

                        if ($this->db->trans_status() === FALSE){
                            $this->db->trans_rollback();
                        }else{
                            $updd = "Updated successfully!";
                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                        }
                    }
                }
            }
        }else{
            $error = $this->upload->display_errors();
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
        }

        $this->session->set_flashdata('notif_video_poster', $notif);
        redirect($_SERVER['HTTP_REFERER']);

    }# func save video poster

    public function save_banner_text(){
        $caption_en = $this->input->post('banner_caption_en');
        $caption_in = $this->input->post('banner_caption_in');
        $content_en = $this->input->post('banner_content_en');
        $content_in = $this->input->post('banner_content_in');
        $id = $this->input->post('banner_id');

        $save_data = array(
                'general_title_en'=>$caption_en,
                'general_title_in'=>$caption_in,
                'general_content_en'=>$content_en,
                'general_content_in'=>$content_in,
                'general_page'=>'home',
                'general_section'=>'banner_text',
                'general_date'=>date('Y-m-d H:i:s'),
            );

        if($id == ''){
            #this will be insert
            $this->db->trans_begin();
            $this->Access->inserttable('general',$save_data);
            $this->db->trans_complete();
        }else{
            #this will be update
            $this->db->trans_begin();
            $this->Access->updatetable('general',$save_data, array('general_id'=>$id));
            $this->db->trans_complete();
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $error_alert = "Update data error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('notif_banner_text'=>$notif));
            $this->session->mark_as_flash("notif_banner_text");
            redirect('backend/home#banner_text');
        }else{
            $success_alert = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
            $this->session->set_userdata(array('notif_banner_text'=>$notif));
            $this->session->mark_as_flash("notif_banner_text");
            redirect('backend/home#banner_text');
        }
    }# func save banner text

    public function save_kart_content(){
        $content_en = $this->input->post('content_en');
        $content_in = $this->input->post('content_in');
        $id = $this->input->post('section_id');

        $save_data = array(
                'general_content_en'=>$content_en,
                'general_content_in'=>$content_in,
                'general_page'=>'home',
                'general_section'=>'kart_content',
                'general_date'=>date('Y-m-d H:i:s'),
            );

        if($id == ''){
            # this will be insert
            $this->db->trans_begin();
            $this->Access->inserttable('general',$save_data);
            $this->db->trans_complete();
        }else{
            # this will be update
            $this->db->trans_begin();
            $this->Access->updatetable('general',$save_data,array('general_id'=>$id));
            $this->db->trans_complete();
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $error_alert = "Update data error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('notif_kart_content'=>$notif));
            $this->session->mark_as_flash("notif_kart_content");
            redirect('backend/home#kart_content');
        }else{
            $success_alert = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
            $this->session->set_userdata(array('notif_kart_content'=>$notif));
            $this->session->mark_as_flash("notif_kart_content");
            redirect('backend/home#kart_content');
        }

    }# func save kart content

    public function save_kart(){
        $id = $this->input->post('kart_id');
        $kart_name = $this->input->post('kart_name');
        $spec_requirement = $this->input->post('spec_requirement');
        $spec_features = $this->input->post('spec_features');
        $spec_frame = $this->input->post('spec_frame');
        $spec_engine_volume = $this->input->post('spec_engine_volume');
        $spec_engine = $this->input->post('spec_engine');
        $spec_power = $this->input->post('spec_power');
        $spec_max_speed = $this->input->post('spec_max_speed');
        $kart_spec = $spec_requirement.";".
                     $spec_features.";".
                     $spec_frame.";".
                     $spec_engine_volume.";".
                     $spec_engine.";".
                     $spec_power.";".
                     $spec_max_speed
                    ;

        $old_file = $this->Access->readtable('kart','',array('kart_id'=>$id))->row()->kart_img;

        /*$z = explode(";",$kart_spec);
        echo "kart name = ".$kart_name."<br>";
        for($i=0;$i<7;$i++){
            echo $i." - ".$z[$i]."<br>";
        }*/
        if(($_FILES['kart_img']['name'])){
            $media_url  =   $_FILES['kart_img']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'kart_'.$date.'.'.$ext;
            $kart_img   =   $media_url;
            $path       =   './assets/upload/kart';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp ) return;
            }

            $this->piclib->get_config($media_url, $path, 1024000);
            if( $this->upload->do_upload('kart_img') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 500 || $height < 375 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/kart/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/kart/thumbnail/'.$media_url ));
                    $img_less = "The image resolution is below the minimum resolution allowed, please make sure the resolution is 500x375px";
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$img_less.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' || $orientation == 'square')
                    {
                        unlink( realpath( APPPATH.'../assets/upload/kart/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/kart/thumbnail/'.$media_url ));
                        $orientation = "The image orientation must be 'Portrait'";
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$orientation.'</div>';     
                    }else{
                        $this->piclib->resize_image($source_path, $width, $height, 500, 375);
                        if( $this->image_lib->resize() ){
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 500, 375, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/kart/'.$old_file ));
                            unlink( realpath(APPPATH.'../assets/upload/kart/thumbnail/'.$old_file ));

                            $save_data = array(
                                'kart_img'=>$kart_img,
                                'kart_name'=>$kart_name,
                                'kart_spec'=>$kart_spec,
                                'kart_date'=>date('Y-m-d H:i:s'),
                                );

                            $this->db->trans_begin();
                            $this->Access->updatetable('kart',$save_data,array('kart_id'=>$id));
                            $this->db->trans_complete();

                            if($this->db->trans_status() === FALSE){
                                $this->db->trans_rollback();
                                $error_alert = "Update data error!";
                                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
                            }else{
                                $success_alert = "Update data success!";
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
                            }

                        }
                    }
                }
            }
        }else{
            $save_data = array(
                'kart_name'=>$kart_name,
                'kart_spec'=>$kart_spec,
                'kart_date'=>date('Y-m-d H:i:s'),
                );

            $this->db->trans_begin();
            $this->Access->updatetable('kart',$save_data,array('kart_id'=>$id));
            $this->db->trans_complete();

            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $error_alert = "Update data error!";
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            }else{
                $success_alert = "Update data success!";
                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
            }
        }

        $this->session->set_userdata(array('notif_karts'.$id=>$notif));
        $this->session->mark_as_flash("notif_karts".$id);
        redirect(base_url('backend/home#karts'));

    }# func save kart

    public function save_track_content(){
        $content_en = $this->input->post('content_en');
        $content_in = $this->input->post('content_in');
        $id = $this->input->post('section_id');

        $save_data = array(
                'general_content_en'=>$content_en,
                'general_content_in'=>$content_in,
                'general_page'=>'home',
                'general_section'=>'track_content',
                'general_date'=>date('Y-m-d H:i:s'),
            );

        if($id == ''){
            # this will be insert
            $this->db->trans_begin();
            $this->Access->inserttable('general',$save_data);
            $this->db->trans_complete();
        }else{
            # this will be update
            $this->db->trans_begin();
            $this->Access->updatetable('general',$save_data,array('general_id'=>$id));
            $this->db->trans_complete();
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $error_alert = "Update data error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('notif_track_content'=>$notif));
            $this->session->mark_as_flash("notif_track_content");
            redirect('backend/home#track_content');
        }else{
            $success_alert = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
            $this->session->set_userdata(array('notif_track_content'=>$notif));
            $this->session->mark_as_flash("notif_track_content");
            redirect('backend/home#track_content');
        }
    }# func save track content

    public function save_track(){
        $id = $this->input->post('track_id');
        $track_distance = $this->input->post('track_distance');

        $old_file = $this->Access->readtable('media','',array('media_page'=>'home','media_section'=>'track_img','media_id'=>$id))->row()->media_url;

        $track_img = $old_file;

        if(isset($_FILES['track_img']['name'])){
            $media_url  =   $_FILES['track_img']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'track_'.$date.'.'.$ext;
            $track_img  =   $media_url;
            $path       =   './assets/upload/track';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp ) return;
            }

            $this->piclib->get_config($media_url, $path, 1024000);
            if( $this->upload->do_upload('track_img') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 250 || $height < 250 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/track/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/track/thumbnail/'.$media_url ));
                    $img_less = "The image resolution is below the minimum resolution allowed, please make sure the resolution is 250x250px";
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$img_less.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' || $orientation == 'landscape')
                    {
                        unlink( realpath( APPPATH.'../assets/upload/track/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/track/thumbnail/'.$media_url ));
                        $orientation = "The image orientation must be 'Square'";
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$orientation.'</div>';     
                    }else{
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250);
                        if( $this->image_lib->resize() ){
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/track/'.$old_file ));
                            unlink( realpath(APPPATH.'../assets/upload/track/thumbnail/'.$old_file ));

                        }
                    }
                }
            }
        }

        $save_data = array(
                    'media_url'=>$track_img,
                    'media_content_en'=>$track_distance,
                    'media_page'=>'home',
                    'media_section'=>'track_img',
                    'media_date'=>date('Y-m-d H:i:s'),
                );

            $this->db->trans_begin();
            $this->Access->updatetable('media',$save_data,array('media_id'=>$id));
            $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $error_alert = "Update data error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('notif_tracks'.$id=>$notif));
            $this->session->mark_as_flash("notif_tracks".$id);
            redirect(base_url('backend/home#tracks'));
        }else{
            $success_alert = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
            $this->session->set_userdata(array('notif_tracks'.$id=>$notif));
            $this->session->mark_as_flash("notif_tracks".$id);
            redirect(base_url('backend/home#tracks'));
        }

    }# func save track img

    public function save_divider(){
        $id = $this->input->post('div_id');
        $div_en = $this->input->post('div_en');
        $div_in = $this->input->post('div_in');
        $div_section = $this->input->post('div_section');

        $save_data = array(
                'general_content_en'=>$div_en,
                'general_content_in'=>$div_in,
                'general_page'=>'home',
                'general_date'=>date('Y-m-d H:i:s'),
            );
        $this->db->trans_begin();
        $this->Access->updatetable('general',$save_data,array('general_id'=>$id));
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $error_alert = "Update data error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('notif_div'.$div_section=>$notif));
            $this->session->mark_as_flash("notif_div".$div_section);
            redirect(base_url('backend/home#div'.$div_section));
        }else{
            $success_alert = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
            $this->session->set_userdata(array('notif_div'.$div_section=>$notif));
            $this->session->mark_as_flash("notif_div".$div_section);
            redirect(base_url('backend/home#div'.$div_section));
        }

    }# func save divider

}

