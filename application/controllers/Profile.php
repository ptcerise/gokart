<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $fe_lang = $this->session->userdata('fe_lang');
            $this->load->model('Access');
            if ($fe_lang) {
                $this->lang->load('gokart_frontend',$fe_lang);
            } else {
                $this->lang->load('gokart_frontend','english');
            }
    } 
    
    public function index()
    {
        if($this->session->userdata('pilot_status_login') == FALSE){
            redirect('sign_in');
        }

        $id = $this->session->userdata('pilot_id');
        $data['data_profile'] = $this->Access->readtable('profile','',array('temp_id'=>$id))->row();
        $data['data_user'] = $this->Access->readtable('user','',array('user_id'=>$id))->row();

        $data['user_stat'] = $this->Access->readtable('statistic','',array('temp_id'=>$id))->row();
        $data['user_best_lap'] = $this->db->query("SELECT * FROM race WHERE race_pilot_id='".$id."' ORDER BY race_best_lap ASC")->row();
        $data['user_fastest_race'] = $this->db->query("SELECT * FROM race WHERE race_pilot_id='".$id."' ORDER BY race_time ASC")->row();

        $data['friend_list'] = $this->db->query("SELECT * FROM statistic WHERE temp_id!='".$id."'")->result();
        
        $data['lang']    = $this->session->userdata('fe_lang');
        $data['language']    = $this->session->userdata('fe_lang');

        $data['my_last'] = $this->db->query("SELECT race_date FROM race WHERE race_pilot_id='".$id."' GROUP BY race_date ORDER BY race_date DESC")->result();
        $fastest_alltime = $this->db->query("SELECT * FROM race ORDER BY race_best_lap ASC LIMIT 1")->row()->race_pilot_id;
        $data['fastest_alltime'] = $this->Access->readtable('user','',array('user_id'=>$fastest_alltime))->row()->user_name;
        $data['fastest_lap_alltime'] = $this->db->query("SELECT race_best_lap FROM race ORDER BY race_best_lap ASC LIMIT 1")->row()->race_best_lap;

        //THE COMMUNITY
        
        // #EVENT
        $date_now = date('Y-m-d H:i:s');
        $data['events'] = $this->db->query('SELECT * FROM event WHERE event_date >= "'.$date_now.'" ORDER BY event_date ASC')->result();
        // #CHAMPIONSHIP
        $data['champ'] = $this->db->query("SELECT * FROM championship GROUP BY champ_date ORDER BY champ_date DESC LIMIT 1")->row();

        // #ALL STAR PILOT
        // 3 TOP PILOT
        $data['top_rank'] = $this->db->query("SELECT * FROM statistic ORDER BY stat_rank ASC LIMIT 3")->result();

        // #LAST RACE
        $data['last_pro'] = $this->db->query("SELECT race_date FROM race WHERE race_type='pro' GROUP BY race_date ORDER BY race_date DESC")->result();
        $data['last_casual'] = $this->db->query("SELECT race_date FROM race WHERE race_type='casual' GROUP BY race_date ORDER BY race_date DESC")->result();

        $fastest_lap_pro = $this->db->query("SELECT * FROM race  WHERE race_type='pro' ORDER BY race_best_lap ASC")->row();
        $fastest_lap_casual = $this->db->query("SELECT * FROM race  WHERE race_type='casual' ORDER BY race_best_lap ASC")->row();

        $data['the_fastest_pro'] = $this->db->query("SELECT * FROM user WHERE user_id='".$fastest_lap_pro->race_pilot_id."'")->row()->user_name;
        $data['the_fastest_casual'] = $this->db->query("SELECT * FROM user WHERE user_id='".$fastest_lap_casual->race_pilot_id."'")->row()->user_name;

        $data['fastest_time_pro'] = $fastest_lap_pro->race_best_lap;
        $data['fastest_time_casual'] = $fastest_lap_casual->race_best_lap;

        //SOCMED
        $data['socmed_tripadvisor'] = $this->Access->readtable('general','',array('general_section'=>'socmed_tripadvisor'))->row()->general_content_en;
        $data['socmed_facebook'] = $this->Access->readtable('general','',array('general_section'=>'socmed_facebook'))->row()->general_content_en;
        $data['socmed_twitter'] = $this->Access->readtable('general','',array('general_section'=>'socmed_twitter'))->row()->general_content_en;
        $data['socmed_instagram'] = $this->Access->readtable('general','',array('general_section'=>'socmed_instagram'))->row()->general_content_en;
        $data['socmed_youtube'] = $this->Access->readtable('general','',array('general_section'=>'socmed_youtube'))->row()->general_content_en;

        $view['location'] = $this->Access->readtable('general','',array('general_section'=>'location'))->row()->general_content_en;
        $view['content']   = $this->load->view('gokart/v_profile',$data,TRUE);
        $this->load->view('gokart/v_master',$view);
    }

    public function update_profile(){
        $profile_id = $this->input->post('profile_id');
        $profile_phone = $this->input->post('user_phone_number');
        $profile_pin_bb = $this->input->post('user_pin_bb');
        $profile_address = $this->input->post('user_address');
        $profile_postcode = $this->input->post('user_postcode');
        $profile_city = $this->input->post('user_city');

        $save_data = array(
                'profile_phone'=>$profile_phone,
                'profile_pin_bb'=>$profile_pin_bb,
                'profile_address'=>$profile_address,
                'profile_postcode'=>$profile_postcode,
                'profile_city'=>$profile_city,
            );

        #print_r($save_data);

        
        $this->db->trans_begin();
        $this->Access->updatetable('profile',$save_data ,array('profile_id'=>$profile_id));
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $error_alert = "Update data error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$error_alert.'</div>';
            $this->session->set_userdata(array('upd_profile'=>$notif));
            $this->session->mark_as_flash("upd_profile");
            redirect(base_url('gokart/profile'));
        }else{
            $success_alert = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$success_alert.'</div>';
            $this->session->set_userdata(array('upd_profile'=>$notif));
            $this->session->mark_as_flash("upd_profile");
            redirect(base_url('profile'));
        }
    }

    // function cek(){
    //     session_destroy();
    // }

}
