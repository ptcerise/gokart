<?php

# MASTER HOME
$lang["the_karts"] = "The Karts";
$lang["the_track"] = "The Track";
$lang["the_community"] = "The Community";
$lang["gallery"] = "Gallery";
$lang["profile"] = "Profile";
$lang["sign_in"] = "Sign in";
$lang["sign_out"] = "Sign out";
$lang["tickets"] = "Tickets";
$lang["contact"] = "Contact";
$lang["location"] = "Location";
$lang["park_open"] = "The park is open 365 days";
$lang["today"] = "Today";
$lang["to"] = "to";
$lang["footer_visitors"] = "Visitors : ";

# BUTTONS
$lang["btn_choose"] = "Choose a Package";
$lang["btn_share"] = "Share";
$lang["btn_discover"] = "Discover";
$lang["btn_have_fun"] = "Let's Have Some Fun!";
$lang["btn_corp_website"] = "Corporate Website";
$lang["btn_showmore"] = "Show More";
$lang["btn_become_member"] = "Become a Member";
$lang["btn_corporate"] = "Corporate";
$lang["btn_close"] = "Close";

# HOME
$lang["all_star"] = "All star drivers";
$lang["championship"] = "Championship";
$lang["last_pro_race"] = "Last advance race";
$lang["last_casual_race"] = "Last rookie race";
$lang["next_events"] = "Next events";
$lang['share_on']		= 'Share On';

# CARDS
$lang["requirement"] = "Requirement";
$lang["features"] = "Features";
$lang["frame"] = "Frame";
$lang["engine_volume"] = "Engine volume";
$lang["engine"]	= "Engine";
$lang["power"] = "Power";
$lang["max_speed"] = "Max speed";
$lang["victories"] = "Victories";
$lang["best_lap"] = "Best lap";
$lang["fastest_race"] = "Fastest race";
$lang["pilot_name"] = "Driver's name";
$lang["races"] = "Races";
$lang["points"] = "Points";
$lang["behind"] = "Podium";
$lang["results"] = "RESULTS";
$lang["fastest_lap"] = "FASTEST LAP";

# PROFILE
$lang["welcome"] = "Welcome to the stands";
$lang["public_profile"] = "Public profile";
$lang["personal_info"] = "Personal info";
$lang["my_past_race"] = "My past race";

## LABEL INPUT
$lang["search"] = "Search a friend";
$lang["phone"] = "Phone";
$lang["pin_bb"] = "Pin BB";
$lang["address"] = "Address";
$lang["post_code"] = "Post code";
$lang["city"] = "City";

## BUTTON
$lang["btn_edit"] = "Edit";
$lang["btn_save"] = "Save";
$lang["btn_cancel"] = "Cancel";

# CONTACT
$lang['contact_us'] = "Contact us";
$lang['contact_name'] = "Name";
$lang['contact_email'] = "E-Mail";
$lang['contact_phone'] = "Phone";
$lang['contact_subject'] = "Subject";
$lang['contact_message'] = "Message";
$lang['contact_send'] = "Send";