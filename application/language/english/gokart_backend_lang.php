
<?php
// sidbar
$lang['main_menu']		= 'Main Menu';
$lang['home'] 			= 'Home';
$lang['community']		= 'Community';
$lang['events']			= 'Events';
$lang['races']			= 'Races';
$lang['championship']	= 'Championship';
$lang['member']			= 'Member';
$lang['gallery']		= 'Gallery';
$lang['contact']		= 'Contact';
$lang['user_management']= 'User Management';

// content
$lang['action']			= 'Action';
$lang['title_en'] 		= 'Title English';
$lang['title_in']		= 'Title Indonesia';
$lang['content_en'] 	= 'Content English';
$lang['content_in'] 	= 'Content Indonesia';
$lang['caption_en']		= 'Caption English';
$lang['caption_in']		= 'Caption Indonesia';
$lang['choose_file']	= 'Choose File';
$lang['choose_video']	= 'Choose Video';
$lang['choose_image'] 	= 'Choose Image';
$lang['divider1_en'] 	= 'Divider English';
$lang['divider1_in'] 	= 'Divider Indonesia';

// btn
$lang['btn_edit'] 		= 'Edit';
$lang['btn_detail'] 	= 'Detail';
$lang['btn_cancel'] 	= 'Cancel';
$lang['btn_add'] 		= 'Add';
$lang['btn_update'] 	= 'Update';
$lang['btn_delete'] 	= 'Delete';
$lang['btn_save'] 		= 'Save';
$lang['btn_close']		= 'Close';

// upload rules
$lang['max_size']		= 'maximum file size';
$lang['file_format']	= 'file format';
$lang['resolution']		= 'minimum resolution';
$lang['orientation']	= 'the orientation is';

// home
$lang['operational']	= "Today's Operational Time";
$lang['open']			= 'Open';
$lang['close']			= 'Close';
$lang['home_banner']	= 'Home Banner';
$lang['video']			= 'Video';
$lang['video_poster']	= 'Video Poster';
$lang['banner_caption'] = 'Banner Caption';
$lang['divider']		= 'Divider';
$lang['div1']			= 'Divider 1 (Choose Package)';
$lang['div2']			= 'Divider 2 (Lets Have Some Fun)';
$lang['div3']			= 'Divider 3 (Corporate Website)';
$lang['div4']			= 'Divider 4 (Join Member)';
$lang['content']		= 'Content';
$lang['the_karts']		= 'The Karts';
$lang['karts']			= 'Karts';
$lang['the_tracks']		= 'The Tracks';
$lang['tracks']			= 'Tracks';
$lang['share_on']		= 'Share On';
//// update kart
$lang['update_kart']	= 'Update Kart';
$lang['kart_image']		= 'Kart Image';
$lang['kart_name']		= 'Kart Name';
$lang['requirement']	= 'Requirement';
$lang['features']		= 'Features';
$lang['frame']			= 'Frame';
$lang['engine_volume']	= 'Engine Volume';
$lang['engine']			= 'Engine';
$lang['power']			= 'Power';
$lang['max_speed']		= 'Max Speed';

//// update tracks
$lang['distance']		= 'Distance';

// EVENTS
$lang['event_name']		= 'Event Name';
$lang['date']			= 'Date';

// RACE
$lang['race_type']		= 'Race Type';
$lang['racers']			= 'Racer(s)';
$lang['winner']			= 'Winner';
$lang['pilot_name']		= 'Driver Name';
$lang['race_time']		= 'Race Time';
$lang['best_lap']		= 'Best Lap';
$lang['additional_info']= 'Additional Info';
$lang['pro_race_rec']	= 'Advance Race Record';
$lang['casual_race_rec']= 'Rookie Race Record';
$lang['input_race']		= 'Input Race Result';

// CHAMPIONSHIP
$lang['input_champ']	= 'Input Championship Result';
$lang['race_count']		= 'Race Count';
$lang['points']			= 'Point(s)';
$lang['year']			= 'Year';

//MEMBER
$lang['email']			= 'E-Mail';
$lang['phone']			= 'Phone';
$lang['pinbb']			= 'Pin BB';
$lang['address']		= 'Address';
$lang['postcode']		= 'Post Code';
$lang['city']			= 'City';
$lang['pass']			= 'Password';
$lang['pass_confirm']	= 'Confirm Password';
$lang['show_pass']		= 'Show Password';
$lang['photo']			= 'Photo';

//GALLERY
$lang['upload_image']	= 'Upload Image';
$lang['image_prev']		= 'Image Preview';

//CONTACT
$lang['messages']		= 'Messages';
$lang['from']			= 'From';
$lang['category']		= 'Category';
$lang['subject']		= 'Subject';
$lang['status']			= 'Status';
$lang['phone_number']	= 'Phone Number';
$lang['address']		= 'Address';
$lang['read']			= 'Read';
$lang['unread']			= 'Unread';
$lang['gmap_link']		= 'Google Map Link';
$lang['info']			= 'Info';

//USER
$lang['user']			= 'User';
$lang['user_name']		= 'User Name';
$lang['user_fname']		= 'User Full Name';
$lang['level']			= 'Level';