
<?php
// sidbar
$lang['main_menu']		= 'Menu Utama';
$lang['home'] 			= 'Beranda';
$lang['community']		= 'Komunitas';
$lang['events']			= 'Even';
$lang['races']			= 'Balapan';
$lang['championship']	= 'Kejuaraan';
$lang['member']			= 'Member';
$lang['gallery']		= 'Galeri';
$lang['contact']		= 'Kontak';
$lang['user_management']= 'Manajemen Pengguna';

// content
$lang['action']			= 'Aksi';
$lang['title_en'] 		= 'Title Inggris';
$lang['title_in']		= 'Title Indonesia';
$lang['content_en'] 	= 'Konten Inggris';
$lang['content_in'] 	= 'Konten Indonesia';
$lang['caption_en']		= 'Caption Inggris';
$lang['caption_in']		= 'Caption Indonesia';
$lang['choose_file']	= 'Pilih File';
$lang['choose_video']	= 'Pilih Video';
$lang['choose_image'] 	= 'Pilih Gambar';
$lang['divider1_en'] 	= 'Divider Inggris';
$lang['divider1_in'] 	= 'Divider Indonesia';

// btn
$lang['btn_edit'] 		= 'Ubah';
$lang['btn_detail'] 	= 'Detail';
$lang['btn_cancel'] 	= 'Batal';
$lang['btn_add'] 		= 'Tambah';
$lang['btn_update'] 	= 'Perbarui';
$lang['btn_delete'] 	= 'Hapus';
$lang['btn_save'] 		= 'Simpan';
$lang['btn_close']		= 'Tutup';

// upload rules
$lang['max_size']		= 'ukuran file maksimal';
$lang['file_format']	= 'format file';
$lang['resolution']		= 'resolusi minimal';
$lang['orientation']	= 'orientasi gambar';

// home
$lang['operational']	= "Jam Operasional Hari Ini";
$lang['open']			= 'Buka';
$lang['close']			= 'Tutup';
$lang['home_banner']	= 'Banner Beranda';
$lang['video']			= 'Video';
$lang['video_poster']	= 'Video Poster';
$lang['banner_caption'] = 'Caption Banner';
$lang['divider']		= 'Divider';
$lang['div1']			= 'Divider 1 (Pilih Paket)';
$lang['div2']			= 'Divider 2 (Mari bersenang-senang)';
$lang['div3']			= 'Divider 3 (Website Korporasi)';
$lang['div4']			= 'Divider 4 (Gabung Member)';
$lang['content']		= 'Konten';
$lang['the_karts']		= 'Kart';
$lang['karts']			= 'Kart';
$lang['the_tracks']		= 'Lintasan';
$lang['tracks']			= 'Lintasan';
$lang['share_on']		= 'Bagikan Di';
//// update kart
$lang['update_kart']	= 'Perbarui Kart';
$lang['kart_image']		= 'Gambar Kart';
$lang['kart_name']		= 'Nama Kart';
$lang['requirement']	= 'Kebutuhan';
$lang['features']		= 'Fitur';
$lang['frame']			= 'Frame';
$lang['engine_volume']	= 'Volume Mesin';
$lang['engine']			= 'Mesin';
$lang['power']			= 'Tenaga';
$lang['max_speed']		= 'Kecepatan Maksimal';

//// update tracks
$lang['distance']		= 'Panjang Lintasan';

// EVENTS
$lang['event_name']		= 'Nama Even';
$lang['date']			= 'Tanggal';

// RACE
$lang['race_type']		= 'Tipe Balapan';
$lang['racers']			= 'Pembalap';
$lang['winner']			= 'Juara';
$lang['pilot_name']		= 'Nama Driver';
$lang['race_time']		= 'Waktu Balapan';
$lang['best_lap']		= 'Putaran Terbaik';
$lang['additional_info']= 'Info Tambahan';
$lang['pro_race_rec']	= 'Catatan Balapan Advance';
$lang['casual_race_rec']= 'Catatan Balapan Pemula';
$lang['input_race']		= 'Input Hasil Balapan';

// CHAMPIONSHIP
$lang['input_champ']	= 'Input Hasil Kejuaraan';
$lang['race_count']		= 'Jumlah Balapan';
$lang['points']			= 'Poin';
$lang['year']			= 'Tahun';

//MEMBER
$lang['email']			= 'E-Mail';
$lang['phone']			= 'Telepon';
$lang['pinbb']			= 'Pin BB';
$lang['address']		= 'Alamat';
$lang['postcode']		= 'Kode Pos';
$lang['city']			= 'Kota';
$lang['pass']			= 'Kata Sandi';
$lang['pass_confirm']	= 'Konfirmasi Kata Sandi';
$lang['show_pass']		= 'Tampilkan Kata Sandi';
$lang['photo']			= 'Foto';

//GALLERY
$lang['upload_image']	= 'Unggah Gambar';
$lang['image_prev']		= 'Tampilan Gambar';

//CONTACT
$lang['messages']		= 'Pesan';
$lang['from']			= 'Dari';
$lang['category']		= 'Kategori';
$lang['subject']		= 'Subyek';
$lang['status']			= 'Status';
$lang['phone_number']	= 'No Telepon';
$lang['address']		= 'Alamat';
$lang['read']			= 'Sudah Dibaca';
$lang['unread']			= 'Belum Dibaca';
$lang['gmap_link']		= 'Link Google Map';
$lang['info']			= 'Info';

//USER
$lang['user']			= 'Pengguna';
$lang['user_name']		= 'Nama Pengguna';
$lang['user_fname']		= 'Nama Lengkap Pengguna';
$lang['level']			= 'Level';