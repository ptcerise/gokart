<?php

# MASTER HOME
$lang["the_karts"] = "Kart";
$lang["the_track"] = "Lintasan";
$lang["the_community"] = "Komunitas";
$lang["gallery"] = "Galeri";
$lang["profile"] = "Profil";
$lang["sign_in"] = "Masuk";
$lang["sign_out"] = "Keluar";
$lang["tickets"] = "Tiket";
$lang["contact"] = "Kontak";
$lang["location"] = "Lokasi";
$lang["park_open"] = "Taman buka 365 hari";
$lang["today"] = "Hari ini";
$lang["to"] = " - ";
$lang["footer_visitors"] = "Pengunjung : ";

# BUTTONS
$lang["btn_choose"] = "Pilih Paket";
$lang["btn_share"] = "Bagikan";
$lang["btn_discover"] = "Temukan";
$lang["btn_have_fun"] = "Mari bersenang-senang !";
$lang["btn_corp_website"] = "Website Korporasi";
$lang["btn_showmore"] = "Tampilkan lebih banyak";
$lang["btn_become_member"] = "Menjadi member";
$lang["btn_corporate"] = "Korporasi";
$lang["btn_close"] = "Tutup";

# HOME
$lang["all_star"] = "Driver terbaik";
$lang["championship"] = "Kejuaraan";
$lang["last_pro_race"] = "Balap advance";
$lang["last_casual_race"] = "Balap pemula";
$lang["next_events"] = "Event selanjutnya";
$lang['share_on']		= 'Bagikan Di';

# CARDS
$lang["requirement"] = "Kebutuhan";
$lang["features"] = "Fitur";
$lang["frame"] = "Frame";
$lang["engine_volume"] = "Volume mesin";
$lang["engine"]	= "Mesin";
$lang["power"] = "Tenaga";
$lang["max_speed"] = "Kecepatan maksimal";
$lang["victories"] = "Menang";
$lang["best_lap"] = "Putaran terbaik";
$lang["fastest_race"] = "Balap tercepat";
$lang["pilot_name"] = "Nama Driver";
$lang["races"] = "Balapan";
$lang["points"] = "Poin";
$lang["behind"] = "Podium";
$lang["results"] = "HASIL";
$lang["fastest_lap"] = "PUTARAN TERCEPAT";

# PROFILE
$lang["welcome"] = "Selamat datang di tribun!";
$lang["public_profile"] = "Profil umum";
$lang["personal_info"] = "Info pribadi";
$lang["my_past_race"] = "Balapan terakhir";

## LABEL INPUT
$lang["search"] = "Temukan teman";
$lang["phone"] = "Telepon";
$lang["pin_bb"] = "Pin BB";
$lang["address"] = "Alamat";
$lang["post_code"] = "Kode pos";
$lang["city"] = "Kota";

## BUTTON
$lang["btn_edit"] = "Ubah";
$lang["btn_save"] = "Simpan";
$lang["btn_cancel"] = "Batal";

# CONTACT
$lang['contact_us'] = "Hubungi kami";
$lang['contact_name'] = "Nama";
$lang['contact_email'] = "E-Mail";
$lang['contact_phone'] = "Nomor Telepon";
$lang['contact_subject'] = "Subyek";
$lang['contact_message'] = "Pesan";
$lang['contact_send'] = "Kirim";