<!-- GET IP VISITORS -->
<?php
    $ip_address = $_SERVER['REMOTE_ADDR'];
    $date       = date('Y-m-d');

    if (!isset($_COOKIE["visitor"]))
    {
        setcookie("visitor", "$ip_address", time() +3600);
        // mysql_connect("localhost", "user", "password"); //sesuaikan host, user, dan password-nya !
        // mysql_select_db(“nama_db”) or die(mysql_error()); //sesuaikan nama database-nya

        $this->db->query("INSERT INTO visitors VALUES ('$ip_address', '$_SERVER[HTTP_USER_AGENT]', '$date')");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url('assets/img/favicon.ico');?>">

    <title>Yogya Gokart</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Oleo+Script:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Racing+Sans+One' rel='stylesheet' type='text/css'>
    <link  href='<?php echo base_url('assets/css/unite-gallery.css');?>' rel='stylesheet' type='text/css' />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link type="text/css" href="<?php echo base_url('assets/css/styles.min.css');?>" rel="stylesheet">
</head>

<body>
<?php include_once("analyticstracking.php") ?>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container nav-container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a class="logo" href="<?php echo base_url(); ?>">
                        <img src="<?php echo base_url('assets/img/logo-gokart.png');?>" alt="logo" class="img-circle hidden-xs logo-header">
                    </a>
                </div>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="row narrow-row">
                    <ul class="nav navbar-nav navbar-right nb-second">
                        <li>
                            <a class="nb-second" href="<?php echo base_url('contact_us'); ?>"><?php echo $this->lang->line("contact"); ?></a>
                        </li>
                        <li>
                            <a class="nb-second" target="_blank" href="<?php echo strip_tags($location); ?>"><?php echo $this->lang->line("location"); ?></a>
                        </li>
                        <li>
                            <a class="nb-second" href="https://kidsfun.co.id/tickets"><i class="fa fa-shopping-cart fa-lg"></i></a>
                        </li>
                        <li id="langflag">
                            <?php if($lang == 'indonesia'): ?>
                                <a href="<?php echo base_url('languageSwitcher/switchLang/english');?>">
                                    <img alt="EN" src="<?php echo site_url('assets/icon/english.png'); ?>">
                                </a>
                            <?php else:?>
                                
                                <a href="<?php echo base_url('languageSwitcher/switchLang/indonesia');?>">
                                    <img alt="ID" src="<?php echo site_url('assets/icon/Indonesia.png'); ?>">
                                </a>
                            <?php endif; ?>
                                
                        </li>
                    </ul>
                </div>
                <div class="row narrow-row">
                    <ul class="nav navbar-nav navbar-right nb-main">
                        <li class="visible-xs">
                            <a href="<?php echo base_url(); ?>" ><i class="fa fa-home" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>#thekarts_anchor" class="nb-main">
                                <?php echo $this->lang->line("the_karts"); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>#thetrack_anchor" class="nb-main">
                                <?php echo $this->lang->line("the_track"); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>#thecommunity_anchor" class="nb-main">
                                <?php echo $this->lang->line("the_community"); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>#gallery_anchor" class="nb-main">
                                <?php echo $this->lang->line("gallery"); ?>
                            </a>
                        </li>
                        <?php 
                                $login = $this->session->userdata('pilot_status_login');
                                if(!empty($login)){
                                    echo "<li><a href='".base_url()."profile' class='nb-main green'>".$this->lang->line('profile')."</a></li>";
                                }
                            ?>
                        <li>
                            <?php 
                                $login = $this->session->userdata('pilot_status_login');
                                if(empty($login)){
                                    echo "<a href='".base_url()."sign_in' class='nb-main green'>".$this->lang->line('sign_in')."</a>";
                                }else{
                                    echo "<a href='".base_url()."sign_in/sign_out' class='nb-main green'>".$this->lang->line('sign_out')."</a>";
                                }
                            ?>
                        </li>
                        <li>
                            <a href="https://kidsfun.co.id/tickets" class="nb-main green">
                                <?php echo $this->lang->line("tickets"); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    <main>
        <?php echo $content; ?>
    </main>

    <!-- Footer -->
    <div class="panel bg-dark-grey divider-pattern"></div>
    
    <footer class="bg-dark-grey">

        <nav class="container">
            <div class="row footer-top">
                <div class="col-xs-12 footer-top-main">
                    <ul class="nav-bottom">
                        <li><a href="<?php echo base_url(); ?>#thekarts_anchor">
                            <?php echo $this->lang->line("the_karts"); ?>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>#thetrack_anchor">
                            <?php echo $this->lang->line("the_track"); ?>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>#thecommunity_anchor">
                            <?php echo $this->lang->line("the_community"); ?>
                        </a></li>
                        <li><a href="<?php echo base_url(); ?>#gallery_anchor">
                            <?php echo $this->lang->line("gallery"); ?>
                        </a></li>
                        <li>
                            <?php 
                                $login = $this->session->userdata('pilot_status_login');
                                if(empty($login)){
                                    echo "<a href='".base_url()."sign_in' class='nb-main green'>".$this->lang->line('sign_in')."</a>";
                                }else{
                                    echo "<a href='".base_url()."profile/sign_out' class='nb-main green'>".$this->lang->line('sign_out')."</a>";
                                }
                            ?>
                        </li>
                        <li><a class="green" href="https://kidsfun.co.id/tickets">
                            <?php echo $this->lang->line("tickets"); ?>
                        </a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-lg-11 col-lg-offset-1 footer-top-second">
                    
                    <div class="ghost col-xs-12 col-lg-6">
                        <button title="kidsfun.co.id" onclick="window.location.href='https://kidsfun.co.id/'" class="btn ghost-btn-footer">KIDS FUN</button>
                        <button title="produkrekreasi.com" onclick="window.location.href='https://produkrekreasi.com/'" class="btn ghost-btn-footer">
                            <?php echo $this->lang->line("btn_corporate"); ?>
                        </button>
                    </div>
                    <div class="socmed col-xs-12 col-lg-6">
                        <a href="http://<?php echo $socmed_tripadvisor ?>" target="_blank" class="socmed-footer">
                            <img alt="tripadvisor" src="<?php echo base_url('assets/icon/icon-tripadvisor.png'); ?>">
                        </a>  
                        <a href="http://<?php echo $socmed_facebook ?>" target="_blank" class="socmed-footer">
                            <img alt="facebook" src="<?php echo base_url('assets/icon/icon-facebook.png'); ?>">
                        </a>
                         <a href="http://<?php echo $socmed_twitter ?>" target="_blank" class="socmed-footer">
                            <img alt="twitter" src="<?php echo base_url('assets/icon/icon-twitter.png'); ?>">
                         </a>
                         <a href="http://<?php echo $socmed_instagram ?>" target="_blank" class="socmed-footer">
                            <img alt="instagram" src="<?php echo base_url('assets/icon/icon-instagram.png'); ?>">
                         </a>
                         <a href="http://<?php echo $socmed_youtube ?>" target="_blank" class="socmed-footer">
                            <img alt="youtube" src="<?php echo base_url('assets/icon/icon-youtube.png'); ?>">
                         </a>
                    </div>
                    <div class="visitors col-xs-12 col-lg-6">
                        <!-- VISITORS COUNTER -->
                        <h4 class="display">
                            <?php 
                                echo $this->lang->line("footer_visitors");

                                $visitors = $this->db->query("SELECT * FROM visitors");
                                echo $visitors->num_rows();
                            ?>
                        </h4>
                    </div>
                </div>
            </div>
        </nav>
        
        <div class="row footer-bottom">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="footer-left">
                    <p class="copyright text-muted">&copy; Kids Fun 2016 - all right reserved </p>
                </div>
                <div class="footer-right">
                    <p class="copyright text-muted"> A concept by <img alt="Cerise" src="<?php echo base_url('assets/icon/logo-cerise.png');?>"></p>
                </div> 
            </div>
        </div>
    
    </footer>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <script src='<?php echo base_url('assets/js/unitegallery.min.js')?>' type='text/javascript' ></script>
    <script src='<?php echo base_url('assets/js/ug-theme-tiles.js')?>' type='text/javascript' ></script>
    <!-- TO USE BASE_URL IN JS FILE -->
    <script type="text/javascript">
        var js_base_url = function( urlText ){
        var urlTmp = "<?php echo base_url('" + urlText + "'); ?>";
        return urlTmp;
        }

        $(document).ready(function(){
            $("#gallery").unitegallery({
                gallery_theme: "tiles",
            });
        });
    </script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/kidsfun/clean.js')?>"></script>
    <!-- FOR THE GALLERY -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/scripts.js')?>"></script>
</body>
</html>
