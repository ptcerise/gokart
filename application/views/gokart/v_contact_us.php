<div class=" bg-asphalt">
<!-- THE CONTENT -->
<div id="main" class="container">
    <header class="row main-header">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <h2 class="display text-center">
                <?php echo $this->lang->line('contact_us'); ?>
            </h2>
            <div class="par lead text-center">
                <?php if($lang == "indonesia"){
                    echo !empty($content->general_content_in) ? $content->general_content_in:"[KOSONG!]";
                }else{
                    
                    echo !empty($content->general_content_en) ? $content->general_content_en:"[EMPTY!]";
                } ?> 
            </div>
        </div>
    </header>
    
    <div class="row" id="form">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 padding-clear">
            <!-- CONTACT FORM -->
            <div class="col-xs-12 col-sm-6 padding-clear">
                <div class="panel panel-default padding-clear">
                
                <!-- THE FORM -->
                <?php echo $this->session->flashdata('info_send'); ?>
                <form method="POST" id="contact_form" name="contact_form" action="<?php echo base_url('contact_us/send_message');?>">
                    <div class="panel-body">

                        <div class="col-effect input-effect">
                            <input class="effect-label" required="" placeholder="" type="text" id="contact_name" name="Name">
                            <label><?php echo $this->lang->line('contact_name'); ?></label>
                            <span class="focus-border"></span>
                        </div>

                        <div class="col-effect input-effect">
                            <input class="effect-label" required="" placeholder="" value="" type="email" name="Email">
                            <label><?php echo $this->lang->line('contact_email'); ?></label>
                            <span class="focus-border"></span>
                        </div>

                        <div class="col-effect input-effect">
                            <input class="effect-label" required="" placeholder="" type="text" id="contact_phone" name="Phone">
                            <label><?php echo $this->lang->line('contact_phone'); ?></label>
                            <span class="focus-border"></span>
                        </div>

                        <!-- <div class="col-effect input-effect">
                            <select class="effect-label select" name="Category">
                                <option value="No category" label="&nbsp;" selected=""></option>
                                <option value="1">Category 1</option>
                                <option value="2">Category 2</option>
                            </select>
                            <label>Category</label>
                            <span class="focus-border"></span>
                        </div> -->

                        <div class="col-effect input-effect">
                            <input class="effect-label" placeholder="" value="" type="text" required="" name="Subject">
                            <label><?php echo $this->lang->line('contact_subject'); ?></label>
                            <span class="focus-border"></span>
                        </div>

                        <div class="col-effect input-effect">
                            <textarea class="effect-label" placeholder="" rows="4" name="Message"></textarea>
                            <label><?php echo $this->lang->line('contact_message'); ?></label>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                            <input class="btn green btn-raised" id="button_send" name="button" type="submit" value="<?php echo $this->lang->line('contact_send'); ?>">
                        </div>
                    </div>
                </form>
                </div>
            </div>
            
            <!-- CONTACT INFO -->
            <div class="col-xs-12 col-sm-6 padding-clear">
                <div class="row">
                    <div class="col-xs-12 padding-clear">
                        <div class="panel panel-default">
                            <div class="panel-body text-center contact-phone">
                                <div class="par">
                                <?php
                                    
                                    echo !empty($phone->general_content_en) ? $phone->general_content_en:"[EMPTY!]";
                                    
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 padding-clear">
                        <div class="panel panel-default">
                            <div class="panel-body text-center contact-address">
                                <div class="par">
                                <?php echo !empty($address->general_content_en) ? $address->general_content_en:"[EMPTY!]"; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
</div>