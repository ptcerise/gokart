<div class="bg-asphalt">
<section class="container">
	<!-- PROFILE -->
	<div class="row">
		<header class="main-header text-center">
			<h2 class="display">
                <?php echo $this->lang->line("welcome"); ?>         
            </h2>
		</header>
		<!-- PUBLIC PROFILE -->
		<div class="col-xs-12 col-sm-4">
			<header class="text-center on-section">
				<h3 class="display">
                    <?php echo $this->lang->line("public_profile"); ?>            
                </h3>
			</header>

			<div class="row">
                <article class="panel panel-default">
                    <div class="panel-body panel-pilot">
                        <figure class="col-xs-12 col-sm-4 pilot-photo">
                            <img alt="kidsfun gokart pilot" class="img-responsive" src="<?php echo !empty($data_profile->profile_photo) ? base_url('assets/upload/pilot/'.$data_profile->profile_photo): base_url('assets/img/pilot-default.jpg');?>">
                        </figure>
                        <div class="col-xs-12 col-sm-8 pilot-content">
                            <header class="panel-body pilot-content-header green-line-bottom">
                                <div class="row">
                                    <div class="col-xs-8 text-left">
                                        <h4>
                                            <?php echo $data_user->user_name; ?>
                                        </h4>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <p class="rank text-green"><?php echo $user_stat->stat_rank; ?></p>
                                    </div>
                                </div>
                            </header>
                            <div class="panel-body table-responsive">
                                <table class="pilot-data">
                                    <tr>
                                        <td class="italic">
                                            <?php echo $this->lang->line("points"); ?>
                                        </td><td class="text-right">
                                            <?php echo $user_stat->stat_point; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="italic">
                                            <?php echo $this->lang->line("best_lap"); ?>
                                        </td><td class="text-right">
                                            <?php
                                                $input = isset($user_best_lap->race_best_lap)?$user_best_lap->race_best_lap : 0;

                                                $uSec = $input % 1000;
                                                $input = floor($input / 1000);

                                                $seconds = $input % 60;
                                                $input = floor($input / 60);

                                                $minutes = $input % 60;
                                                $input = floor($input / 60);

                                                if($seconds < 10){
                                                    $seconds = "0".$seconds;
                                                }

                                                if($minutes < 10){
                                                    $minutes = "0".$minutes;
                                                }

                                                echo $minutes!=0 ? $minutes."m ".$seconds.".".$uSec."s" : $seconds.".".$uSec."s";
                                            ?>
                                        </td>
                                    </tr>
                                    <!--<tr>
                                        <td class="italic">
                                            <?php echo $this->lang->line("fastest_race"); ?>
                                        </td><td class="text-right">
                                            <?php
                                                $input = isset($user_fastest_race->race_time)?$user_fastest_race->race_time : 0;

                                                $uSec = $input % 1000;
                                                $input = floor($input / 1000);

                                                $seconds = $input % 60;
                                                $input = floor($input / 60);

                                                $minutes = $input % 60;
                                                $input = floor($input / 60);

                                                $hours = $input % 60;

                                                if($seconds < 10){
                                                    $seconds = "0".$seconds;
                                                }

                                                if($minutes < 10){
                                                    $minutes = "0".$minutes;
                                                }

                                                echo  $hours.":".$minutes.":".$seconds.".".$uSec;
                                            ?>
                                        </td>
                                    </tr>-->
                                </table>
                            </div>
                        </div>
                    </div>
                </article>
	        </div>
            
            <br>
	        <aside class="row">
	        	<!-- SEARCH FORM -->
                <div>
    	            <div class="panel search-panel panel-default">
    	            	<div class="panel-body green-line-bottom">
	            			<div class="col-effect input-effect">
	                            <input class="effect-label" type="text" name="search_friend">
	                            <label>
                                    <?php echo $this->lang->line("search"); ?>   
                                </label>
	                            <span class="focus-border"></span>
	                        </div>
    	            	</div>
    	            </div>
                </div>

	            <!-- SEARCH RESULTS -->
	            <div id="search_result" class="panel panel-default search-result">
                    <article class="panel panel-default">
                    <?php foreach($friend_list as $key=>$val){ ?>
                        <div data-name="<?php echo strtolower($val->stat_pilot); ?>" class="panel-body panel-pilot">
                            <figure class="col-xs-12 col-sm-4 pilot-photo">
                                <?php
                                    $photo = $this->db->query("SELECT * FROM profile WHERE temp_id='$val->temp_id'")->row()->profile_photo;
                                ?>
                                <img alt="kidsfun gokart pilot" class="img-responsive" src="<?php echo base_url("assets/upload/pilot/".$photo); ?>">
                            </figure>
                            <div class="col-xs-12 col-sm-8 pilot-content">
                                <header class="panel-body pilot-content-header green-line-bottom">
                                    <div class="row">
                                        <div class="col-xs-8 text-left">
                                            <h4>
                                                <?php echo $val->stat_pilot; ?>
                                            </h4>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p class="rank text-green">
                                                <?php echo $val->stat_rank; ?>
                                            </p>
                                        </div>
                                    </div>
                                </header>
                                <div class="panel-body table-responsive">
                                    <table class="pilot-data">
                                        <tr>
                                            <td class="italic">
                                                <?php echo $this->lang->line("victories"); ?>
                                            </td><td class="text-right">
                                                <?php echo $val->stat_point; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="italic">
                                                <?php echo $this->lang->line("best_lap"); ?>
                                            </td><td class="text-right">
                                                <?php
                                                $bestlap = $this->db->query("SELECT * FROM race WHERE race_pilot_id='$val->temp_id'")->row();
                                                $input = isset($bestlap->race_best_lap)?$bestlap->race_best_lap : 0;

                                                $uSec = $input % 1000;
                                                $input = floor($input / 1000);

                                                $seconds = $input % 60;
                                                $input = floor($input / 60);

                                                $minutes = $input % 60;
                                                $input = floor($input / 60);

                                                if($seconds < 10){
                                                    $seconds = "0".$seconds;
                                                }

                                                if($minutes < 10){
                                                    $minutes = "0".$minutes;
                                                }

                                                echo $minutes!=0 ? $minutes."m ".$seconds.".".$uSec."s" : $seconds.".".$uSec."s";
                                            ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </article>
	            </div>
	        </aside>
		</div>

		<!-- PERSONAL INFO -->
		<div class="col-xs-12 col-sm-4 padding-clear">
            <article class="col-xs-12 padding-clear">
    			<header class="text-center on-section">
    				<h3 class="display">
                        <?php echo $this->lang->line("personal_info"); ?>            
                    </h3>
    			</header>
    			
    			<!-- PERSONAL INFO FORM -->
    			<div class="panel panel-default personal-info">
                    <?php echo $this->session->userdata('upd_profile'); ?>
    				<div class="panel-body">
    					<form id="form_info" method="post" action="<?php echo base_url(); ?>profile/update_profile">
                            <input type="hidden" name="profile_id" value="<?php echo $data_profile->profile_id; ?>">
    						<div class="col-effect input-effect">
                                <input
                                	class="effect-label has-content"
                                	type="text"
                                	name="user_phone_number"
                                	value="<?php echo $data_profile->profile_phone; ?>"
                                	required readonly
                                />
                                <label>
                                    <?php echo $this->lang->line("phone"); ?>
                                </label>
                                <span class="focus-border"></span>
                            </div>

                            <div class="col-effect input-effect">
                                <input
                                	class="effect-label has-content"
                                	type="text"
                                	name="user_pin_bb"
                                	value="<?php echo $data_profile->profile_pin_bb; ?>"
                                	required readonly
                                />
                                <label>
                                    <?php echo $this->lang->line("pin_bb"); ?>
                                </label>
                                <span class="focus-border"></span>
                            </div>

                            <div class="col-effect input-effect">
                                <input
                                	class="effect-label has-content"
                                	type="text" name="user_address"
                                	value="<?php echo $data_profile->profile_address; ?>"
                                	required readonly
                                />
                                <label>
                                    <?php echo $this->lang->line("address"); ?>
                                </label>
                                <span class="focus-border"></span>
                            </div>

                            <div class="col-effect input-effect">
                                <input
                                	class="effect-label has-content"
                                	type="text"
                                	name="user_postcode"
                                	value="<?php echo $data_profile->profile_postcode; ?>"
                                	required readonly
                                />
                                <label>
                                    <?php echo $this->lang->line("post_code"); ?>
                                </label>
                                <span class="focus-border"></span>
                            </div>

                            <div class="col-effect input-effect">
                                <input
                                	class="effect-label has-content"
                                	type="text"
                                	name="user_city"
                                	value="<?php echo $data_profile->profile_city; ?>"
                                	required readonly
                                />
                                <label>
                                    <?php echo $this->lang->line("city"); ?>
                                </label>
                                <span class="focus-border"></span>
                            </div>

    					</form>
    				</div>
    				<div class="panel-body card-bottom-btn text-right">
    					<span class="edit_btn">
                        	<a class="btn btn-flat edit">
                             <?php echo $this->lang->line("btn_edit"); ?>   
                            </a>
                        </span>
                        <span class="cancel_and_save">
                            <a id="save_profile" class="btn btn-flat save_info">
                                <?php echo $this->lang->line("btn_save"); ?>
                            </a>
                        	<a class="btn btn-flat cancel_edit">
                                <?php echo $this->lang->line("btn_cancel"); ?>   
                            </a>
                        </span>
                    </div>
    			</div>
    		</article>
        </div>

		<!-- MY PAST RACES -->
		<div class="col-xs-12 col-sm-4">
            <?php $my_id = $this->session->userdata('pilot_id'); ?>
			<header class="text-center on-section">
				<h3 class="display">
                    <?php echo $this->lang->line("my_past_race"); ?>            
                </h3>
			</header>
			<div class="panel panel-default last-race row my-race" data-id="<?php echo $my_id; ?>">
                <div class="panel-body green-line-bottom header-date">
                    <div class="col-xs-2">
                        <p class="text-left">
                            <a href="#" class="prev nav"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                        </p>
                    </div>
                    <div class="col-xs-8 text-center">
                        <?php foreach ($my_last as $key => $value) {?>
                            <p class="race-date" data-date="<?php echo $value->race_date;?>">
                                <?php echo date('d/m/y', strtotime($value->race_date));?>
                            </p>
                        <?php } ?>
                    </div>
                    <div class="col-xs-2">
                        <p class="text-right">
                            <a href="#" class="next nav"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </p>
                    </div>
                </div>

                <article class="panel-body">
                    <h5><?php echo $this->lang->line("results"); ?>:</h5>
                    <div class="table-responsive">
                        <table>
                        <?php foreach ($my_last as $key => $value) { ?>
                            <tbody data-date="<?php echo $value->race_date; ?>">
                            <?php
                                $data = $this->db->query("SELECT * FROM race WHERE race_date='".$value->race_date."' ORDER BY race_time ASC")->result();
                                $fast = $this->db->query("SELECT * FROM race WHERE race_date='".$value->race_date."' ORDER BY race_time ASC LIMIT 1")->row()->race_time;

                                foreach ($data as $i => $val) {
                            ?>
                                <tr data-id="<?php echo $val->race_pilot_id;?>">
                                    <td class="number"><?php echo $i+1; ?></td>
                                    <td><?php
                                        $name = $this->db->query("SELECT user_name FROM user WHERE user_id='".$val->race_pilot_id."'")->row()->user_name;
                                        echo $name;
                                    ?></td>
                                    <td class="text-right">
                                        <?php
                                            if($i+1 == 1){
                                                $input = $val->race_time;

                                                $uSec = $input % 1000;
                                                $input = floor($input / 1000);

                                                $seconds = $input % 60;
                                                $input = floor($input / 60);

                                                $minutes = $input % 60;
                                                $input = floor($input / 60);

                                                if($minutes == 0){
                                                    echo $seconds.".".$uSec."s";
                                                }else{
                                                    echo $minutes.":".$seconds.".".$uSec;
                                                }
                                            }elseif ($val->race_info == "dnf") {
                                                echo strtoupper($val->race_info);
                                            }else{
                                                $input = $val->race_time - $fast;

                                                $uSec = ($input % 1000);
                                                $input = floor($input / 1000);

                                                $seconds = ($input % 60);
                                                $input = floor($input / 60);

                                                $minutes = ($input % 60);
                                                $input = floor($input / 60);

                                                if($minutes == 0){
                                                    echo "+".$seconds.".".$uSec."s";
                                                }else{
                                                    echo "+".$minutes.":".$seconds.".".$uSec;
                                                }
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        <?php } ?>
                        </table>
                    </div>
                </article>

                <div class="panel-body card-bottom-btn green-line-bottom text-right">
                    <a class="btn btn-flat last-showmore">
                        <?php echo $this->lang->line("btn_showmore"); ?>
                    </a>
                </div>
                
                <div class="panel-body">
                    <header>
                        <h5><?php echo $this->lang->line("fastest_lap"); ?>:</h5>
                    </header>
                    <div class="table-responsive">
                        <table><tbody class="bottom">
                            <tr>
                                <td class="clock"><i class="material-icons text-green">alarm</i></td>
                                <td><?php echo $fastest_alltime; ?></td>
                                <td class="text-right">
                                    <?php
                                        $input = $fastest_lap_alltime;

                                        $uSec = $input % 1000;
                                        $input = floor($input / 1000);

                                        $seconds = $input % 60;
                                        $input = floor($input / 60);

                                        $minutes = $input % 60;
                                        $input = floor($input / 60);

                                        echo $minutes."m ".$seconds.".".$uSec."s";
                                    ?> 
                                </td>
                            </tr>
                        </tbody></table>
                    </div>
                </div>
            </div>
		</div>
	</div>

	<div id="community1" class="row">
        <!-- LAST PRO RACE -->   
        <article class="col-xs-12 col-sm-4 padding-clear">
            <header class="text-center on-section">
                <h3 class="display">
                    <?php echo $this->lang->line("last_pro_race"); ?>
                </h3>
            </header>

            <div class="panel panel-default last-race pro">
                <div class="panel-body green-line-bottom header-date">
                    <div class="col-xs-2">
                        <p class="text-left">
                            <a href="#" class="prev" data-type="pro"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                        </p>
                    </div>
                    <div class="col-xs-8 text-center">
                    <?php foreach ($last_pro as $key => $value) {?>
                        <p class="race-date" data-date="<?php echo $value->race_date;?>">
                            <?php echo date('d/m/y', strtotime($value->race_date));?>
                        </p>
                    <?php } ?>
                    </div>
                    <div class="col-xs-2">
                        <p class="text-right">
                            <a href="#" class="next" data-type="pro"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </p>
                    </div>
                </div>

                <div class="panel-body">
                    <header>
                        <h5><?php echo $this->lang->line("results"); ?>:</h5>
                    </header>
                    <div class="table-responsive">
                    <?php
                        foreach($last_pro as $key=>$value){
                    ?>
                        <table data-date="<?php echo $value->race_date; ?>">
                        <?php
                            $data = $this->db->query("SELECT * FROM race WHERE race_date='".$value->race_date."' AND race_type='pro' ORDER BY race_time ASC")->result();
                            $fast = $this->db->query("SELECT * FROM race WHERE race_date='".$value->race_date."' ORDER BY race_time ASC LIMIT 1")->row()->race_time;
                            $input = $fast;
                            $uSec = $input % 1000; $fastest[0] = $uSec;
                            $input = floor($input / 1000);

                            $seconds = $input % 60; $fastest[1] = $seconds;
                            $input = floor($input / 60);

                            $minutes = $input % 60; $fastest[2] = $minutes;
                            $input = floor($input / 60);
                            foreach($data as $i=>$val){
                        ?>
                            <tr>
                                    <td class="number"><?php echo $i+1; ?></td>
                                    <td>&nbsp </td>
                                    <td><?php
                                        $name = $this->db->query("SELECT user_name FROM user WHERE user_id='".$val->race_pilot_id."'")->row()->user_name;
                                        echo $name;
                                    ?></td>
                                    <!-- <td class="text-right"> -->
                                        <?php
                                            // if($i+1 == 1){
                                            //     $input = $val->race_time;

                                            //     $uSec = $input % 1000;
                                            //     $input = floor($input / 1000);

                                            //     $seconds = $input % 60;
                                            //     $input = floor($input / 60);

                                            //     $minutes = $input % 60;
                                            //     $input = floor($input / 60);

                                            //     if($minutes == 0){
                                            //         echo $seconds.".".$uSec."s";
                                            //     }else{
                                            //         echo $minutes.":".$seconds.".".$uSec;
                                            //     }
                                            // }elseif ($val->race_info == "dnf") {
                                            //     echo strtoupper($val->race_info);
                                            // }else{
                                            //     $input = $val->race_time - $fast;

                                            //     $uSec = ($input % 1000);
                                            //     $input = floor($input / 1000);

                                            //     $seconds = ($input % 60);
                                            //     $input = floor($input / 60);

                                            //     $minutes = ($input % 60);
                                            //     $input = floor($input / 60);

                                            //     if($minutes == 0){
                                            //         echo "+".$seconds.".".$uSec."s";
                                            //     }else{
                                            //         echo "+".$minutes.":".$seconds.".".$uSec;
                                            //     }
                                            // }
                                        ?>
                                    <!--</td>-->
                                </tr>
                        <?php } ?>
                        </table>
                    <?php } ?>
                    </div>
                </div>

                <div class="panel-body card-bottom-btn green-line-bottom text-right">
                    <a class="btn btn-flat" data-toggle="modal" data-target="#share-prorace">
                        <?php echo $this->lang->line("btn_share"); ?>
                    </a>
                    <a class="btn btn-flat last-showmore">
                        <?php echo $this->lang->line("btn_showmore"); ?>
                    </a>
                </div>
                
                <div class="panel-body">
                    <header>
                        <h5><?php echo $this->lang->line("fastest_lap"); ?>:</h5>
                    </header>
                    <div class="table-responsive">
                        <table>
                            <tr>
                                <td class="clock"><i class="material-icons text-green">alarm</i></td>
                                <td><?php echo $the_fastest_pro; ?></td>
                                <td class="text-right">
                                    <?php
                                        $input = $fastest_time_pro;

                                        $uSec = $input % 1000;
                                        $input = floor($input / 1000);

                                        $seconds = $input % 60;
                                        $input = floor($input / 60);

                                        $minutes = $input % 60;
                                        $input = floor($input / 60);

                                        echo $minutes."m ".$seconds.".".$uSec."s";
                                    ?> 
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <!-- SHARE -->
            <div id="share-prorace" class="modal fade">
              <div class="modal-dialog modal-share">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <?php
                            $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#thecommunity_anchor";
                            $title = $this->lang->line('last_pro_race');

                            $share_title = $title;
                            if($language == "indonesia"){
                                $share_text = $title." di Gokart Kidsfun Yogyakarta. Klik ";
                                
                            }else{
                                $share_text = $title." on Gokart Kidsfun Yogyakarta. Click ";
                            }
                        ?>
                        <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                        <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                            <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                        </a>
                        <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                            <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                        </a>
                        <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                            <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                        </a>
                    </div>
                    <div class="panel-body modal-close">
                        <a class="btn btn-flat pull-right" data-dismiss="modal">
                            <?php echo $this->lang->line("btn_close"); ?>
                        </a>
                    </div> 
                </div>
              </div>
            </div>
        </article>

        <!-- LAST CASUAL RACE -->
        <article class="col-xs-12 col-sm-4 padding-clear">
            <header class="text-center on-section">
                <h3 class="display">
                    <?php echo $this->lang->line("last_casual_race"); ?>
                </h3>
            </header>

            <div class="panel panel-default last-race casual">
                <div class="panel-body green-line-bottom header-date">
                    <div class="col-xs-2">
                        <p class="text-left">
                            <a href="#" class="prev" data-type="pro"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                        </p>
                    </div>
                    <div class="col-xs-8 text-center">
                    <?php foreach ($last_casual as $key => $value) {?>
                        <p class="race-date" data-date="<?php echo $value->race_date;?>">
                            <?php echo date('d/m/y', strtotime($value->race_date));?>
                        </p>
                    <?php } ?>
                    </div>
                    <div class="col-xs-2">
                        <p class="text-right">
                            <a href="#" class="next" data-type="casual"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </p>
                    </div>
                </div>

                <div class="panel-body">
                    <header>
                        <h5><?php echo $this->lang->line("results"); ?>:</h5>
                    </header>
                    <div class="table-responsive">
                    <?php
                        foreach($last_casual as $key=>$value){
                    ?>
                        <table data-date="<?php echo $value->race_date; ?>">
                        <?php
                            $data = $this->db->query("SELECT * FROM race WHERE race_date='".$value->race_date."' AND race_type='casual' ORDER BY race_time ASC")->result();
                            $fast = $this->db->query("SELECT * FROM race WHERE race_date='".$value->race_date."' ORDER BY race_time ASC LIMIT 1")->row()->race_time;
                            $input = $fast;
                            $uSec = $input % 1000; $fastest[0] = $uSec;
                            $input = floor($input / 1000);

                            $seconds = $input % 60; $fastest[1] = $seconds;
                            $input = floor($input / 60);

                            $minutes = $input % 60; $fastest[2] = $minutes;
                            $input = floor($input / 60);
                            foreach($data as $i=>$val){
                        ?>
                            <tr>
                                <td class="number"><?php echo $i+1; ?></td>
                                <td><?php
                                    $name = $this->db->query("SELECT user_name FROM user WHERE user_id='".$val->race_pilot_id."'")->row()->user_name;
                                    echo $name;
                                ?></td>
                                <td class="text-right">
                                    <?php
                                        if($i+1 == 1){
                                            $input = $val->race_time;

                                            $uSec = $input % 1000;
                                            $input = floor($input / 1000);

                                            $seconds = $input % 60;
                                            $input = floor($input / 60);

                                            $minutes = $input % 60;
                                            $input = floor($input / 60);

                                            if($minutes == 0){
                                                echo $seconds.".".$uSec."s";
                                            }else{
                                                echo $minutes.":".$seconds.".".$uSec;
                                            }
                                        }elseif ($val->race_info == "dnf") {
                                            echo strtoupper($val->race_info);
                                        }else{
                                            $input = $val->race_time - $fast;

                                            $uSec = ($input % 1000);
                                            $input = floor($input / 1000);

                                            $seconds = ($input % 60);
                                            $input = floor($input / 60);

                                            $minutes = ($input % 60);
                                            $input = floor($input / 60);

                                            if($minutes == 0){
                                                echo "+".$seconds.".".$uSec."s";
                                            }else{
                                                echo "+".$minutes.":".$seconds.".".$uSec;
                                            }
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </table>
                    <?php } ?>
                    </div>
                </div>

                <div class="panel-body card-bottom-btn green-line-bottom text-right">
                    <a class="btn btn-flat" data-toggle="modal" data-target="#share-casualrace">
                        <?php echo $this->lang->line("btn_share"); ?>
                    </a>
                    <a class="btn btn-flat last-showmore">
                        <?php echo $this->lang->line("btn_showmore"); ?>
                    </a>
                </div>
                
                <div class="panel-body">
                    <header>
                        <h5><?php echo $this->lang->line("fastest_lap"); ?>:</h5>
                    </header>
                    <div class="table-responsive">
                        <table>
                            <tr>
                                <td class="clock"><i class="material-icons text-green">alarm</i></td>
                                <td><?php echo $the_fastest_casual; ?></td>
                                <td class="text-right">
                                    <?php
                                        $input = $fastest_time_casual;

                                        $uSec = $input % 1000;
                                        $input = floor($input / 1000);

                                        $seconds = $input % 60;
                                        $input = floor($input / 60);

                                        $minutes = $input % 60;
                                        $input = floor($input / 60);

                                        echo $minutes."m ".$seconds.".".$uSec."s";
                                    ?> 
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- SHARE -->
            <div id="share-casualrace" class="modal fade">
              <div class="modal-dialog modal-share">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <?php
                            $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#thecommunity_anchor";
                            $title = $this->lang->line('last_casual_race');

                            $share_title = $title;
                            if($language == "indonesia"){
                                
                                $share_text = $title." di Gokart Kidsfun Yogyakarta. Klik ";
                            }else{
                                $share_text = $title." on Gokart Kidsfun Yogyakarta. Click ";
                            }
                        ?>
                        <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                        <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                            <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                        </a>
                        <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                            <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                        </a>
                        <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                            <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                        </a>
                    </div>
                    <div class="panel-body modal-close">
                        <a class="btn btn-flat pull-right" data-dismiss="modal">
                            <?php echo $this->lang->line("btn_close"); ?>
                        </a>
                    </div> 
                </div>
              </div>
            </div>
        </article>

        <!-- NEXT EVENTS -->
        <article class="col-xs-12 col-sm-4 padding-clear">
            <header class="text-center on-section">
                <h3 class="display">
                    <?php echo $this->lang->line("next_events"); ?>
                </h3>
            </header>
            <div class="panel panel-default table-responsive">
                <div class="next-events">
                    <table class="table-gokart">
                    <?php foreach($events as $key=>$event){ ?>
                        <tr>
                            <td class="col-divider col-date"><?php echo date('d/m/y', strtotime($event->event_date)); ?></td>
                            <td class="event-name"><?php echo $event->event_name; ?></td>
                        </tr>
                    <?php } ?>
                    </table>
                </div>

                <div class="panel-body champion-bottom-btn text-right">
                    <a class="btn btn-flat showmore-event">
                        <?php echo $this->lang->line("btn_showmore"); ?>
                    </a>
                </div>
            </div>
        </article>
    </div>

	<div id="community2" class="row">
        <!-- ALL STAR PILOT -->
        <div class="col-xs-12 col-md-4">
            <header class="text-center on-section">
                <h3 class="display">
                    <?php echo $this->lang->line("all_star"); ?>
                </h3>
            </header>
        
            <div class="row ">
                <?php foreach($top_rank as $key=>$value){ ?>
                    <div class="col-xs-12 padding-clear">
                        <div class="panel panel-default">
                            <div class="panel-body panel-pilot">
                                <figure class="col-xs-12 col-sm-4 pilot-photo">
                                    <?php $photo = $this->db->query("SELECT * FROM profile WHERE temp_id='".$value->temp_id."'")->row()->profile_photo; ?>
                                    <img alt="kidsfun gokart pilot" class="img-responsive" src="<?php echo !empty($photo)?base_url('assets/upload/pilot/'.$photo): base_url('assets/img/pilot-default.jpg');?>">
                                </figure>
                                <article class="col-xs-12 col-sm-8 pilot-content">
                                    <header class="panel-body pilot-content-header green-line-bottom">
                                        <div class="row">
                                            <div class="col-xs-8 text-left">
                                                <h4>
                                                    <?php echo $value->stat_pilot; ?>
                                                </h4>
                                            </div>
                                            <div class="col-xs-4 text-right">
                                                <?php $rank_clr = array('text-yellow', 'text-grey', 'text-orange'); ?>
                                                <p class="rank <?php echo $rank_clr[$key]; ?>"><?php echo $key+1; ?></p>
                                            </div>
                                        </div> 
                                    </header>
                                    <div class="panel-body table-responsive">
                                        <table class="pilot-data">
                                            <tr>
                                                <td class="italic">
                                                    <?php echo $this->lang->line("points"); ?>
                                                </td><td class="text-right"><?php echo $value->stat_point; ?></td>
                                            </tr>

                                            <?php 
                                                //$rtime = $this->db->query("SELECT * FROM race WHERE race_pilot_id='".$value->temp_id."' ORDER BY race_time ASC")->row()->race_time;
                                                $blap = $this->db->query("SELECT * FROM race WHERE race_pilot_id='".$value->temp_id."' ORDER BY race_best_lap ASC")->row();
                                                if(empty($blap)){
                                                    $blap = 0;
                                                }else{
                                                    $blap = $blap->race_best_lap;
                                                }
                                            ?>
                                            <tr>
                                                <td class="italic">
                                                    <?php echo $this->lang->line("best_lap"); ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php
                                                        $input = $blap;

                                                        $uSec = $input % 1000;
                                                        $input = floor($input / 1000);

                                                        $seconds = $input % 60;
                                                        $input = floor($input / 60);

                                                        $minutes = $input % 60;
                                                        $input = floor($input / 60);

                                                        echo $minutes!=0 ? $minutes."m ".$seconds.".".$uSec."s" : $seconds.".".$uSec."s";
                                                    ?>
                                                </td>
                                            </tr>
                                            <!--<tr>
                                                <td class="italic">-->
                                                <?php //echo $this->lang->line("fastest_race"); ?>
                                                <!--</td>
                                                <td class="text-right">-->
                                                   <?php
                                                        // $input = $rtime;

                                                        // $uSec = $input % 1000;
                                                        // $input = floor($input / 1000);

                                                        // $seconds = $input % 60;
                                                        // $input = floor($input / 60);

                                                        // $minutes = $input % 60;
                                                        // $input = floor($input / 60);

                                                        // echo $minutes!=0 ? $minutes."m ".$seconds.".".$uSec."s" : $seconds.".".$uSec."s";
                                                    ?> 
                                                <!--</td>
                                            </tr>-->
                                        </table>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        
        <!-- CHAMPIONSHIP -->
        <div class="col-xs-12 col-md-8 padding-clear">
            <div class="col-xs-12">
                <header class="text-center on-section">
                    <h3 class="display">
                        <?php
                        if($language == "indonesia"){
                            echo $this->lang->line("championship")." ".date('Y',strtotime($champ->champ_date));
                        }
                        else{
                            
                            echo date('Y',strtotime($champ->champ_date))." ".$this->lang->line("championship");
                        }
                        #endif;
                        ?>
                    </h3>
                </header>
                <div class="row">
                    <div class="panel panel-default">
                        <div class="table-responsive champ-tbl">
                            <table class="table-gokart">
                                <thead>
                                    <th class="number col-divider">No</th>
                                    <th class="pilot-name">
                                        <?php echo $this->lang->line("pilot_name"); ?>
                                    </th>
                                    <th class="text-center">
                                        <?php echo $this->lang->line("races"); ?>
                                    </th>
                                    <th class="text-center">
                                        <?php echo $this->lang->line("points"); ?>
                                    </th>
                                    <th class="text-center">
                                        <?php echo $this->lang->line("behind"); ?>
                                    </th>
                                </thead>
                                <tbody>
                                <?php
                                    $pilots = $this->db->query("SELECT * FROM championship WHERE champ_date='".$champ->champ_date."' ORDER BY champ_point DESC")->result();
                                    $highest = $this->db->query("SELECT * FROM championship WHERE champ_date='".$champ->champ_date."' ORDER BY champ_point DESC")->row()->champ_point;

                                    foreach ($pilots as $key => $x) {
                                ?>
                                    <tr>
                                        <td class="col-divider"><?php echo $key+1; ?></td>
                                        <td><?php echo $x->champ_pilot; ?></td>
                                        <td class="text-center"><?php echo $x->champ_race_count; ?></td>
                                        <td class="text-center"><?php echo $x->champ_point; ?></td>
                                        <td class="text-center"><?php echo $x->champ_podium; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>

                        <!-- SHARE & SHOWMORE -->
                        <div class="panel-body champion-bottom-btn text-right">
                            <a class="btn btn-flat" data-toggle="modal" data-target="#share-champ">
                                <?php echo $this->lang->line("btn_share"); ?>
                            </a>
                            <a class="btn btn-flat showmore-champ">
                                <?php echo $this->lang->line("btn_showmore"); ?>
                            </a>
                        </div>
                        <div id="share-champ" class="modal fade">
                          <div class="modal-dialog modal-share">
                            <div class="panel panel-default">
                                <div class="panel-body text-center">
                                    <?php
                                        $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#thecommunity_anchor";
                                        $year = date('Y',strtotime($champ->champ_date));
                                        $title = $this->lang->line('championship');

                                        $share_title = $title.$year;
                                        if($language == "indonesia"){
                                            $share_text = $title." ".$year." di Gokart Kidsfun Yogyakarta. Klik ";
                                        }else{
                                            
                                            $share_text = $year." ".$title." on Gokart Kidsfun Yogyakarta. Click ";
                                        }
                                    ?>
                                    <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                                    <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                        <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                                    </a>
                                    <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                        <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                                    </a>
                                    <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                        <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                                    </a>
                                </div>
                                <div class="panel-body modal-close">
                                    <a class="btn btn-flat pull-right" data-dismiss="modal">
                                        <?php echo $this->lang->line("btn_close"); ?>
                                    </a>
                                </div> 
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>