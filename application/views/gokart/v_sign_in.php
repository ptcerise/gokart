<div class="bg-asphalt">
<div class="container">
	<section class="col-xs-12 col-sm-4 col-sm-offset-4 sign-in">
		<header class="main-header text-center">
			<h3 class="display">
				<?php echo $this->lang->line("sign_in"); ?>
			</h3>
		</header>

		<div class="row">
			<div class="panel panel-default bg-light-grey panel-sign-in">
				<div id="signin_alert" class='alert alert-danger'>E-mail / Password is incorrect!</div>
				<div class="panel-body">

					<form>
                        <div class="col-effect input-effect">
                            <input class="effect-label" type="email" name="email" >
                            <label>Email</label>
                            <span class="focus-border"></span>
                        </div>

                        <br/>
                        <div class="col-effect input-effect">
                            <input class="effect-label" type="password" name="password" >
                            <label>Password</label>
                            <span class="focus-border"></span>
                        </div>

                        <div class="text-center">
                        	<button id="signin_btn" type="submit" class="btn btn-raised btn-sign-in"><?php echo $this->lang->line("sign_in");?></button>
	                    </div>
					</form>

				</div>
			</div>
		</div>

	</section>
</div>
</div>