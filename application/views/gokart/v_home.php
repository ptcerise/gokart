<div class="card-times">
    <div class="open-time">
        <p><?php echo $this->lang->line("park_open"); ?><br>
        <?php echo $this->lang->line("today"); ?> : <?php echo $open; ?> am <?php echo $this->lang->line("to"); ?> <?php echo $close; ?> pm</p>
    </div>
</div>
<section class="video-banner">
    <!-- VIDEO ON BACKGROUND -->
    <video class="vid-background" id="vid-background" muted="" autoplay="" poster="<?php echo isset($video_poster->media_url)?base_url('assets/upload/home/'.$video_poster->media_url):base_url('assets/img/blank_large.png');?>">
        <source id="vid-source" src="<?php echo isset($video_banner->media_url)?base_url('assets/upload/video/'.$video_banner->media_url):'';?>" type="video/mp4">
    </video>

    <header class="video-title text-center">
        <h1 class="display">
            <?php
                if($lang == "indonesia"){
                    echo isset($banner_text->general_title_in)?$banner_text->general_title_in:'[ KOSONG! ]';
                }else{
                    echo isset($banner_text->general_title_en)?$banner_text->general_title_en:'[ EMPTY! ]';
                }
            ?>
        </h1>
        <p class="lead">
            <?php
                if($lang == "indonesia"){
                    echo isset($banner_text->general_content_in)?$banner_text->general_content_in:'';
                }else{
                    echo isset($banner_text->general_content_en)?$banner_text->general_content_en:'';
                }
            ?>
        </p>
        <a href="#popup" class="btn-play" onclick="play_video()">
            <img class="icon-play" src="<?php echo base_url('assets/icon/playbutton.svg');?>" alt="play-icon">
        </a>
    </header>

    <!-- VIDEO POP UP -->
    <div id="popup" class="popup">
        <div class="window container-fluid">
            <div class="col-xsections-12">
                <button id="close-vid-btn" onclick="stop_video();document.location.href='#'" class="btn green btn-raised btn-sm" title="Close">
                    <i class="fa fa-times-circle close-vid" aria-hidden="true"></i> Close
                </button>
            </div>

            <div class="col-xs-12">
                <video id="vid-popup" class="vid-popup" controls>
                    <!-- PLACE FOR VIDEO POPUP, THE VIDEO WILL BE MOVED HERE, LEAVE THIS LINE EMPTY -->
                </video>
            </div> 
        </div>           
    </div>
</section>

<div class="bg-asphalt">
    <!-- DIVIDER AFTER VIDEO -->
    <div class="container-fluid">
        <div class="row">
            <div class="panel panel-default video-div">
            <div class="panel bg-dark-grey divider-pattern"></div>
                <div class="panel-body text-center top-divider">
                    <span class="divider-text">
                        <?php
                            if($lang == "indonesia"){
                                echo !empty($div1->general_content_in)?$div1->general_content_in:'[ KOSONG! ]';
                            }else{
                                echo !empty($div1->general_content_en)?$div1->general_content_en:'[ EMPTY! ]';
                            }
                        ?>
                    </span>
                    <span>
                        <button onclick="window.location.href='https://kidsfun.co.id/tickets';" class=" btn green btn-raised">
                            <?php echo $this->lang->line("btn_choose"); ?>
                        </button>
                    </span>
                </div>
            <div class="panel bg-dark-grey divider-pattern"></div>
            </div>
        </div>
    </div>  

    <!-- THE KARTS -->
    <section id="thekarts_anchor" class="container">
        <header class="row on-section text-center">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h2 class="display">
                    <?php echo $this->lang->line("the_karts"); ?>
                </h2>     
                <div class="par section-desc">
                    <?php
                        if($lang == "indonesia"){
                            echo !empty($kart_content->general_content_in)?$kart_content->general_content_in:'[ KOSONG! ]';
                        }else{
                            echo !empty($kart_content->general_content_en)?$kart_content->general_content_en:'[ EMPTY! ]';
                        }
                    ?>
                </div>
            </div>
        </header>

        <div class="row">
            <?php foreach ($karts as $key => $row) {?>
            <?php 
                $x = explode(";",$row->kart_spec);
                $spec = array('requirement'=>$x[0],'features'=>$x[1],'frame'=>$x[2],'engine_volume'=>$x[3],'engine'=>$x[4],'power'=>$x[5],'max_speed'=>$x[6]);
            ?>
            <article class="col-xs-12 col-sm-6 col-md-3 padding-clear">
                <div class="panel panel-default">
                
                    <figure>
                        <img alt="kidsfun gokart-double" src="<?php echo isset($row->kart_img)?base_url('assets/upload/kart/'.$row->kart_img):base_url('assets/img/blank_500x375.png'); ?>" class="img-responsive img-cards">
                    </figure>
                    <header class="panel-body card-header">
                        <div class="row">
                            <div class="col-xs-9 text-left">
                                <h4 class="display">
                                    <?php echo $row->kart_name; ?>
                                </h4>
                            </div>
                            <div class="col-xs-3 text-right">
                                <p class="engine-volume"><?php echo $spec['engine_volume']; ?></p>
                            </div>
                        </div>
                    </header>
                    <div class="panel-body card-bottom-btn text-right">
                        <a class="btn btn-flat" data-toggle="modal" data-target="#share-kart<?php echo $row->kart_id; ?>">
                            <?php echo $this->lang->line("btn_share"); ?>
                        </a>
                        <a onclick="view_detail('#karts<?php echo $key; ?>');" class="btn btn-flat">
                            <?php echo $this->lang->line("btn_discover"); ?>
                        </a>
                    </div>

                    <div id="share-kart<?php echo $row->kart_id; ?>" class="modal fade">
                      <div class="modal-dialog modal-share">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <?php
                                    $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#thekarts_anchor";

                                    if($lang == "indonesia"){
                                        $kart = $row->kart_name;
                                    }else{
                                       $kart = $row->kart_name;
                                    }

                                    $share_title = $kart;
                                    if($lang == "indonesia"){
                                        $share_text = $kart." di Gokart Kidsfun Yogyakarta. Klik ";
                                    }else{
                                        $share_text = $kart." on Gokart Kidsfun Yogyakarta. Click ";
                                    }
                                ?>
                                <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                                <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                                </a>
                                <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                                </a>
                                <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                                </a>
                            </div>
                            <div class="panel-body modal-close">
                                <a class="btn btn-flat pull-right" data-dismiss="modal">
                                    <?php echo $this->lang->line("btn_close"); ?>
                                </a>
                            </div> 
                        </div>
                      </div>
                    </div>
                    

                    <article id="karts<?php echo $key; ?>" class="panel kart-detail">
                        <div class="panel-body">
                        <header>
                            <h4 class="display"><?php echo $row->kart_name; ?></h4>
                        </header>
                            <p>
                                <span class="desc"><?php echo $this->lang->line("requirement"); ?>:</span> <?php echo $spec['requirement'] ?> <br>
                                <span class="desc"><?php echo $this->lang->line("features"); ?>:</span> <?php echo $spec['features'] ?> <br>
                                <span class="desc"><?php echo $this->lang->line("frame"); ?>:</span> <?php echo $spec['frame'] ?> <br>
                                <span class="desc"><?php echo $this->lang->line("engine_volume"); ?>:</span> <?php echo $spec['engine_volume'] ?> <br>
                                <span class="desc"><?php echo $this->lang->line("engine"); ?>:</span> <?php echo $spec['engine'] ?> <br>
                                <span class="desc"><?php echo $this->lang->line("power"); ?>:</span> <?php echo $spec['power'] ?> <br>
                                <span class="desc"><?php echo $this->lang->line("max_speed"); ?>:</span> <?php echo $spec['max_speed'] ?> <br>
                            </p>
                        </div>
                        <div class="panel-body card-bottom-btn">
                            <a onclick="close_detail('#karts<?php echo $key; ?>')" class="btn btn-flat pull-right"><?php echo $this->lang->line("btn_close"); ?></a>
                        </div>
                    </article>
                </div>
            </article>
            <?php } ?>
        </div> 
    </section>

    <!-- DIVIDER -->
    <div class="container-fluid">
        <div class="row">
            
            <div class="panel panel-default div-panel">
            <div class="panel bg-dark-grey divider-pattern"></div>

                <div class="panel-body text-center divider">
                    <span class="divider-text">
                        <?php
                            if($lang == "indonesia"){
                                echo !empty($div2->general_content_in)?$div2->general_content_in:'[ KOSONG! ]';
                            }else{
                                echo !empty($div2->general_content_en)?$div2->general_content_en:'[ EMPTY! ]';
                            }
                        ?>
                    </span>
                    <span>
                        <button onclick="window.location.href='https://kidsfun.co.id/';" class="btn green btn-raised">
                            <?php echo $this->lang->line("btn_have_fun"); ?>
                        </button>
                    </span>
                </div>

            <div class="panel bg-dark-grey divider-pattern"></div>
            </div>
            
        </div>
    </div>

    <!-- THE TRACK -->
    <section id="thetrack_anchor" class="container">
        <header class="text-center on-section">
            <h2 class="display">
                <?php echo $this->lang->line("the_track"); ?>
            </h2>
        </header>
        
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">
                <?php foreach ($tracks as $key => $row) { ?>
                <figure class="col-xs-6 col-sm-3">
                    <img alt="kidsfun gokart track1" class="img img-responsive img-track" src="<?php echo !empty($row->media_url)?base_url('assets/upload/track/'.$row->media_url):base_url('assets/img/blank_img.png');?>">
                    <figcaption>
                        <div class="par track-distance"><?php echo $row->media_content_en; ?></div>
                    </figcaption>
                </figure>
                <?php } ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">
                <div class="par">
                    <?php
                        if($lang == "indonesia"){
                            echo !empty($track_content->general_content_in) ? $track_content->general_content_in : '[ KOSONG! ]';
                        }else{
                            echo !empty($track_content->general_content_en) ? $track_content->general_content_en : '[ EMPTY! ]';
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>
    
    <!-- DIVIDER -->
    <div class="container-fluid">
        <div class="row">

            <div class="panel panel-default div-panel">
            <div class="panel bg-dark-grey divider-pattern"></div>

                <div class="panel-body text-center divider">
                    <span class="divider-text">
                    <?php # echo $div3->general_content_en;
                        if($lang == "indonesia"){
                            echo !empty($div3->general_content_in) ? $div3->general_content_in : '[ KOSONG! ]';
                        }else{
                            echo !empty($div3->general_content_en) ? $div3->general_content_en : '[ EMPTY! ]';
                        }
                    ?>
                    </span>
                    <span>
                        <button onclick="window.location.href='https://produkrekreasi.com/';" class=" btn green btn-raised">
                            <?php echo $this->lang->line("btn_corp_website"); ?>
                        </button>
                    </span>
                </div>

            <div class="panel bg-dark-grey divider-pattern"></div>
            </div>
            
        </div>
    </div>

    <!-- THE COMMUNITY -->
    <section id="thecommunity_anchor" class="container">
        <header class="text-center on-section">
            <h2 class="display">
                <?php echo $this->lang->line("the_community"); ?>
            </h2>
        </header>

        
        <div id="all_star_champ" class="row">
            <!-- ALL STAR PILOT -->
            <div class="col-xs-12 col-md-4">
                <header class="text-center on-section">
                    <h3 class="display">
                        <?php echo $this->lang->line("all_star"); ?>
                    </h3>
                </header>
            
                <div class="row ">
                    <?php foreach($top_rank as $key=>$value){ ?>
                        <div class="col-xs-12 padding-clear">
                            <div class="panel panel-default">
                                <div class="panel-body panel-pilot">
                                    <figure class="col-xs-12 col-sm-4 pilot-photo">
                                        <?php $photo = $this->db->query("SELECT * FROM profile WHERE temp_id='".$value->temp_id."'")->row()->profile_photo; ?>
                                        <img alt="kidsfun gokart pilot" class="img-responsive" src="<?php echo !empty($photo)?base_url('assets/upload/pilot/'.$photo): base_url('assets/img/pilot-default.jpg');?>">
                                    </figure>
                                    <article class="col-xs-12 col-sm-8 pilot-content">
                                        <header class="panel-body pilot-content-header green-line-bottom">
                                            <div class="row">
                                                <div class="col-xs-8 text-left">
                                                    <h4>
                                                        <?php echo $value->stat_pilot; ?>
                                                    </h4>
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    <?php $rank_clr = array('text-yellow', 'text-grey', 'text-orange'); ?>
                                                    <p class="rank <?php echo $rank_clr[$key]; ?>"><?php echo $key+1; ?></p>
                                                </div>
                                            </div> 
                                        </header>
                                        <div class="panel-body table-responsive">
                                            <table class="pilot-data">
                                                <tr>
                                                    <td class="italic">
                                                        <?php echo $this->lang->line("points"); ?>
                                                    </td><td class="text-right"><?php echo $value->stat_point; ?></td>
                                                </tr>

                                                <?php 
                                                    //$rtime = $this->db->query("SELECT * FROM race WHERE race_pilot_id='".$value->temp_id."' ORDER BY race_time ASC")->row()->race_time;
                                                    $blap = $this->db->query("SELECT * FROM race WHERE race_pilot_id='".$value->temp_id."' ORDER BY race_best_lap ASC")->row();
                                                    if(empty($blap)){
                                                        $blap = 0;
                                                    }else{
                                                        $blap = $blap->race_best_lap;
                                                    }
                                                ?>
                                                <tr>
                                                    <td class="italic">
                                                        <?php echo $this->lang->line("best_lap"); ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php
                                                            $input = $blap;

                                                            $uSec = $input % 1000/100;
                                                            $input = floor($input / 1000);

                                                            $seconds = $input % 60;
                                                            $input = floor($input / 60);

                                                            echo $seconds!=0 ? $seconds."s ".$uSec."ms" : $uSec."ms";
                                                        ?>
                                                    </td>
                                                </tr>
                                                <!--<tr>
                                                    <td class="italic">-->
                                                    <?php //echo $this->lang->line("fastest_race"); ?>
                                                    <!--</td>
                                                    <td class="text-right">-->
                                                       <?php
                                                            // $input = $rtime;

                                                            // $uSec = $input % 1000;
                                                            // $input = floor($input / 1000);

                                                            // $seconds = $input % 60;
                                                            // $input = floor($input / 60);

                                                            // $minutes = $input % 60;
                                                            // $input = floor($input / 60);

                                                            // echo $minutes!=0 ? $minutes."m ".$seconds.".".$uSec."s" : $seconds.".".$uSec."s";
                                                        ?> 
                                                    <!--</td>
                                                </tr>-->
                                            </table>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            
            <!-- CHAMPIONSHIP -->
            <div class="col-xs-12 col-md-8 padding-clear">
                <div class="col-xs-12">
                    <header class="text-center on-section">
                        <h3 class="display">
                            <?php
                            if($lang == "indonesia"){
                                echo $this->lang->line("championship")." ".date('Y',strtotime($champ->champ_date));
                            }
                            else{
                                
                                echo date('Y',strtotime($champ->champ_date))." ".$this->lang->line("championship");
                            }
                            #endif;
                            ?>
                        </h3>
                    </header>
                    <div class="row">
                        <div class="panel panel-default">
                            <div class="table-responsive champ-tbl">
                                <table class="table-gokart">
                                    <thead>
                                        <th class="number col-divider">No</th>
                                        <th class="pilot-name">
                                            <?php echo $this->lang->line("pilot_name"); ?>
                                        </th>
                                        <th class="text-center">
                                            <?php echo $this->lang->line("races"); ?>
                                        </th>
                                        <th class="text-center">
                                            <?php echo $this->lang->line("points"); ?>
                                        </th>
                                        <th class="text-center">
                                            <?php echo $this->lang->line("behind"); ?>
                                        </th>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $pilots = $this->db->query("SELECT * FROM championship WHERE champ_date='".$champ->champ_date."' ORDER BY champ_point DESC, champ_podium DESC")->result();
                                        $highest = $this->db->query("SELECT * FROM championship WHERE champ_date='".$champ->champ_date."' ORDER BY champ_point DESC")->row()->champ_point;

                                        foreach ($pilots as $key => $x) {
                                    ?>
                                        <tr>
                                            <td class="col-divider"><?php echo $key+1; ?></td>
                                            <td><?php echo $x->champ_pilot; ?></td>
                                            <td class="text-center"><?php echo $x->champ_race_count; ?></td>
                                            <td class="text-center"><?php echo $x->champ_point; ?></td>
                                            <td class="text-center"><?php echo $x->champ_podium; ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>

                            <!-- SHARE & SHOWMORE -->
                            <div class="panel-body champion-bottom-btn text-right">
                                <a class="btn btn-flat" data-toggle="modal" data-target="#share-champ">
                                    <?php echo $this->lang->line("btn_share"); ?>
                                </a>
                                <a class="btn btn-flat showmore-champ">
                                    <?php echo $this->lang->line("btn_showmore"); ?>
                                </a>
                            </div>
                            <div id="share-champ" class="modal fade">
                              <div class="modal-dialog modal-share">
                                <div class="panel panel-default">
                                    <div class="panel-body text-center">
                                        <?php
                                            $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#thecommunity_anchor";
                                            $year = date('Y',strtotime($champ->champ_date));
                                            $title = $this->lang->line('championship');

                                            $share_title = $title.$year;
                                            if($lang == "indonesia"){
                                                $share_text = $title." ".$year." di Gokart Kidsfun Yogyakarta. Klik ";
                                            }else{
                                                
                                                $share_text = $year." ".$title." on Gokart Kidsfun Yogyakarta. Click ";
                                            }
                                        ?>
                                        <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                                        <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                            <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                                        </a>
                                        <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                            <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                                        </a>
                                        <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                            <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                                        </a>
                                    </div>
                                    <div class="panel-body modal-close">
                                        <a class="btn btn-flat pull-right" data-dismiss="modal">
                                            <?php echo $this->lang->line("btn_close"); ?>
                                        </a>
                                    </div> 
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  
        <div id="last_race_event" class="row">
            <!-- LAST PRO RACE -->   
            <article class="col-xs-12 col-sm-4 padding-clear">
                <header class="text-center on-section">
                    <h3 class="display">
                        <?php echo $this->lang->line("last_pro_race"); ?>
                    </h3>
                </header>

                <div class="panel panel-default last-race pro">
                    <div class="panel-body green-line-bottom header-date">
                        <div class="col-xs-2">
                            <p class="text-left">
                                <a href="#" class="prev" data-type="pro"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                            </p>
                        </div>
                        <div class="col-xs-8 text-center">
                        <?php foreach ($last_pro as $key => $value) {?>
                            <p class="race-date" data-date="<?php echo $value->race_date;?>">
                                <?php echo date('d/m/y', strtotime($value->race_date));?>
                            </p>
                        <?php } ?>
                        </div>
                        <div class="col-xs-2">
                            <p class="text-right">
                                <a href="#" class="next" data-type="pro"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </p>
                        </div>
                    </div>

                    <div class="panel-body">
                        <header>
                            <h5><?php echo $this->lang->line("results"); ?>:</h5>
                        </header>
                        <div class="table-responsive">
                        <?php
                            foreach($last_pro as $key=>$value){
                        ?>
                            <table data-date="<?php echo $value->race_date; ?>">
                            <?php
                                $data = $this->db->query("SELECT * FROM race WHERE race_date='".$value->race_date."' AND race_type='pro' ORDER BY race_time ASC")->result();
                                $fast = $this->db->query("SELECT * FROM race WHERE race_date='".$value->race_date."' ORDER BY race_time ASC LIMIT 1")->row()->race_time;
                                $input = $fast;
                                $uSec = $input % 1000; $fastest[0] = $uSec;
                                $input = floor($input / 1000);

                                $seconds = $input % 60; $fastest[1] = $seconds;
                                $input = floor($input / 60);

                                foreach($data as $i=>$val){
                            ?>
                                <tr>
                                    <td class="number"><?php echo $i+1; ?></td>
                                    <td>&nbsp </td>
                                    <td><?php
                                        $name = $this->db->query("SELECT user_name FROM user WHERE user_id='".$val->race_pilot_id."'")->row()->user_name;
                                        echo $name;
                                    ?></td>
                                    <!-- <td class="text-right"> -->
                                        <?php
                                            // if($i+1 == 1){
                                            //     $input = $val->race_time;

                                            //     $uSec = $input % 1000;
                                            //     $input = floor($input / 1000);

                                            //     $seconds = $input % 60;
                                            //     $input = floor($input / 60);

                                            //     $minutes = $input % 60;
                                            //     $input = floor($input / 60);

                                            //     if($minutes == 0){
                                            //         echo $seconds.".".$uSec."s";
                                            //     }else{
                                            //         echo $minutes.":".$seconds.".".$uSec;
                                            //     }
                                            // }elseif ($val->race_info == "dnf") {
                                            //     echo strtoupper($val->race_info);
                                            // }else{
                                            //     $input = $val->race_time - $fast;

                                            //     $uSec = ($input % 1000);
                                            //     $input = floor($input / 1000);

                                            //     $seconds = ($input % 60);
                                            //     $input = floor($input / 60);

                                            //     $minutes = ($input % 60);
                                            //     $input = floor($input / 60);

                                            //     if($minutes == 0){
                                            //         echo "+".$seconds.".".$uSec."s";
                                            //     }else{
                                            //         echo "+".$minutes.":".$seconds.".".$uSec;
                                            //     }
                                            // }
                                        ?>
                                    <!--</td>-->
                                </tr>
                            <?php } ?>
                            </table>
                        <?php } ?>
                        </div>
                    </div>

                    <div class="panel-body card-bottom-btn green-line-bottom text-right">
                        <a class="btn btn-flat" data-toggle="modal" data-target="#share-prorace">
                            <?php echo $this->lang->line("btn_share"); ?>
                        </a>
                        <a class="btn btn-flat last-showmore">
                            <?php echo $this->lang->line("btn_showmore"); ?>
                        </a>
                    </div>
                    
                    <div class="panel-body">
                        <header>
                            <h5><?php echo $this->lang->line("fastest_lap"); ?>:</h5>
                        </header>
                        <div class="table-responsive">
                            <table>
                                <tr>
                                    <td class="clock"><i class="material-icons text-green">alarm</i></td>
                                    <td><?php echo $the_fastest_pro; ?></td>
                                    <td class="text-right">
                                        <?php
                                            $input = $fastest_time_pro;

                                            $uSec = $input % 1000/100;
                                            $input = floor($input / 1000);

                                            $seconds = $input % 60;
                                            $input = floor($input / 60);

                                            echo $seconds."s ".$uSec."ms";
                                        ?> 
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- SHARE -->
                <div id="share-prorace" class="modal fade">
                  <div class="modal-dialog modal-share">
                    <div class="panel panel-default">
                        <div class="panel-body text-center">
                            <?php
                                $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#thecommunity_anchor";
                                $title = $this->lang->line('last_pro_race');

                                $share_title = urlencode($title);
                                if($lang == "indonesia"){
                                    $share_text = $title." di Gokart Kidsfun Yogyakarta. Klik ";
                                }else{
                                    
                                    $share_text = $title." on Gokart Kidsfun Yogyakarta. Click ";
                                }
                            ?>
                            <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                            <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                            </a>
                            <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                            </a>
                            <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                            </a>
                        </div>
                        <div class="panel-body modal-close">
                            <a class="btn btn-flat pull-right" data-dismiss="modal">
                                <?php echo $this->lang->line("btn_close"); ?>
                            </a>
                        </div> 
                    </div>
                  </div>
                </div>
            </article>

            <!-- LAST CASUAL RACE -->
            <article class="col-xs-12 col-sm-4 padding-clear">
                <header class="text-center on-section">
                    <h3 class="display">
                        <?php echo $this->lang->line("last_casual_race"); ?>
                    </h3>
                </header>

                <div class="panel panel-default last-race casual">
                    <div class="panel-body green-line-bottom header-date">
                        <div class="col-xs-2">
                            <p class="text-left">
                                <a href="#" class="prev" data-type="pro"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                            </p>
                        </div>
                        <div class="col-xs-8 text-center">
                        <?php foreach ($last_casual as $key => $value) {?>
                            <p class="race-date" data-date="<?php echo $value->race_date;?>">
                                <?php echo date('d/m/y', strtotime($value->race_date));?>
                            </p>
                        <?php } ?>
                        </div>
                        <div class="col-xs-2">
                            <p class="text-right">
                                <a href="#" class="next" data-type="casual"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </p>
                        </div>
                    </div>

                    <div class="panel-body">
                        <header>
                            <h5><?php echo $this->lang->line("results"); ?>:</h5>
                        </header>
                        <div class="table-responsive">
                        <?php
                            foreach($last_casual as $key=>$value){
                        ?>
                            <table data-date="<?php echo $value->race_date; ?>">
                            <?php
                                $data = $this->db->query("SELECT * FROM race WHERE race_date='".$value->race_date."' AND race_type='casual' ORDER BY race_time ASC")->result();
                                $fast = $this->db->query("SELECT * FROM race WHERE race_date='".$value->race_date."' ORDER BY race_time ASC LIMIT 1")->row()->race_time;
                                $input = $fast;
                                $uSec = $input % 1000; $fastest[0] = $uSec;
                                $input = floor($input / 1000);

                                $seconds = $input % 60; $fastest[1] = $seconds;
                                $input = floor($input / 60);

                                foreach($data as $i=>$val){
                            ?>
                                <tr>
                                    <td class="number"><?php echo $i+1; ?></td>
                                    <td>&nbsp</td>
                                    <td><?php
                                        $name = $this->db->query("SELECT user_name FROM user WHERE user_id='".$val->race_pilot_id."'")->row()->user_name;
                                        echo $name;
                                    ?></td>
                                    <!--<td class="text-right">-->
                                        <?php
                                            /*if($i+1 == 1){
                                                $input = $val->race_time;

                                                $uSec = $input % 1000;
                                                $input = floor($input / 1000);

                                                $seconds = $input % 60;
                                                $input = floor($input / 60);

                                                $minutes = $input % 60;
                                                $input = floor($input / 60);

                                                if($minutes == 0){
                                                    echo $seconds.".".$uSec."s";
                                                }else{
                                                    echo $minutes.":".$seconds.".".$uSec;
                                                }
                                            }elseif ($val->race_info == "dnf") {
                                                echo strtoupper($val->race_info);
                                            }else{
                                                $input = $val->race_time - $fast;

                                                $uSec = ($input % 1000);
                                                $input = floor($input / 1000);

                                                $seconds = ($input % 60);
                                                $input = floor($input / 60);

                                                $minutes = ($input % 60);
                                                $input = floor($input / 60);

                                                if($minutes == 0){
                                                    echo "+".$seconds.".".$uSec."s";
                                                }else{
                                                    echo "+".$minutes.":".$seconds.".".$uSec;
                                                }
                                            }*/
                                        ?>
                                    <!--</td>-->
                                </tr>
                            <?php } ?>
                            </table>
                        <?php } ?>
                        </div>
                    </div>

                    <div class="panel-body card-bottom-btn green-line-bottom text-right">
                        <a class="btn btn-flat" data-toggle="modal" data-target="#share-casualrace">
                            <?php echo $this->lang->line("btn_share"); ?>
                        </a>
                        <a class="btn btn-flat last-showmore">
                            <?php echo $this->lang->line("btn_showmore"); ?>
                        </a>
                    </div>
                    
                    <div class="panel-body">
                        <header>
                            <h5><?php echo $this->lang->line("fastest_lap"); ?>:</h5>
                        </header>
                        <div class="table-responsive">
                            <table>
                                <tr>
                                    <td class="clock"><i class="material-icons text-green">alarm</i></td>
                                    <td><?php echo $the_fastest_casual; ?></td>
                                    <td class="text-right">
                                        <?php
                                            $input = $fastest_time_casual;

                                            $uSec = $input % 1000/100;
                                            $input = floor($input / 1000);

                                            $seconds = $input % 60;
                                            $input = floor($input / 60);

                                            echo $seconds."s ".$uSec."ms";
                                        ?> 
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- SHARE -->
                <div id="share-casualrace" class="modal fade">
                  <div class="modal-dialog modal-share">
                    <div class="panel panel-default">
                        <div class="panel-body text-center">
                            <?php
                                $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#thecommunity_anchor";
                                $title = $this->lang->line('last_casual_race');

                                $share_title = urlencode($title);
                                if($lang == "indonesia"){
                                    $share_text = $title." di Gokart Kidsfun Yogyakarta. Klik ";
                                }else{
                                    
                                    $share_text = $title." on Gokart Kidsfun Yogyakarta. Click ";
                                }
                            ?>
                            <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                            <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                            </a>
                            <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                            </a>
                            <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                            </a>
                        </div>
                        <div class="panel-body modal-close">
                            <a class="btn btn-flat pull-right" data-dismiss="modal">
                                <?php echo $this->lang->line("btn_close"); ?>
                            </a>
                        </div> 
                    </div>
                  </div>
                </div>
            </article>

            <!-- NEXT EVENTS -->
            <article class="col-xs-12 col-sm-4 padding-clear">
                <header class="text-center on-section">
                    <h3 class="display">
                        <?php echo $this->lang->line("next_events"); ?>
                    </h3>
                </header>
                <div class="panel panel-default table-responsive">
                    <div class="next-events">
                        <table class="table-gokart">
                        <?php foreach($events as $key=>$event){ ?>
                            <tr>
                                <td class="col-divider col-date"><?php echo date('d/m/y', strtotime($event->event_date)); ?></td>
                                <td class="event-name"><?php echo $event->event_name; ?></td>
                            </tr>
                        <?php } ?>
                        </table>
                    </div>

                    <div class="panel-body champion-bottom-btn text-right">
                        <a class="btn btn-flat showmore-event">
                            <?php echo $this->lang->line("btn_showmore"); ?>
                        </a>
                    </div>
                </div>
            </article>
        </div>   
    </section>

    <!-- DIVIDER -->
    <div class="container-fluid">
        <div class="row">

            <div class="panel panel-default div-panel">
            <div class="panel bg-dark-grey divider-pattern"></div>

                <div class="panel-body divider text-center">
                    <span class="divider-text">
                        <?php
                            if($lang == "indonesia"){
                                echo !empty($div4->general_content_in)?$div4->general_content_in:'[ KOSONG! ]';
                            }else{
                                
                                echo !empty($div4->general_content_en)?$div4->general_content_en:'[ EMPTY! ]';
                            }
                        ?>
                    </span>
                    <span>
                        <button onclick="window.location.href='<?php echo base_url(); ?>contact_us';" class=" btn green btn-raised">
                            <?php echo $this->lang->line("btn_become_member"); ?>
                        </button>
                    </span>
                </div>

            <div class="panel bg-dark-grey divider-pattern"></div>
            </div>
            
        </div>
    </div>

    <!-- GALLERY -->
    <section id="gallery_anchor" class="container">
        <header class="text-center on-section">
            <h2 class="display">
                <?php echo $this->lang->line("gallery"); ?>
            </h2>
        </header>
        
        <div id="gallery">
            <?php foreach($gallery as $key=>$row){ ?>
                <img alt="<?php
                    if($lang == "indonesia"){
                        echo $row->media_content_in;
                    }else{
                        
                        echo $row->media_content_en;
                    }
                ?>"
                src="<?php echo base_url('assets/upload/gallery/'.$row->media_url); ?>"
                data-image="<?php echo base_url('assets/upload/gallery/'.$row->media_url); ?>"
                data-description="<?php 
                    if($lang == "indonesia"){
                        echo $row->media_content_in;
                    }else{
                        
                        echo $row->media_content_en;
                    }
                ?>">
            <?php ; } ?>
        </div>
    </section>
</div>