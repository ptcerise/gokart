 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line('home') ?>
        </h1>
    </section>

    <!-- Main content -->
    <!-- OPENING TIME -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-sm-offset-4">
                <div class="panel panel-info">
                    <?php echo $this->session->userdata('notif_operational'); ?>
                    <div class="panel-heading">
                        <?php echo $this->lang->line('operational'); ?>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="<?php echo base_url(); ?>backend/home/save_opening">
                        <div class="form-group row">
                            <label class="control-label col-xs-3"><?php echo $this->lang->line('open'); ?></label>
                            <div class="col-xs-6">
                            <input type="text" class="form-control" name="open" value="<?php echo $open; ?>">
                            </div>
                            <div class="col-xs-3">AM</div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-xs-3"><?php echo $this->lang->line('close'); ?></label>
                            <div class="col-xs-6">
                            <input type="text" class="form-control" name="close" value="<?php echo $close; ?>">
                            </div>
                            <div class="col-xs-3">PM</div>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <button type="submit" class="btn btn-md btn-primary">
                            <?php echo $this->lang->line('btn_update') ?>
                        </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- BANNER SECTION -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div id="video" class="panel-heading"><?php echo $this->lang->line('home_banner') ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
            <!-- ########## Video ########## -->
                        <div class="col-xs-6">
                            <?php echo $this->session->userdata('notif_video'); ?>
                            <form method="POST" action="<?php echo base_url(); ?>backend/home/save_video" accept-charset="utf-8" enctype="multipart/form-data">
                            <input type="hidden" name="video_id" value="<?php echo isset($video_banner->media_id)?$video_banner->media_id : '' ?>">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line('video') ?>
                                    </div>
                                    <div class="panel-body text-center">
                                        <div class="form-group">
                                            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                                <video id="home_video" muted="" controls="" poster="<?php echo isset($video_banner->media_url)? base_url('assets/upload/home/'.$video_poster->media_url) : base_url('assets/img/blank_large.png'); ?>">
                                                    <source src="<?php echo isset($video_banner->media_url)?base_url('assets/upload/video/'.$video_banner->media_url):''; ?>">
                                                    </source>
                                                </video>
                                                <div class="upload-rules text-left">
                                                    <ul>
                                                        <li>* <?php echo $this->lang->line('max_size') ?> <strong>8Mb</strong></li>
                                                        <li>* <?php echo $this->lang->line('file_format') ?> <strong>.mp4 / .flv / .avi</strong></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Title English" class="col-xs-12 control-label">
                                                <?php echo $this->lang->line('choose_video') ?>
                                            </label>
                                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                              <input type="file" class="form-control" name="video" id="video_up">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer text-center">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-md btn-primary">
                                                <?php echo $this->lang->line('btn_update') ?>
                                            </button> 
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
            <!-- ########## Video Poster ########## -->
                    <div class="col-xs-6">
                        <?php echo $this->session->userdata('notif_video_poster'); ?>
                        <form method="POST" action="<?php echo base_url(); ?>backend/home/save_video_poster" accept-charset="utf-8" enctype="multipart/form-data">
                        <input type="hidden" name="video_poster_id" value="<?php echo isset($video_poster->media_id)?$video_poster->media_id:''; ?>">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line('video_poster') ?>
                                </div>
                                <div class="panel-body text-center">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                            <img class="img-responsive" src="<?php echo isset($video_poster->media_url) ? base_url('assets/upload/home/thumbnail/'.$video_poster->media_url) : base_url('assets/img/blank_large.png'); ?>">
                                        </div>
                                    </div>
                                    <div class="upload-rules col-xs-12 col-sm-10 col-sm-offset-1 text-left">
                                        <ul>
                                            <li>* <?php echo $this->lang->line('max_size') ?> <strong>2Mb</strong></li>
                                            <li>* <?php echo $this->lang->line('resolution') ?> <strong>1920 x 939</strong> pixel</li>
                                            <li>* <?php echo $this->lang->line('orientation') ?> <strong>'Landscape'</strong></li>
                                        </ul>
                                    </div>
                                    <div class="form-group">
                                        <label for="Title English" class="col-xs-12 control-label">
                                            <br>
                                            <?php echo $this->lang->line('choose_image') ?>
                                        </label>
                                        <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                                          <input type="file" class="form-control" name="media_url">
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-md btn-primary" title="">
                                            <?php echo $this->lang->line('btn_update') ?>
                                        </button> 
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
            <!-- ########## Video Content ########## -->
                        <div class="col-xs-12" id="banner_text">
                            <?php echo $this->session->userdata('notif_banner_text'); ?>
                            <form method="POST" action="<?php echo base_url(); ?>backend/home/save_banner_text" accept-charset="utf-8" enctype="multipart/form-data">
                            <input type="hidden" name="banner_id" value="<?php echo isset($banner_text->general_id)?$banner_text->general_id:''; ?>">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line('banner_caption') ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="Title English" class="col-xs-12 control-label">
                                                            <?php echo $this->lang->line('caption_en') ?>
                                                        </label>
                                                        <div class="col-xs-12">
                                                          <input type="text" class="form-control" id="banner_caption_en" name="banner_caption_en" value="<?php echo isset($banner_text->general_title_en) ? $banner_text->general_title_en:''; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group"> 
                                                        <label for="Title English" class="col-xs-12 control-label"><br>
                                                            <?php echo $this->lang->line('content_en') ?>
                                                        </label>
                                                        <div class="col-xs-12">
                                                            <textarea class="form-control wysihtml5" rows="7" id="banner_content_en" name="banner_content_en"><?php echo isset($banner_text->general_content_in) ? $banner_text->general_content_in:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="Title English" class="col-xs-12 control-label">
                                                            <?php echo $this->lang->line('caption_in') ?>
                                                        </label>
                                                        <div class="col-xs-12">
                                                          <input type="text" class="form-control" id="banner_caption_in" name="banner_caption_in" value="<?php echo isset($banner_text->general_title_in) ? $banner_text->general_title_in:''; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group"> 
                                                        <label for="Title English" class="col-xs-12 control-label"><br>
                                                            <?php echo $this->lang->line('content_in') ?>
                                                        </label>
                                                        <div class="col-xs-12">
                                                            <textarea class="form-control wysihtml5" rows="7" id="banner_content_in" name="banner_content_in"><?php echo isset($banner_text->general_content_in) ? $banner_text->general_content_in:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer text-center">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary">
                                                <?php echo $this->lang->line('btn_update') ?>
                                            </button> 
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- DIVIDER -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div id="divider" class="panel-heading"><?php echo $this->lang->line('divider') ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <!-- DIVIDER 1 -->
                            <div class="col-xs-12" id="div1">
                                <?php echo $this->session->userdata('notif_div1'); ?>
                                <form method="POST" action="<?php echo base_url(); ?>backend/home/save_divider" accept-charset="utf-8" enctype="multipart/form-data">
                                <input type="hidden" name="div_section" value="1">
                                <input type="hidden" name="div_id" value="<?php echo isset($div1->general_id)?$div1->general_id:''; ?>">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <?php echo $this->lang->line('div1') ?>
                                        </div>
                                        <div class="panel-body">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="Title English" class="col-xs-12 control-label">
                                                                <?php echo $this->lang->line('caption_en') ?>
                                                            </label>
                                                            <div class="col-xs-12">
                                                              <input type="text" class="form-control" id="div_en" name="div_en" value="<?php echo isset($div1->general_content_en) ? $div1->general_content_en:''; ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="Title English" class="col-xs-12 control-label">
                                                                <?php echo $this->lang->line('caption_in') ?>
                                                            </label>
                                                            <div class="col-xs-12">
                                                              <input type="text" class="form-control" id="div_in" name="div_in" value="<?php echo isset($div1->general_content_in) ? $div1->general_content_in:''; ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer text-center">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-md btn-primary">
                                                    <?php echo $this->lang->line('btn_update') ?>
                                                </button> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- DIVIDER 2 -->
                            <div class="col-xs-12" id="div2">
                                <?php echo $this->session->userdata('notif_div2'); ?>
                                <form method="POST" action="<?php echo base_url(); ?>backend/home/save_divider" accept-charset="utf-8" enctype="multipart/form-data">
                                <input type="hidden" name="div_section" value="2">
                                <input type="hidden" name="div_id" value="<?php echo isset($div2->general_id)?$div2->general_id:''; ?>">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <?php echo $this->lang->line('div2') ?>
                                        </div>
                                        <div class="panel-body">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="Title English" class="col-xs-12 control-label">
                                                                <?php echo $this->lang->line('caption_en') ?>
                                                            </label>
                                                            <div class="col-xs-12">
                                                              <input type="text" class="form-control" id="div_en" name="div_en" value="<?php echo isset($div2->general_content_en) ? $div2->general_content_en:''; ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="Title English" class="col-xs-12 control-label">
                                                                <?php echo $this->lang->line('caption_in') ?>
                                                            </label>
                                                            <div class="col-xs-12">
                                                              <input type="text" class="form-control" id="div_in" name="div_in" value="<?php echo isset($div2->general_content_in) ? $div2->general_content_in:''; ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer text-center">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-md btn-primary">
                                                    <?php echo $this->lang->line('btn_update') ?>
                                                </button> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- DIVIDER 3 -->
                            <div class="col-xs-12" id="div3">
                                <?php echo $this->session->userdata('notif_div3'); ?>
                                <form method="POST" action="<?php echo base_url(); ?>backend/home/save_divider" accept-charset="utf-8" enctype="multipart/form-data">
                                <input type="hidden" name="div_section" value="3">
                                <input type="hidden" name="div_id" value="<?php echo isset($div3->general_id)?$div3->general_id:''; ?>">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <?php echo $this->lang->line('div3') ?>
                                        </div>
                                        <div class="panel-body">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="Title English" class="col-xs-12 control-label">
                                                                <?php echo $this->lang->line('caption_en') ?>
                                                            </label>
                                                            <div class="col-xs-12">
                                                              <input type="text" class="form-control" id="div_en" name="div_en" value="<?php echo isset($div3->general_content_en) ? $div3->general_content_en:''; ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="Title English" class="col-xs-12 control-label">
                                                                <?php echo $this->lang->line('caption_in') ?>
                                                            </label>
                                                            <div class="col-xs-12">
                                                              <input type="text" class="form-control" id="div_in" name="div_in" value="<?php echo isset($div3->general_content_in) ? $div3->general_content_in:''; ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer text-center">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-md btn-primary">
                                                    <?php echo $this->lang->line('btn_update') ?>
                                                </button> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- DIVIDER 4 -->
                            <div class="col-xs-12" id="div4">
                                <?php echo $this->session->userdata('notif_div4'); ?>
                                <form method="POST" action="<?php echo base_url(); ?>backend/home/save_divider" accept-charset="utf-8" enctype="multipart/form-data">
                                <input type="hidden" name="div_section" value="4">
                                <input type="hidden" name="div_id" value="<?php echo isset($div4->general_id)?$div4->general_id:''; ?>">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <?php echo $this->lang->line('div4') ?>
                                        </div>
                                        <div class="panel-body">
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="Title English" class="col-xs-12 control-label">
                                                                <?php echo $this->lang->line('caption_en') ?>
                                                            </label>
                                                            <div class="col-xs-12">
                                                              <input type="text" class="form-control" id="div_en" name="div_en" value="<?php echo isset($div4->general_content_en) ? $div4->general_content_en:''; ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="Title English" class="col-xs-12 control-label">
                                                                <?php echo $this->lang->line('caption_in') ?>
                                                            </label>
                                                            <div class="col-xs-12">
                                                              <input type="text" class="form-control" id="div_in" name="div_in" value="<?php echo isset($div4->general_content_in) ? $div4->general_content_in:''; ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer text-center">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-md btn-primary">
                                                    <?php echo $this->lang->line('btn_update') ?>
                                                </button> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- THE KARTS SECTION -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div id="the_karts" class="panel-heading"><?php echo $this->lang->line('the_karts') ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                        <!-- The Karts Content -->
                        <div class="col-xs-12" id="kart_content">
                            <?php echo $this->session->userdata('notif_kart_content'); ?>
                            <form method="POST" action="<?php echo base_url(); ?>backend/home/save_kart_content" accept-charset="utf-8" enctype="multipart/form-data">
                            <input type="hidden" name="section_id" value="<?php echo isset($kart_content->general_id)?$kart_content->general_id:''; ?>">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line('content') ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group"> 
                                                        <label for="Title English" class="col-xs-12 control-label">
                                                            <?php echo $this->lang->line('content_en') ?>
                                                        </label>
                                                        <div class="col-xs-12">
                                                            <textarea class="form-control wysihtml5" rows="7" name="content_en"><?php echo isset($kart_content->general_content_en) ? $kart_content->general_content_en:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group"> 
                                                        <label for="Title English" class="col-xs-12 control-label">
                                                            <?php echo $this->lang->line('content_in') ?>
                                                        </label>
                                                        <div class="col-xs-12">
                                                            <textarea class="form-control wysihtml5" rows="7" name="content_in"><?php echo isset($kart_content->general_content_in) ? $kart_content->general_content_in:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer text-center">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary">
                                                <?php echo $this->lang->line('btn_update') ?>
                                            </button> 
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <!-- The Karts -->
                        <div class="col-xs-12" id="karts">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line('karts') ?>
                                </div>
                                <div class="panel-body">
                                <div class="row">
                                <?php $i = 0;/*
                                    $text = "text1;text2;text3;text4";
                                    $show = explode(";",$text);

                                    for($i=0;$i<4;$i++){
                                        echo $show[$i]."<br>";
                                    } */
                                ?>
                                <?php foreach($karts as $key=>$row){ ?>
                                    <?php if($i==4){break;} ?>
                                    <?php
                                        $spec = explode(";",$row->kart_spec);
                                    ?>

                                    <div class="col-xs-12 col-sm-3">
                                    <?php echo $this->session->userdata('notif_karts'.$row->kart_id); ?>
                                        <div class="panel panel-default per-kart">
                                            <article class="panel-body">
                                            <figure class="col-xs-12">
                                                <img class="img-responsive img-thumb" src="<?php echo !empty($row->kart_img)?base_url('assets/upload/kart/'.$row->kart_img):base_url('assets/img/blank_500x375.png');?>">
                                            </figure>
                                            <div class="col-xs-12 text-center kart-name">
                                                <h4><?php echo isset($row->kart_name)?$row->kart_name:'-----'; ?></h4>
                                            </div>
                                            <div class="col-xs-12 text-center">
                                                <p><?php echo isset($spec[3])?$spec[3]:'-----'; ?></p>
                                            </div>
                                            </article>
                                            <div class='panel-footer text-center'>
                                                <a href="#"
                                                data-id="<?php echo $row->kart_id; ?>"
                                                data-img="<?php echo base_url('assets/upload/kart/'.$row->kart_img) ?>"
                                                data-spec0="<?php echo $spec[0]; ?>"
                                                data-spec1="<?php echo $spec[1]; ?>"
                                                data-spec2="<?php echo $spec[2]; ?>"
                                                data-spec3="<?php echo $spec[3]; ?>"
                                                data-spec4="<?php echo $spec[4]; ?>"
                                                data-spec5="<?php echo $spec[5]; ?>"
                                                data-spec6="<?php echo $spec[6]; ?>"
                                                class="btn btn-warning edit-kart" title="edit" data-toggle="modal" data-target="#update_kart"><i class="fa fa-edit"></i> <?php echo $this->lang->line('btn_detail') ?></a>
                                            </div>
                                        </div>
                                    </div>

                                <?php $i++;} ?>

                                <div id="update_kart" class="modal fade" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title"><?php echo $this->lang->line('update_kart') ?></h4>
                                            </div>
                                            <div class="modal-body row">
                                                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                                    <form class="form" method="post" action="<?php echo base_url(); ?>backend/home/save_kart" enctype="multipart/form-data">
                                                    <input type="hidden" name="kart_id" value="">
                                                        <div class="form-group row">
                                                            <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('kart_image') ?></label>
                                                            <div class="col-xs-9">
                                                                <input id="imgInpKart" type="file" class="form-control" name="kart_img">
                                                                <div class="photo-preview">
                                                                    <img id="imgPrevKart" class="img img-responsive img-bordered" src="" alt="your image" />
                                                                </div>
                                                                <div class="upload-rules">
                                                                    <ul>
                                                                        <li>* <?php echo $this->lang->line('max_size') ?> <strong>1Mb</strong></li>
                                                                        <li>* <?php echo $this->lang->line('resolution') ?><strong> 500 x 375</strong> pixel</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('kart_name') ?></label>
                                                            <div class="col-xs-9">
                                                                <input type="text" class="form-control" name="kart_name" required="" value="">
                                                            </div>
                                                        </div>
                                                        <!-- SPECIFICATION -->
                                                        <div class="form-group row">
                                                            <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('requirement') ?></label>
                                                            <div class="col-xs-9">
                                                                <input type="text" class="form-control" name="spec_requirement" required="" value="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('features') ?></label>
                                                            <div class="col-xs-9">
                                                                <input type="text" class="form-control" name="spec_features" required="" value="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('frame') ?></label>
                                                            <div class="col-xs-9">
                                                                <input type="text" class="form-control" name="spec_frame" required="" value="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('engine_volume') ?></label>
                                                            <div class="col-xs-9">
                                                                <input type="text" class="form-control" name="spec_engine_volume" required="" value="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('engine') ?></label>
                                                            <div class="col-xs-9">
                                                                <input type="text" class="form-control" name="spec_engine" required="" value="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('power') ?></label>
                                                            <div class="col-xs-9">
                                                                <input type="text" class="form-control" name="spec_power" required="" value="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('max_speed') ?></label>
                                                            <div class="col-xs-9">
                                                                <input type="text" class="form-control" name="spec_max_speed" required="" value="">
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 text-center">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('btn_cancel') ?></button>
                                                            <button id="btn_add_pilot" type="submit" name="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_save') ?></button>
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                               </div>
                               </div>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- THE TRACKS SECTION -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div id="the_tracks" class="panel-heading"><?php echo $this->lang->line('the_tracks') ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                        <!-- The Karts -->
                        <div class="col-xs-12" id="tracks">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line('tracks') ?>
                                </div>
                                <div class="panel-body">
                                
                                <?php foreach($tracks as $key=>$row){ ?>
                                    <div class="col-xs-12 col-sm-3">
                                    <?php echo $this->session->userdata('notif_tracks'.$row->media_id); ?>
                                        <div class="panel panel-default tracks">
                                            <div class="panel-body text-center">
                                            <figure>
                                                <img class="img-responsive img-track" alt="gokart-track" src="<?php echo !empty($row->media_url)?base_url('assets/upload/track/'.$row->media_url):base_url('assets/img/blank_img.png'); ?>">
                                            </figure>
                                            <div class="upload-rules text-left">
                                                <ul>
                                                    <li>* <?php echo $this->lang->line('max_size') ?> <strong>1Mb</strong></li>
                                                    <li>* <?php echo $this->lang->line('resolution') ?><strong> 250 x 250</strong> pixel</li>
                                                </ul>
                                            </div>
                                        <form method="POST" action="<?php echo base_url(); ?>backend/home/save_track" accept-charset="utf-8" enctype="multipart/form-data">
                                            <input type="hidden" name="track_id" value="<?php echo $row->media_id; ?>">
                                            <div class="form-group row">
                                                <label class="col-xs-12 control-label text-center"><br><?php echo $this->lang->line('choose_image') ?></label>
                                                <div class="col-xs-12">
                                                    <input type="file" class="form-control" name="track_img">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xs-12 control-label"><?php echo $this->lang->line('distance') ?></label>
                                                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                                    <input type="text" class="form-control" name="track_distance" value="<?php echo $row->media_content_en; ?>">
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_update') ?></button>
                                        </form>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                               </div>
                            </div>
                        </div>

                        <!-- The Tracks Content -->
                        <div class="col-xs-12" id="track_content">
                            <form method="POST" action="<?php echo base_url(); ?>backend/home/save_track_content" accept-charset="utf-8" enctype="multipart/form-data">
                            <input type="hidden" name="section_id" value="<?php echo isset($track_content->general_id)?$track_content->general_id:''; ?>">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line('content') ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                        <?php echo $this->session->userdata('notif_track_content'); ?>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group"> 
                                                        <label for="Title English" class="col-xs-12 control-label">
                                                            <?php echo $this->lang->line('content_en') ?>
                                                        </label>
                                                        <div class="col-xs-12">
                                                            <textarea class="form-control wysihtml5" rows="7" name="content_en"><?php echo isset($track_content->general_content_en) ? $track_content->general_content_en:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group"> 
                                                        <label for="Title English" class="col-xs-12 control-label">
                                                            <?php echo $this->lang->line('content_in') ?>
                                                        </label>
                                                        <div class="col-xs-12">
                                                            <textarea class="form-control wysihtml5" rows="7" name="content_in"><?php echo isset($track_content->general_content_in) ? $track_content->general_content_in:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer text-center">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary">
                                                <?php echo $this->lang->line('btn_update') ?>
                                            </button> 
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- /.content -->
</div>
