 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line('events') ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line('events') ?>
                        <div class="caption pull-right">
                            <a href="#" class="page btn btn-success btn-xs btn-add" title="Add Event" data-toggle="modal" data-target="#modal_event"><i class="fa fa-plus"></i> <?php echo $this->lang->line('events') ?></a>
                        </div>
                    </div>
                    <?php echo $this->session->userdata('notif_event'); ?>
                    <div class="panel-body table-responsive">
                        <table id="table-pilot" class="table table-bordered table-hover">
                            <thead>
                                <th>#</th>
                                <th><?php echo $this->lang->line('event_name') ?></th>
                                <th><?php echo $this->lang->line('date') ?></th>
                                <th><?php echo $this->lang->line('action') ?></th>
                            </thead>
                            <tbody>
                            <?php
                                foreach($event as $key=>$row){
                            ?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td class="event-name"><?php echo $row->event_name; ?></td>
                                    <td class="event-date"><?php echo date('d/m/y', strtotime($row->event_date)); ?></td>
                                    <td class="text-center">
                                        <a href="#" class="btn btn-warning btn-xs edit-event" title="edit" data-id="<?php echo $row->event_id; ?>" data-toggle="modal" data-target="#modal_event"><i class="fa fa-edit"></i></a>

                                        <a class="btn btn-danger btn-xs edit-event" href="<?php echo base_url('backend/event/delete_event/'.$row->event_id);?>" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"> </i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>

        <div id="modal_event" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo $this->lang->line('events') ?></h4>
                    </div>
                    <div class="modal-body row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <form class="form" id="form_event" method="post" action="<?php echo base_url(); ?>backend/event/save_event" enctype="multipart/form-data">
                            <input type="hidden" name="event_id" value="">
                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('event_name') ?></label>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control" name="event_name" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('date') ?></label>
                                    <div class="col-xs-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control datepicker" name="event_date" value=""/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('btn_cancel') ?></button>
                                    <button id="btn_save_event" type="submit" name="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_save') ?></button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </section><!-- /.content -->
</div>
