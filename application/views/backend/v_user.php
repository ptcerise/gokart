 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line('user_management') ?>
        </h1>
    </section>

    <!-- Main content -->
     <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line('user') ?>
                        <div class="caption pull-right">
                            <a href="#" class="page btn btn-success btn-xs btn-add" title="Add Pilot" data-toggle="modal" data-target="#modal_user"><i class="fa fa-plus"></i> <?php echo $this->lang->line('user') ?></a>
                        </div>
                    </div>
                    <?php echo $this->session->userdata('notif'); ?>
                    <div class="panel-body table-responsive">
                        <table id="table-pilot" class="table table-bordered table-hover">
                            <thead>
                                <th>#</th>
                                <th><?php echo $this->lang->line('user_name') ?></th>
                                <th><?php echo $this->lang->line('user_fname') ?></th>
                                <th><?php echo $this->lang->line('email') ?></th>
                                <th><?php echo $this->lang->line('level') ?></th>
                                <th><?php echo $this->lang->line('action') ?></th>
                            </thead>
                            <tbody>
                            <?php
                                foreach($user as $key=>$row){
                                $profile = $db_access->readtable('profile','',array('temp_id'=>$row->user_id))->row();
                            ?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td class="user_name"><?php echo $row->user_name; ?></td>
                                    <td class="user_fname"><?php echo $row->user_full_name; ?></td>
                                    <td class="user_email"><?php echo $row->user_email; ?></td>
                                    <td class="user_level"><?php
                                        if($row->user_level == 2){
                                            echo "Operator";
                                        }else{
                                            echo "Admin";
                                        }
                                    ?></td>
                                    <td class="text-center">
                                        <a href="#" data-userid="<?php echo $row->user_id; ?>" class="btn btn-warning btn-xs edit-user" title="edit" data-toggle="modal" data-target="#modal_user"><i class="fa fa-edit"></i></a>

                                        <a href="<?php echo base_url('backend/user/delete_user/'.$row->user_id);?>" class="btn btn-danger btn-xs" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"> </i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL ADD PILOT -->
        <div id="modal_user" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo $this->lang->line('user') ?></h4>
                    </div>
                    <div class="modal-body row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <form class="form" id="form_add_user" method="post" action="<?php echo base_url(); ?>backend/user/save_user" enctype="multipart/form-data">
                            <input type="hidden" name="user_id" value="">

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('user_name') ?></label>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control" name="user_name" required="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('user_fname') ?></label>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control" name="user_full_name" required="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('email') ?></label>
                                    <div class="col-xs-9">
                                        <input type="email" class="form-control" name="user_email" required="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('pass') ?></label>
                                    <div class="col-xs-9">
                                        <input type="password" class="form-control" name="user_pass" required="">
                                        <input type="checkbox" id="show_pass"> <?php echo $this->lang->line('show_pass') ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('pass_confirm') ?></label>
                                    <div class="col-xs-9">
                                        <input type="password" class="form-control" name="user_pass_confirm" required="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('level') ?></label>
                                    <div class="col-xs-9">
                                        <select class="form-control" name="user_level">
                                            <option value="1">Admin</option>
                                            <option value="2">Operator</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <button type="button" class="btn btn-default btn-close-modal" data-dismiss="modal"><?php echo $this->lang->line('btn_cancel') ?></button>
                                    <button id="btn_add_pilot" type="submit" name="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_save') ?></button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </section><!-- /.content -->
</div>
