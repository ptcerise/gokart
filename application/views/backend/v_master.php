<?php
  if($this->session->userdata('status_login') != TRUE){
    redirect('backend/Login');
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Backend | <?php echo ucwords($current); ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="<?php echo base_url('assets/img/favicon.ico');?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

      <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/skins/_all-skins.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

  <link rel="stylesheet" href="<?php echo site_url()?>assets/css/kidsfun/custom_bo.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">G</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Gokart</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <?php if($this->session->userdata('be_lang') == 'indonesia' || $this->session->userdata('be_lang') == ''): ?>
                <a href="<?php echo base_url('backend/languageSwitcher/switchLang/indonesia');?>" style="height: 50px;">
                  <img src="<?php echo site_url('assets/icon/english.png'); ?>">
                <?php else:?>
                
                <a href="<?php echo base_url('backend/languageSwitcher/switchLang/english');?>" style="height: 50px;">
                  <img src="<?php echo site_url('assets/icon/indonesia.png'); ?>">
                <?php endif; ?>
                </a>
          </li>

          <li class="dropdown messages-menu">
            <a href="<?php echo base_url();?>backend/contact_us#message">
              <i class="fa fa-envelope-o"></i>
              <?php
                $unread = $this->db->query("SELECT * FROM contact WHERE contact_status='0'")->num_rows();

                if($unread > 0){
              ?>
              <span class="label label-danger"><?php echo "* ".$unread ?></span>
              <?php } ?>
            </a>
          </li>
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php echo $this->session->userdata('user_full_name');?>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/img/logo-gokart.png');?>" class="img-circle" alt="User Image">
                <p><?php echo $this->session->userdata('user_full_name');?></p>
              </li>
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="text-center">
                  <a href="<?php echo base_url('backend/Login/logout');?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header"><?php echo $this->lang->line('main_menu') ?></li>
        <li <?php
              if ($current == "home") {
                  echo "class='active treeview'";
              }
            ?>
        >
          <a href="<?php echo base_url();?>backend/home">
            <i class="fa fa-home"></i> <span><?php echo $this->lang->line('home') ?></span>
          </a>
        </li>

        <li <?php
              if ($current == "event" || $current == "race" || $current == "championship") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="#">
            <i class="fa fa-dashboard"></i>
            <span><?php echo $this->lang->line('community') ?></span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php
              if ($current == "event") {
                  echo "class='active treeview'";
              }
              ?>>
              <a href="<?php echo base_url();?>backend/event"><i class="fa fa-circle-o"></i> <?php echo $this->lang->line('events') ?></a>
            </li>
            <li <?php
              if ($current == "race") {
                  echo "class='active treeview'";
              }
              ?>>
              <a href="<?php echo base_url();?>backend/race"><i class="fa fa-circle-o"></i> <?php echo $this->lang->line('races') ?></a>
            </li>

            <li <?php
              if ($current == "championship") {
                  echo "class='active treeview'";
              }
              ?>>
              <a href="<?php echo base_url();?>backend/championship"><i class="fa fa-circle-o"></i> <?php echo $this->lang->line('championship') ?></a>
            </li>
          </ul>
        </li>

        <li <?php
              if ($current == "member") {
                  echo "class='active treeview'";
              }
            ?>
        >
          <a href="<?php echo base_url();?>backend/pilot">
            <i class="fa fa-group"></i> <span><?php echo $this->lang->line('member') ?></span>
          </a>
        </li>
        <li <?php
              if ($current == "gallery") {
                  echo "class='active treeview'";
              }
            ?>
        >
          <a href="<?php echo base_url();?>backend/gallery">
            <i class="fa fa-image"></i> <span><?php echo $this->lang->line('gallery') ?></span>
          </a>
        </li>
        <li <?php
              if ($current == "contact_us") {
                  echo "class='active treeview'";
              }
            ?>
        >
          <a href="<?php echo base_url();?>backend/contact_us">
            <i class="fa fa-phone-square"></i> <span><?php echo $this->lang->line('contact') ?></span>
          </a>
        </li>

        <?php if($this->session->userdata('user_level') == 1){ ?>
        <li
          <?php
              if ($current == "user") {
                  echo "class='active treeview'";
              }
            ?>
        >
          <a href="<?php echo base_url();?>backend/user">
            <i class="fa fa-user"></i> <span><?php echo $this->lang->line('user_management') ?></span>
          </a>
        </li>
        <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  
  <?php echo $content;?>
  
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs"></div>
    <strong>Copyright &copy; <?php echo date('Y'); ?> Kids Fun | developed by <a href="http://ptcerise.com" target="blank">Cerise</a>.</strong> All rights reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php echo @$script ?>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.0.0/js/bootstrap-datetimepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.0.0/js/bootstrap-datetimepicker.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
 <!-- Bootstrap 3.3.6 -->
 <script src="<?php echo base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<!-- TO USE BASE_URL IN JS FILE -->
<script type="text/javascript">
    var js_base_url = function( urlText ){
    var urlTmp = "<?php echo base_url('" + urlText + "'); ?>";
    return urlTmp;
    }
</script>

<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/js/app.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/backend/scripts.js'); ?>"></script>

<script type="text/javascript">

</script>

</body>
</html>
