 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line('championship') ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line('input_champ') ?></div>
                    <div class="panel-body">
                        <div class="form-group col-xs-12 col-md-4 col-md-offset-1 text-center">
                            <label class="control-label col-xs-12 col-sm-6"><?php echo $this->lang->line('racers') ?></label>
                            <div class="input-group col-xs-12 col-sm-6">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input type="number" min="1" max="<?php echo count($pilot); ?>" class="form-control" id="racer_count" value="1"/>
                            </div>
                        </div>
                        <button id="btn_inp_racer" class="btn btn-primary"><?php echo $this->lang->line('btn_add') ?></button>
                        <hr>

                        <div class="col-xs-12 col-sm-6 col-sm-offset-3 well">
                            <?php echo $this->session->userdata('notif_champ'); ?>
                            <form method="post" action="<?php echo base_url(); ?>backend/championship/save_records">
                                <div class="form-group row">
                                    <label class="col-xs-1 control-label text-right"><?php echo $this->lang->line('date') ?></label>
                                    <div class="col-xs-5">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control datepicker" name="champ_date" value="<?php echo date('m/d/Y'); ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-bordered table-hover well">
                                    <thead>
                                        <th><?php echo $this->lang->line('pilot_name') ?></th>
                                        <th><?php echo $this->lang->line('race_count') ?></th>
                                        <th><?php echo $this->lang->line('points') ?></th>
                                        <th>Podium</th>
                                    </thead>
                                    <tbody id="entry">
                                        <tr class="to-copy">
                                            <td>
                                                <select class="form-control" name="champ_pilot[]">
                                                    <?php foreach($pilot as $key=>$row){ ?>
                                                        <option value="<?php echo $row->user_name; ?>"><?php echo $row->user_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control" name="champ_race_count[]" value="0">
                                            </td>
                                            <td>
                                                <input type="number" class="form-control" name="champ_point[]" value="0">
                                            </td>
                                            <td>
                                                <input type="number" class="form-control" name="champ_podium[]" value="0">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-primary pull-right"><?php echo $this->lang->line('btn_save') ?></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line('championship') ?></div>
                    <?php echo $this->session->userdata('notif_update'); ?>
                    <div class="panel-body table-responsive">
                        <table id="table-championship" class="table table-bordered table-hover">
                            <thead>
                                <th>#</th>
                                <th><?php echo $this->lang->line('pilot_name') ?></th>
                                <th><?php echo $this->lang->line('race_count') ?></th>
                                <th><?php echo $this->lang->line('points') ?></th>
                                <th>Podium</th>
                                <th><?php echo $this->lang->line('year') ?></th>
                                <th></th>
                            </thead>
                            <tbody>
                            <?php
                                foreach($champ as $i=>$row){
                            ?>
                                <?php
                                $result = $this->db->query("SELECT * FROM championship WHERE champ_date='".$row->champ_date."' ORDER BY champ_point DESC, champ_podium DESC")->result();

                                foreach($result as $key=>$x){
                                ?>
                                <tr class="line">
                                    <td><?php
                                        if($key+1 == 1){
                                            $add = "st";
                                        }elseif($key+1 == 2){
                                            $add = "nd";
                                        }elseif($key+1 == 3){
                                            $add = "rd";
                                        }else{
                                            $add = "th";
                                        }
                                        $rank = $key+1;
                                        echo $rank.$add;
                                        ?></td>
                                    <td class="champ-pilot"><?php echo $x->champ_pilot; ?></td>
                                    <td class="champ-race-count"><?php echo $x->champ_race_count; ?></td>
                                    <td class="champ-point"><?php echo $x->champ_point; ?></td>
                                    <td class="champ-podium"><?php echo $x->champ_podium; ?></td>
                                    <td class="champ-year"><?php echo date('Y',strtotime($x->champ_date)); ?></td>
                                    <td class="text-center">
                                        <a href="#" class="btn btn-warning btn-xs edit-champ" title="edit" data-id="<?php echo $x->champ_id; ?>" data-date="<?php echo $x->champ_date; ?>" data-toggle="modal" data-target="#modal_champ"><i class="fa fa-edit"></i></a>

                                        <a class="btn btn-danger btn-xs" href="<?php echo base_url('backend/championship/delete_data/'.$x->champ_id);?>" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"> </i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>

        <div id="modal_champ" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo $this->lang->line('championship') ?></h4>
                    </div>
                    <div class="modal-body row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <form class="form" id="form_edit_champ" method="post" action="<?php echo base_url(); ?>backend/championship/update_champ" enctype="multipart/form-data">
                            <input type="hidden" name="champ_id" value="">
                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('pilot_name') ?></label>
                                    <div class="col-xs-9">
                                        <select class="form-control" name="champ_pilot">
                                            <?php foreach($pilot as $key=>$row){ ?>
                                                <option value="<?php echo $row->user_name; ?>"><?php echo $row->user_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('race_count') ?></label>
                                    <div class="col-xs-9">
                                        <input type="number" class="form-control" name="champ_race_count" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('points') ?></label>
                                    <div class="col-xs-9">
                                        <input type="number" class="form-control" name="champ_point" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right">Podium</label>
                                    <div class="col-xs-9">
                                        <input type="number" class="form-control" name="champ_podium" required="">
                                    </div>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('btn_cancel') ?></button>
                                    <button id="btn_save_event" type="submit" name="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_save') ?></button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </section><!-- /.content -->
</div>
