<script type="text/javascript">
    // video
    $('#home_video').prop('controls',true);
    $(document).on('click','#btn_edit', function (e) {
        $('#video_title_en').attr('readonly',false);
        $('#video_title_in').attr('readonly',false);
        $('#home_video').prop('controls',false);
        $('#video_content_en').attr('readonly',false);
        $('#video_content_in').attr('readonly',false);
     
        $('#video_cencel').removeClass('hidden');
        $('#video_save').removeClass('hidden');
        $('.video').slideDown('slow');
        $('.video-image').slideDown('slow');

       $(this).addClass('hidden');
    });
    $(document).on('click','#video_cencel', function (e) {
        $('#video_title_en').attr('readonly',true);
        $('#video_title_in').attr('readonly',true);
        $('#home_video').prop('controls',true);
        $('#video_content_en').attr('readonly',true);
        $('#video_content_in').attr('readonly',true);

        $('#video_cencel').addClass('hidden');
        $('#video_save').addClass('hidden');
        $('#btn_edit').removeClass('hidden');
        $('.video').slideUp('slow');
        $('.video-image').slideUp('slow');

    });
    // video
    $(document).on('click','#video_save',function(){
        $('#form_video').submit();
        
    });
    $(document).on('submit','#form_video',function(e){
        e.preventDefault();
        var data = new FormData(this);
        var en = $('#video_title_en').val();
        var id = $('#video_title_in').val();
        if (en && id){
            $.ajax({
                url : "<?php echo site_url()?>backend/home/save_video",
                type: "post",
                data: data,
                 cache:false,
                processData:false,
                contentType:false,
                success:function(){
                    location.reload();
                }
            });
        }
        
    });

    // divider1
    $(document).on('click','#div1_edit',function(e){

        $('#divider1_en').attr('readonly',false);
        $('#divider1_in').prop('readonly',false);

        $('#div1_cencel').removeClass('hidden');
        $('#div1_save').removeClass('hidden');

        $(this).addClass('hidden');
    });
    $(document).on('click','#div1_cencel',function(e){

        $('#divider1_en').attr('readonly',true);
        $('#divider1_in').prop('readonly',true);

        $('#div1_cencel').addClass('hidden');
        $('#div1_save').addClass('hidden');
        $('#div1_edit').removeClass('hidden');
    });
    $(document).on('click','#div1_save',function(){
        $('#dividers1').submit();
        
    });
    $(document).on('submit','#dividers1',function(e){
        e.preventDefault();
        var data = new FormData(this);
        var div1_en = $('#divider1_en').val();
        var div1_id = $('#divider1_in').val();
        if (div1_en && div1_id){
            $.ajax({

                url : "<?php echo site_url()?>backend/home/save_divrs1",
                type: "post",
                data: data,
                cache:false,
                processData:false,
                contentType:false,
                success:function(){
                    location.reload();
                }
            });
        }
        
    });

    // rides

    // divider2
    $(document).on('click','#div2_edit',function(e){

        $('#divider2_en').attr('readonly',false);
        $('#divider2_in').prop('readonly',false);

        $('#div2_cencel').removeClass('hidden');
        $('#div2_save').removeClass('hidden');

        $(this).addClass('hidden');
    });
    $(document).on('click','#div2_cencel',function(e){

        $('#divider2_en').attr('readonly',true);
        $('#divider2_in').prop('readonly',true);

        $('#div2_cencel').addClass('hidden');
        $('#div2_save').addClass('hidden');
        $('#div2_edit').removeClass('hidden');
    });
    $(document).on('click','#div2_save',function(){
        $('#dividers2').submit();
        
    });
    $(document).on('submit','#dividers2',function(e){
        e.preventDefault();
        var data = new FormData(this);
        var div2_en = $('#divider2_en').val();
        var div2_id = $('#divider2_in').val();
        if (div2_en && div2_id){
            $.ajax({

                url : "<?php echo site_url()?>backend/home/save_divrs2",
                type: "post",
                data: data,
                cache:false,
                processData:false,
                contentType:false,
                success:function(){
                    location.reload();
                }
            });
        }
        
    });


    // carousel1
    $(document).on('click','#carsel1_edit',function(e){

        $('#carsel1_title_en').attr('readonly',false);
        $('#carsel1_title_in').prop('readonly',false);
        $('#carsel1_content_en').attr('readonly',false);
        $('#carsel1_content_in').prop('readonly',false);

        $('#carsel1_cencel').removeClass('hidden');
        $('#carsel1_save').removeClass('hidden');
        $('.home-carousel1').slideDown('slow');

        $(this).addClass('hidden');
    });
    $(document).on('click','#carsel1_cencel',function(e){

        $('#carsel1_title_en').attr('readonly',true);
        $('#carsel1_title_in').prop('readonly',true);
        $('#carsel1_content_en').attr('readonly',true);
        $('#carsel1_content_in').prop('readonly',true);

        $('#carsel1_cencel').addClass('hidden');
        $('#carsel1_save').addClass('hidden');
        $('#carsel1_edit').removeClass('hidden');

        $('.home-carousel1').slideUp('slow');
    });
    $(document).on('click','#carsel1_save',function(){
        $('#carsel1').submit();
        
    });
    $(document).on('submit','#carsel1',function(e){
        e.preventDefault();
        var data = new FormData(this);
        var carsel1_title_en = $('#carsel1_title_en').val();
        var carsel1_title_id = $('#carsel1_title_in').val();
        if (carsel1_title_en && carsel1_title_id){
            $.ajax({

                url : "<?php echo site_url()?>backend/home/save_carousel1",
                type: "post",
                data: data,
                cache:false,
                processData:false,
                contentType:false,
                success:function(){
                    location.reload();
                }
            });
        }
    });

    // dividers3
    $(document).on('click','#div3_edit',function(e){

        $('#divider3_en').attr('readonly',false);
        $('#divider3_in').prop('readonly',false);

        $('#div3_cencel').removeClass('hidden');
        $('#div3_save').removeClass('hidden');

        $(this).addClass('hidden');
    });
    $(document).on('click','#div3_cencel',function(e){

        $('#divider3_en').attr('readonly',true);
        $('#divider3_in').prop('readonly',true);

        $('#div3_cencel').addClass('hidden');
        $('#div3_save').addClass('hidden');
        $('#div3_edit').removeClass('hidden');
    });
    $(document).on('click','#div3_save',function(){
        $('#dividers3').submit();
        
    });
    $(document).on('submit','#dividers3',function(e){
        e.preventDefault();
        var data = new FormData(this);
        var div3_en = $('#divider3_en').val();
        var div3_id = $('#divider3_in').val();
        if (div3_en && div3_id){
            $.ajax({

                url : "<?php echo site_url()?>backend/home/save_divrs3",
                type: "post",
                data: data,
                cache:false,
                processData:false,
                contentType:false,
                success:function(){
                    location.reload();
                }
            });
        }
    });

    // carousel2
    $(document).on('click','#carsel2_edit',function(e){

        $('#carsel2_title_en').attr('readonly',false);
        $('#carsel2_title_in').prop('readonly',false);
        $('#carsel2_content_en').attr('readonly',false);
        $('#carsel2_content_in').prop('readonly',false);

        $('#carsel2_cencel').removeClass('hidden');
        $('#carsel2_save').removeClass('hidden');
        $('.home-carousel2').slideDown('slow');

        $(this).addClass('hidden');
    });
    $(document).on('click','#carsel2_cencel',function(e){

        $('#carsel2_title_en').attr('readonly',true);
        $('#carsel2_title_in').prop('readonly',true);
        $('#carsel2_content_en').attr('readonly',true);
        $('#carsel2_content_in').prop('readonly',true);

        $('#carsel2_cencel').addClass('hidden');
        $('#carsel2_save').addClass('hidden');
        $('#carsel2_edit').removeClass('hidden');

        $('.home-carousel2').slideUp('slow');
    });
    $(document).on('click','#carsel2_save',function(){
        $('#carsel2').submit();
        
    });
    $(document).on('submit','#carsel2',function(e){
        e.preventDefault();
        var data = new FormData(this);
        var carsel2_title_en = $('#carsel2_title_en').val();
        var carsel2_title_id = $('#carsel2_title_in').val();
        if (carsel2_title_en && carsel2_title_id){
            $.ajax({

                url : "<?php echo site_url()?>backend/home/save_carousel2",
                type: "post",
                data: data,
                cache:false,
                processData:false,
                contentType:false,
                success:function(){
                    location.reload();
                }
            });
        }
    });

// long dividers
    //long1
    $(document).on('click','#dl1_edit',function(e){

        $('#dl1_content_en').attr('readonly',false);
        $('#dl1_content_in').prop('readonly',false);

        $('#dl1_cencel').removeClass('hidden');
        $('#dl1_save').removeClass('hidden');

        $(this).addClass('hidden');
    });
    $(document).on('click','#dl1_cencel',function(e){

        $('#dl1_content_en').attr('readonly',true);
        $('#dl1_content_in').prop('readonly',true);

        $('#dl1_cencel').addClass('hidden');
        $('#dl1_save').addClass('hidden');
        $('#dl1_edit').removeClass('hidden');
    });
    $(document).on('click','#dl1_save',function(){
        $('#long_div1').submit();
        
    });
    $(document).on('submit','#long_div1',function(e){
        e.preventDefault();
        var data = new FormData(this);
        var dl1_content_en = $('#dl1_content_en').val();
        var dl1_content_id = $('#dl1_content_in').val();
        if (dl1_content_en && dl1_content_id){
            $.ajax({

                url : "<?php echo site_url()?>backend/home/save_divrs3",
                type: "post",
                data: data,
                cache:false,
                processData:false,
                contentType:false,
                success:function(){
                    location.reload();
                }
            });
        }
    });
    //long2
    $(document).on('click','#dl2_edit',function(e){

        $('#dl2_content_en').attr('readonly',false);
        $('#dl2_content_in').prop('readonly',false);

        $('#dl2_cencel').removeClass('hidden');
        $('#dl2_save').removeClass('hidden');

        $(this).addClass('hidden');
    });
    $(document).on('click','#dl2_cencel',function(e){

        $('#dl2_content_en').attr('readonly',true);
        $('#dl2_content_in').prop('readonly',true);

        $('#dl2_cencel').addClass('hidden');
        $('#dl2_save').addClass('hidden');
        $('#dl2_edit').removeClass('hidden');
    });
    $(document).on('click','#dl2_save',function(){
        $('#long_div2').submit();
        
    });
    $(document).on('submit','#long_div2',function(e){
        e.preventDefault();
        var data = new FormData(this);
        var dl2_content_en = $('#dl2_content_en').val();
        var dl2_content_id = $('#dl2_content_in').val();
        if (dl2_content_en && dl2_content_id){
            $.ajax({

                url : "<?php echo site_url()?>backend/home/save_divrs3",
                type: "post",
                data: data,
                cache:false,
                processData:false,
                contentType:false,
                success:function(){
                    location.reload();
                }
            });
        }
    });
    //long3
    $(document).on('click','#dl3_edit',function(e){

        $('#dl3_content_en').attr('readonly',false);
        $('#dl3_content_in').prop('readonly',false);

        $('#dl3_cencel').removeClass('hidden');
        $('#dl3_save').removeClass('hidden');

        $(this).addClass('hidden');
    });
    $(document).on('click','#dl3_cencel',function(e){

        $('#dl3_content_en').attr('readonly',true);
        $('#dl3_content_in').prop('readonly',true);

        $('#dl3_cencel').addClass('hidden');
        $('#dl3_save').addClass('hidden');
        $('#dl3_edit').removeClass('hidden');
    });
    $(document).on('click','#dl3_save',function(){
        $('#long_div3').submit();
        
    });
    $(document).on('submit','#long_div3',function(e){
        e.preventDefault();
        var data = new FormData(this);
        var dl3_content_en = $('#dl3_content_en').val();
        var dl3_content_id = $('#dl3_content_in').val();
        if (dl3_content_en && dl3_content_id){
            $.ajax({

                url : "<?php echo site_url()?>backend/home/save_divrs3",
                type: "post",
                data: data,
                cache:false,
                processData:false,
                contentType:false,
                success:function(){
                    location.reload();
                }
            });
        }
    });
    //long4
    $(document).on('click','#dl4_edit',function(e){

        $('#dl4_content_en').attr('readonly',false);
        $('#dl4_content_in').prop('readonly',false);

        $('#dl4_cencel').removeClass('hidden');
        $('#dl4_save').removeClass('hidden');

        $(this).addClass('hidden');
    });
    $(document).on('click','#dl4_cencel',function(e){

        $('#dl4_content_en').attr('readonly',true);
        $('#dl4_content_in').prop('readonly',true);

        $('#dl4_cencel').addClass('hidden');
        $('#dl4_save').addClass('hidden');
        $('#dl4_edit').removeClass('hidden');
    });
    $(document).on('click','#dl4_save',function(){
        $('#long_div4').submit();
        
    });
    $(document).on('submit','#long_div4',function(e){
        e.preventDefault();
        var data = new FormData(this);
        var dl4_content_en = $('#dl4_content_en').val();
        var dl4_content_id = $('#dl4_content_in').val();
        if (dl4_content_en && dl4_content_id){
            $.ajax({

                url : "<?php echo site_url()?>backend/home/save_divrs3",
                type: "post",
                data: data,
                cache:false,
                processData:false,
                contentType:false,
                success:function(){
                    location.reload();
                }
            });
        }
    });

    // carousel3
    $(document).on('click','#carsel3_edit',function(e){

        $('#carsel3_title_en').attr('readonly',false);
        $('#carsel3_title_in').prop('readonly',false);
        $('#carsel3_content_en').attr('readonly',false);
        $('#carsel3_content_in').prop('readonly',false);

        $('#carsel3_cencel').removeClass('hidden');
        $('#carsel3_save').removeClass('hidden');
        $('.home-carousel3').slideDown('slow');

        $(this).addClass('hidden');
    });
    $(document).on('click','#carsel3_cencel',function(e){

        $('#carsel3_title_en').attr('readonly',true);
        $('#carsel3_title_in').prop('readonly',true);
        $('#carsel3_content_en').attr('readonly',true);
        $('#carsel3_content_in').prop('readonly',true);

        $('#carsel3_cencel').addClass('hidden');
        $('#carsel3_save').addClass('hidden');
        $('#carsel3_edit').removeClass('hidden');

        $('.home-carousel3').slideUp('slow');
    });
    $(document).on('click','#carsel3_save',function(){
        $('#carsel3').submit();
        
    });
    $(document).on('submit','#carsel3',function(e){
        e.preventDefault();
        var data = new FormData(this);
        var carsel3_title_en = $('#carsel3_title_en').val();
        var carsel3_title_id = $('#carsel3_title_in').val();
        if (carsel3_title_en && carsel3_title_id){
            $.ajax({

                url : "<?php echo site_url()?>backend/home/save_carousel3",
                type: "post",
                data: data,
                cache:false,
                processData:false,
                contentType:false,
                success:function(){
                    location.reload();
                }
            });
        }
    });
</script>