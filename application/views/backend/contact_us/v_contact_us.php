<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line('contact') ?>
        </h1>
    </section>

    <section class="content">
        <div class="row" id="content">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/contact_us/edit_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line('content') ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12"> 
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control" name="div_contact_en" rows="5" required><?php echo isset($content_en->general_content_en) ? $content_en->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12"> 
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control" name="div_contact_in" rows="5" required><?php echo isset($content_in->general_content_in) ? $content_in->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save text-center">
                                <button type="submit" class="btn btn-primary" title="Update">
                                    <?php echo $this->lang->line('btn_save') ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row" id="message">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <?php echo $this->lang->line('messages') ?>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_message');?>
                            <table class="table table-hover table-bordered" id="contact">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">#</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line('date') ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line('from') ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line('phone') ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line('subject') ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line('action') ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line('status') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <?php foreach($message as $key => $row):?>
                                        <?php
                                            if($row->contact_status == 0) {
                                                $status = "<i class='fa fa-circle' style='color:green'></i> <i><b>".$this->lang->line('unread')."</b></i>";
                                            }
                                            else{
                                                $status = "<i class='fa fa-check'></i> <i>".$this->lang->line('read')."</i>";
                                            }
                                        ?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td><?php echo date('d/m/y',strtotime($row->contact_date));?></td>
                                        <td><?php echo $row->contact_name;?></td>
                                        <td><?php echo $row->contact_phone;?></td>
                                        <td><?php echo $row->contact_subject;?></td>
                                        <td><center>
                                            <a href="#" id="<?php echo @$row->contact_id;?>" class="btn btn-success btn-xs view-page" title="<?php echo $this->lang->line("detail_message"); ?>" data-target="#view_message<?php echo $row->contact_id; ?>" data-toggle="modal"><i class="fa fa-file-text"></i> <?php echo $this->lang->line("detail"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/Contact_us/delete_message/'.@$row->contact_id);?>" class="btn-delete btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_message"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                        <td>
                                        <center><?php echo $status;?></center>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <?php foreach($message as $key => $view_message):?>
        <div class="modal fade" id="view_message<?php echo $view_message->contact_id; ?>" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" value="Refresh Page" onClick="window.location.reload()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line('messages') ?><br></h4>
                        </div>
                        <div class="modal-body">
                            <h4><i class="fa fa-user"></i> <?php echo $view_message->contact_name;?> (<a href="mailto:<?php echo $view_message->contact_email;?>"><?php echo $view_message->contact_email;?></a>)</h4>
                            <div class="panel-body">                
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="control-label"><?php echo $this->lang->line('from') ?></label>
                                        <label class="form-control" name="contact_name"><?php echo $view_message->contact_name;?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="control-label"><?php echo $this->lang->line('phone') ?></label>
                                        <label class="form-control" name="contact_name"><?php echo $view_message->contact_phone;?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="control-label"><?php echo $this->lang->line('date') ?></label>
                                        <label class="form-control" name="contact_date"><?php echo date('d/m/y', strtotime($view_message->contact_date));?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="control-label"><?php echo $this->lang->line('subject') ?></label>
                                        <label class="form-control" name="contact_subject"><?php echo $view_message->contact_subject;?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="control-label"><?php echo $this->lang->line('messages') ?></label>
                                        <textarea class="form-control" rows="5" name="contact_message" readonly=""><?php echo $view_message->contact_message;?></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" value="Refresh Page" onClick="window.location.reload()"><?php echo $this->lang->line('btn_close') ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php endforeach;?>
        <div class="row" id="info">
            <div class="col-xs-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/contact_us/info');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("info"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_contact');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <label class="control-label"><p>
                                                    <?php echo $this->lang->line('phone_number') ?>
                                                </p></label>
                                                <textarea class="form-control" name="phone" rows="5" required><?php echo isset($phone->general_content_en) ? $phone->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                            <label class="control-label"><p>
                                                <?php echo $this->lang->line('address') ?>
                                            </p></label>
                                                <textarea class="form-control" name="address" rows="5" required><?php echo isset($address->general_content_en) ? $address->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                            <label class="control-label"><p>
                                                <?php echo $this->lang->line('gmap_link') ?>
                                            </p></label>
                                                <input class="form-control" name="location" rows="5" value="<?php echo isset($location->general_content_en) ? $location->general_content_en:''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save text-center">
                                <button type="submit" class="btn btn-primary" title="Update">
                                    <?php echo $this->lang->line('btn_save') ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row" id="socmed">
            <div class="col-xs-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/contact_us/save_socmed');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading">Social Media</div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('socmed');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                                        <div class="form-group">
                                            <label class="control-label">TripAdvisor</label>
                                            <input class="form-control" name="socmed_tripadvisor" value="<?php echo $socmed_tripadvisor; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                                        <div class="form-group">
                                            <label class="control-label">Facebook</label>
                                            <input class="form-control" name="socmed_facebook" value="<?php echo $socmed_facebook; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                                        <div class="form-group">
                                            <label class="control-label">Twitter</label>
                                            <input class="form-control" name="socmed_twitter" value="<?php echo $socmed_twitter; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                                        <div class="form-group">
                                            <label class="control-label">Instagram</label>
                                            <input class="form-control" name="socmed_instagram" value="<?php echo $socmed_instagram; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                                        <div class="form-group">
                                            <label class="control-label">Youtube</label>
                                            <input class="form-control" name="socmed_youtube" value="<?php echo $socmed_youtube; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save text-center">
                                <button type="submit" class="btn btn-primary" title="Update">
                                    <?php echo $this->lang->line('btn_save') ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section><!-- /.content -->
</div>