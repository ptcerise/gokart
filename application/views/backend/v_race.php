 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line('race') ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line('input_race') ?></div>
                    <div class="panel-body">
                        <div class="form-group col-xs-12 col-md-4 text-center">
                            <label class="control-label col-xs-12 col-sm-6"><?php echo $this->lang->line('racers') ?></label>
                            <div class="input-group col-xs-12 col-sm-6">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input type="number" min="1" max="<?php echo count($pilot); ?>" class="form-control" id="racer_count" value="1"/>
                            </div>
                        </div>
                        <button id="btn_inp_racer" class="btn btn-primary"><?php echo $this->lang->line('btn_add') ?></button>
                        <hr>

                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 well">
                            <?php echo $this->session->userdata('notif_race'); ?>
                            <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>backend/race/save_records">
                                <div class="form-group row">
                                    <label class="col-xs-1 control-label text-right"><?php echo $this->lang->line('date') ?></label>
                                    <div class="col-xs-5">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control datepicker" name="race_date" value="<?php echo date('m/d/Y'); ?>"/>
                                        </div>
                                    </div>
                                
                                    <label class="col-xs-2 control-label text-right"><?php echo $this->lang->line('race_type') ?></label>
                                    <div class="col-xs-4">
                                        <select class="form-control" name="race_type">
                                            <?php
                                                $type = array('pro', 'casual');

                                                for($i=0;$i<count($type);$i++){
                                            ?>
                                                <option value="<?php echo $type[$i]; ?>">
                                                    <?php
                                                        $lang = $this->session->userdata('be_lang');
                                                        if($type[$i] == "pro"){
                                                            if($lang == "indonesia" || $lang ==""){
                                                                echo "Advance";
                                                            }else{
                                                                echo "Advance";
                                                            }
                                                        }else{
                                                            if($lang == "indonesia" || $lang ==""){
                                                                echo "Pemula";
                                                            }else{
                                                                echo "Rookie";
                                                            }
                                                        }
                                                    ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                <table class="table table-bordered table-hover well">
                                    <thead>
                                        <th><?php echo $this->lang->line('winner') ?></th>
                                        <th><?php echo $this->lang->line('pilot_name') ?></th>
                                        <th><?php echo $this->lang->line('points') ?></th>
                                        <th class="text-center" colspan="2"><?php echo $this->lang->line('race_time') ?> (Sec : MiliSec)</th>
                                        <th class="text-center" colspan="2"><?php echo $this->lang->line('best_lap') ?> (Sec : MiliSec)</th>
                                        <th><?php echo $this->lang->line('additional_info') ?></th>
                                    </thead>
                                    <tbody id="entry">
                                        <tr class="to-copy">
                                            <td class="text-center">
                                                <input type="radio" name="race_winner" value="" required="">
                                            </td>
                                            <td>
                                                <select class="form-control select-pilot" name="race_pilot_id[]">
                                                    <?php foreach($pilot as $key=>$row){ ?>
                                                        <option value="<?php echo $row->user_id; ?>"><?php echo $row->user_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td class="text-center">
                                                <input class="form-control" type="number" name="race_point[]" value="0" required="">
                                            </td>
                                            <!-- RACE TIME -->
                                            <td class="race-time pull-right">
                                                <input min="0" max="60" step="any" type="number" class="form-control" name="race_time_s[]" value="0">
                                            </td>
                                            <td class="race-time">
                                                <input min="0" max="1000" step="any" type="number" class="form-control" name="race_time_ms[]" value="0">
                                            </td>
                                            <!-- BEST LAP -->
                                            <td class="race-time pull-right">
                                                <input min="0" max="60" step="any" type="number" class="form-control" name="race_best_lap_s[]" value="0">
                                            </td>
                                            <td class="race-time">
                                                <input min="0" max="1000" step="any" type="number" class="form-control" name="race_best_lap_ms[]" value="0">
                                            </td> 

                                            <td>
                                                <select class="form-control" name="race_info[]">
                                                    <?php
                                                        $info = array('','dnf');
                                                        $title = array('- EMPTY -', 'Did Not Finished');

                                                        for($i=0;$i<count($info);$i++){
                                                    ?>
                                                        <option value="<?php echo $info[$i]; ?>">
                                                            <?php echo $title[$i]; ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>
                                <button type="submit" class="btn btn-primary pull-right"><?php echo $this->lang->line('btn_save') ?></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line('pro_race_rec') ?></div>
                    <?php echo $this->session->userdata('notif_race_pro'); ?>
                    <div class="panel-body table-responsive">
                        <table id="table_racepro" class="table table-bordered table-hover tbl-race">
                            <thead>
                                <th>#</th>
                                <th><?php echo $this->lang->line('pilot_name') ?></th>
                                <th><?php echo $this->lang->line('points') ?></th>
                                <th><?php echo $this->lang->line('race_time') ?></th>
                                <th><?php echo $this->lang->line('best_lap') ?></th>
                                <th><?php echo $this->lang->line('date') ?></th>
                                <th><?php echo $this->lang->line('additional_info') ?></th>
                                <th><?php echo $this->lang->line('action') ?></th>
                            </thead>
                            
                            
                            <tbody>
                            <?php
                                foreach($pro_race as $key=>$row){
                            ?>
                                <tr>
                                    <td><?php
                                        if($key+1 == 1){
                                            $add = "st";
                                        }elseif($key+1 == 2){
                                            $add = "nd";
                                        }elseif($key+1 == 3){
                                            $add = "rd";
                                        }else{
                                            $add = "th";
                                        }
                                        $rank = $key+1;
                                        echo $rank;
                                        ?></td>
                                    <td class="race-pilot">
                                        <?php
                                            echo $pilot = $this->db->query("SELECT * FROM user WHERE user_id='".$row->race_pilot_id."'")->row()->user_name;
                                        ?>
                                    </td>
                                    <td class="race-point">
                                        <?php echo $row->race_point; ?>
                                    </td>
                                    <td class="race-time"><?php
                                        $input = $row->race_time;

                                        $uSec = $input % 1000;
                                        $input = floor($input / 1000);

                                        $seconds = $input % 60;
                                        $input = floor($input / 60); 

                                        echo "<span class='sec'>".$seconds."s </span>"."<span class='msec'>".$uSec."ms</span>";
                                    ?></td>
                                    <td class="race-best-lap"><?php
                                        $input = $row->race_best_lap;

                                        $uSec = $input % 1000;
                                        $input = floor($input / 1000);

                                        $seconds = $input % 60;
                                        $input = floor($input / 60);

                                        echo "<span class='sec'>".$seconds."s </span>"."<span class='msec'>".$uSec."ms</span>";
                                    ?></td>
                                    <td class="race-date"><?php echo date('d/m/y',strtotime($row->race_date)); ?></td>
                                    <td class="race-info">
                                        <?php echo $row->race_info == "dnf"?"Did Not Finished":"-- EMPTY --"; ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" class="btn btn-warning btn-xs edit-race" title="edit"
                                            data-id="<?php echo $row->race_id; ?>"
                                            data-info="<?php echo $row->race_info; ?>"
                                            data-type="<?php echo $row->race_type; ?>"
                                            data-toggle="modal" data-target="#modal_race">
                                            <i class="fa fa-edit"></i></a>

                                        <a class="btn btn-danger btn-xs" href="<?php echo base_url('backend/race/delete_data/'.$row->race_id);?>" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"> </i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line('casual_race_rec') ?></div>
                    <?php echo $this->session->userdata('notif_race_casual'); ?>
                    <div class="panel-body table-responsive">
                        <table id="table_racecasual" class="table table-bordered table-hover tbl-race">
                            <thead>
                                <th>#</th>
                                <th><?php echo $this->lang->line('pilot_name') ?></th>
                                <th><?php echo $this->lang->line('points') ?></th>
                                <th><?php echo $this->lang->line('race_time') ?></th>
                                <th><?php echo $this->lang->line('best_lap') ?></th>
                                <th><?php echo $this->lang->line('date') ?></th>
                                <th><?php echo $this->lang->line('additional_info') ?></th>
                                <th><?php echo $this->lang->line('action') ?></th>
                            </thead>
                            
                            <tbody>
                            <?php
                                foreach($casual_race as $key=>$row){
                            ?>
                                <tr>
                                    <td><?php
                                        if($key+1 == 1){
                                            $add = "st";
                                        }elseif($key+1 == 2){
                                            $add = "nd";
                                        }elseif($key+1 == 3){
                                            $add = "rd";
                                        }else{
                                            $add = "th";
                                        }
                                        $rank = $key+1;
                                        echo $rank;
                                        ?></td>
                                    <td class="race-pilot">
                                        <?php
                                            echo $pilot = $this->db->query("SELECT * FROM user WHERE user_id='".$row->race_pilot_id."'")->row()->user_name;
                                        ?>
                                    </td>
                                    <td class="race-point">
                                        <?php echo $row->race_point; ?>
                                    </td>
                                    <td class="race-time"><?php
                                        $input = $row->race_time;

                                        $uSec = $input % 1000;
                                        $input = floor($input / 1000);

                                        $seconds = $input % 60;
                                        $input = floor($input / 60);

                                        echo "<span class='sec'>".$seconds."s </span>"."<span class='msec'>".$uSec."ms</span>";
                                    ?></td>
                                    <td class="race-best-lap"><?php
                                        $input = $row->race_best_lap;

                                        $uSec = $input % 1000;
                                        $input = floor($input / 1000);

                                        $seconds = $input % 60;
                                        $input = floor($input / 60);

                                        echo "<span class='sec'>".$seconds."s </span>"."<span class='msec'>".$uSec."ms</span>";
                                    ?></td>
                                    <td class="race-date"><?php echo date('d/m/y',strtotime($row->race_date)); ?></td>
                                    <td class="race-info">
                                        <?php echo $row->race_info == "dnf"?"Did not finished":"-- EMPTY --"; ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" class="btn btn-warning btn-xs edit-race" title="edit"
                                            data-id="<?php echo $row->race_id; ?>"
                                            data-info="<?php echo $row->race_info; ?>"
                                            data-type="<?php echo $row->race_type; ?>"
                                            data-toggle="modal" data-target="#modal_race">
                                            <i class="fa fa-edit"></i></a>

                                        <a class="btn btn-danger btn-xs" href="<?php echo base_url('backend/race/delete_data/'.$row->race_id);?>" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"> </i></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
		</div>

        <div id="modal_race" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo $this->lang->line('btn_edit') ?></h4>
                    </div>
                    <div class="modal-body row">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                            <form class="form" id="form_edit_race" method="post" action="<?php echo base_url(); ?>backend/race/update_race" enctype="multipart/form-data">
                            <input type="hidden" name="race_id" value="">
                            <input type="hidden" name="race_type" value="">
                                <div class="form-group row">
                                    <label class="col-xs-4 control-label text-right"><?php echo $this->lang->line('pilot_name') ?></label>
                                    <div class="col-xs-8" id="race_pilot">
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xs-4 control-label text-right"><?php echo $this->lang->line('points') ?></label>
                                    <div class="col-xs-8">
                                        <input type="number" class="form-control" name="race_point" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xs-4 control-label text-right"><?php echo $this->lang->line('race_time') ?></label>
                                    <div class="col-xs-8">
                                        <table>
                                            <tr>
                                                <td class="race-time pull-right">
                                                    <input min="0" max="60" step="any" type="number" class="form-control" name="race_time_s" value="">
                                                </td>
                                                <td>sec</td>
                                                <td class="race-time">
                                                    <input min="0" max="1000" step="any" type="number" class="form-control" name="race_time_ms" value="">
                                                </td>
                                                <td>milisec</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xs-4 control-label text-right"><?php echo $this->lang->line('best_lap') ?></label>
                                    <div class="col-xs-8">
                                        <table>
                                            <tr>
                                                <td class="race-time pull-right">
                                                    <input min="0" max="60" step="any" type="number" class="form-control" name="race_best_lap_s" value="">
                                                </td>
                                                <td>sec</td>
                                                <td class="race-time">
                                                    <input min="0" max="1000" step="any" type="number" class="form-control" name="race_best_lap_ms" value="">
                                                </td>
                                                <td>milisec</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xs-4 control-label text-right"><?php echo $this->lang->line('additional_info') ?></label>
                                    <div class="col-xs-8">
                                        <select class="form-control" name="race_info">
                                            <?php
                                                $info = array('','dnf');
                                                $title = array('- EMPTY -', 'Did Not Finished');

                                                for($i=0;$i<count($info);$i++){
                                            ?>
                                                <option value="<?php echo $info[$i]; ?>">
                                                    <?php echo $title[$i]; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('btn_cancel') ?></button>
                                    <button id="btn_save" type="submit" name="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_save') ?></button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </section><!-- /.content -->
</div>
