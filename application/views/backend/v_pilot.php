 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line('member') ?>
        </h1>
    </section>

    <!-- Main content -->
     <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line('member') ?>
                        <div class="caption pull-right">
                            <a href="#" class="page btn btn-success btn-xs btn-add-pilot" title="Add Pilot" data-toggle="modal" data-target="#modal_pilot"><i class="fa fa-plus"></i> <?php echo $this->lang->line('member') ?></a>
                        </div>
                    </div>
                    <?php echo $this->session->userdata('notif'); ?>
                    <div class="panel-body table-responsive">
                        <table id="table-pilot" class="table table-bordered table-hover">
                            <thead>
                                <th>#</th>
                                <th><?php echo $this->lang->line('pilot_name') ?></th>
                                <th><?php echo $this->lang->line('email') ?></th>
                                <th><?php echo $this->lang->line('phone') ?></th>
                                <th><?php echo $this->lang->line('pinbb') ?></th>
                                <th><?php echo $this->lang->line('address') ?></th>
                                <th><?php echo $this->lang->line('postcode') ?></th>
                                <th><?php echo $this->lang->line('city') ?></th>
                                <th><?php echo $this->lang->line('action') ?></th>
                            </thead>
                            <tbody>
                            <?php
                                foreach($user as $key=>$row){
                                $profile = $db_access->readtable('profile','',array('temp_id'=>$row->user_id))->row();
                            ?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td class="pilot-name"><?php echo $row->user_name; ?></td>
                                    <td class="pilot-email"><?php echo $row->user_email; ?></td>
                                    <td class="pilot-phone"><?php echo $profile->profile_phone; ?></td>
                                    <td class="pilot-pinbb"><?php echo $profile->profile_pin_bb; ?></td>
                                    <td class="pilot-address"><?php echo $profile->profile_address; ?></td>
                                    <td class="pilot-postcode"><?php echo $profile->profile_postcode; ?></td>
                                    <td class="pilot-city"><?php echo $profile->profile_city; ?></td>
                                    <td class="text-center">
                                        <a href="#" class="btn btn-warning btn-xs edit-pilot" title="edit" data-toggle="modal" data-target="#modal_pilot" data-id="<?php echo $row->user_id; ?>" data-img="<?php echo base_url('assets/upload/pilot/'.$profile->profile_photo); ?>"><i class="fa fa-edit"></i></a>

                                        <a href="<?php echo base_url('backend/pilot/delete_pilot/'.$row->user_id);?>" class="btn btn-danger btn-xs" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"> </i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL ADD PILOT -->
        <div id="modal_pilot" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo $this->lang->line('member') ?></h4>
                    </div>
                    <div class="modal-body row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <form class="form" id="form_save_pilot" method="post" action="<?php echo base_url(); ?>backend/pilot/save_pilot" enctype="multipart/form-data">
                                <input type="hidden" name="pilot_id" value="">
                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('photo') ?></label>
                                    <div class="col-xs-9">
                                        <input id="imgInp" type="file" class="form-control" name="profile_photo">
                                        <div class="photo-preview">
                                            <img class="img img-responsive img-bordered" id="imgPrev" src="<?php echo base_url('assets/img/pilot-default.jpg'); ?>" alt="your image" />
                                        </div>
                                        <div class="upload-rules">
                                            <ul>
                                                <li>* <?php echo $this->lang->line('max_size') ?> <strong>1Mb</strong></li>
                                                <li>* <?php echo $this->lang->line('resolution') ?> <strong>250 x 320</strong> pixel</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('pilot_name') ?></label>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control" name="user_name" required="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('email') ?></label>
                                    <div class="col-xs-9">
                                        <input type="email" class="form-control" name="user_email" required="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('pass') ?></label>
                                    <div class="col-xs-9">
                                        <input type="password" class="form-control" name="user_pass" required="">
                                        <input type="checkbox" id="show_pass"> <?php echo $this->lang->line('show_pass') ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('pass_confirm') ?></label>
                                    <div class="col-xs-9">
                                        <input type="password" class="form-control" name="user_pass_confirm" required="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('phone') ?></label>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control" name="profile_phone" required="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('pinbb') ?></label>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control" name="profile_pin_bb" required="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('address') ?></label>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control" name="profile_address" required="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('postcode') ?></label>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control" name="profile_postcode" required="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xs-3 control-label text-right"><?php echo $this->lang->line('city') ?></label>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control" name="profile_city" required="">
                                    </div>
                                </div>

                                <div class="col-xs-12 text-center">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('btn_cancel') ?></button>
                                    <button id="btn_save_pilot" type="button" name="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_save') ?></button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </section><!-- /.content -->
</div>
