 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line('gallery') ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- IMAGE UPLOAD -->
			<div class="col-xs-12">
                <div class="panel panel-info">
                    <div id="upload" class="panel-heading">
                        <?php echo $this->lang->line('upload_image') ?>
                    </div>
                    <div class="panel-body">
                        <?php echo $this->session->userdata('upload_images'); ?>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="well">
                                    <form class="form" method="post" action="<?php echo base_url(); ?>backend/gallery/save_images" enctype="multipart/form-data">
                                        <div class="form-group upload-image">
                                            <label class="control-label"><?php echo $this->lang->line('choose_image') ?></label>
                                            <!-- The file input field used as target for the file upload widget -->
                                            <input id="gallery_upl" class="form-control btn btn-default" type="file" name="upl_img">
                                            <div class="upload-rules"><ul>
                                                <li>* <?php echo $this->lang->line('max_size') ?> <strong>2Mb</strong></li>
                                            </ul></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line('caption_en') ?></label>
                                            <!-- The file input field used as target for the file upload widget -->
                                            <input type="text" class="form-control" name="desc_en" value="">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line('caption_in') ?></label>
                                            <!-- The file input field used as target for the file upload widget -->
                                            <input type="text" class="form-control" name="desc_in" value="">
                                        </div>
                                        <div class="form-group text-right">
                                            <button class="btn btn-primary" type="submit"><?php echo $this->lang->line('btn_save') ?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 img-gallery-upl">
                                <img id="imgPrev" class="img-responsive img-bordered" src="<?php echo base_url('assets/img/blank_large.png'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- IMAGE LIST -->
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div id="edit" class="panel-heading">
                        <?php echo $this->lang->line('gallery') ?>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata('edit'); ?>
                        <div class="row">
                            <div class="panel-body table-responsive">
                        <table id="table-pilot" class="table table-bordered table-hover">
                            <thead>
                                <th>#</th>
                                <th><?php echo $this->lang->line('image_prev') ?></th>
                                <th><?php echo $this->lang->line('caption_en') ?></th>
                                <th><?php echo $this->lang->line('caption_in') ?></th>
                                <th><?php echo $this->lang->line('action') ?></th>
                            </thead>
                            <tbody>
                            <?php
                                foreach($gallery as $key=>$row){
                            ?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td><img class="img-responsive img-on-table" alt="img-gallery" src="<?php echo base_url('assets/upload/gallery/thumbnail/'.$row->media_url); ?>"></td>
                                    <td class="content_en"><?php echo $row->media_content_en; ?></td>
                                    <td class="content_in"><?php echo $row->media_content_in; ?></td>
                                    <td class="text-center">
                                        <a href="#" class="btn btn-warning btn-xs edit-gallery" title="edit" data-toggle="modal" data-target="#update_gallery" data-id="<?php echo $row->media_id; ?>"><i class="fa fa-edit"></i></a>

                                        <a href="<?php echo base_url('backend/gallery/delete_image/'.$row->media_id);?>" class="btn btn-danger btn-xs" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"> </i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="update_gallery" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line('gallery') ?></h4>
                        </div>
                        <div class="modal-body row">
                            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                <form class="form" method="post" action="<?php echo base_url(); ?>backend/gallery/save_images" enctype="multipart/form-data">
                                    <input type="hidden" name="img_id" value="">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $this->lang->line('caption_en') ?></label>
                                        <!-- The file input field used as target for the file upload widget -->
                                        <input type="text" id="desc_en" class="form-control" name="desc_en" value="">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $this->lang->line('caption_in') ?></label>
                                        <!-- The file input field used as target for the file upload widget -->
                                        <input type="text" id="desc_in" class="form-control" name="desc_in" value="">
                                    </div>
                                    <div class="col-xs-12 text-center">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('btn_cancel') ?></button>
                                    <button type="submit" name="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_save') ?></button>
                                </div>
                                </form>
                            </div>

                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

		</div>
    </section><!-- /.content -->
</div>