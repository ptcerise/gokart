-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2016 at 12:02 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yogyabis_gokart`
--

-- --------------------------------------------------------

--
-- Table structure for table `championship`
--

CREATE TABLE `championship` (
  `champ_id` int(11) NOT NULL,
  `champ_pilot` varchar(50) NOT NULL,
  `champ_race_count` int(11) NOT NULL,
  `champ_point` int(11) NOT NULL,
  `champ_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `championship`
--

INSERT INTO `championship` (`champ_id`, `champ_pilot`, `champ_race_count`, `champ_point`, `champ_date`) VALUES
(3, 'CeriseIT', 6, 90, '2015-08-02'),
(4, 'Clasher', 6, 88, '2015-08-02'),
(7, 'CeriseIT', 6, 90, '2016-07-30'),
(8, 'Clasher', 6, 87, '2016-07-30'),
(9, 'DarkRacer', 6, 82, '2016-07-30'),
(10, 'DoomHammer', 6, 76, '2016-07-30'),
(11, 'Exkalibur', 6, 70, '2016-07-30');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(100) NOT NULL,
  `event_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_name`, `event_date`) VALUES
(2, 'August Pro Race', '2016-08-13'),
(3, 'September Pro Race', '2016-09-24'),
(4, 'October Pro Race', '2016-10-15'),
(5, 'November Pro Race', '2016-11-12'),
(6, 'Christmas Casual Race', '2016-12-25'),
(7, 'New Year Race 2017', '2017-01-07');

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE `general` (
  `general_id` int(11) NOT NULL,
  `general_title_en` varchar(100) NOT NULL,
  `general_title_in` varchar(100) NOT NULL,
  `general_content_en` text,
  `general_content_in` text,
  `general_url` varchar(150) DEFAULT NULL,
  `general_page` varchar(100) NOT NULL,
  `general_section` varchar(100) NOT NULL,
  `general_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `temp_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general`
--

INSERT INTO `general` (`general_id`, `general_title_en`, `general_title_in`, `general_content_en`, `general_content_in`, `general_url`, `general_page`, `general_section`, `general_date`, `temp_id`) VALUES
(3, ' Let the race begin !', 'Mulai balapan !', '', '', NULL, 'home', 'banner_text', '2016-07-29 03:57:51', NULL),
(4, '', '', '', '<p>Temukan\r\ndan buktikan gokart dengan power 4 stroke, 350 cc dan kekuatan sampai\r\ndengan 8.4\r\nhp/3600 rpm, sangat mantap dikendarai oleh siapa saja yang ingin\r\nmenguji adrenalin &amp; keberanian diri.<small></small></p>', NULL, 'home', 'kart_content', '2016-07-29 04:54:07', NULL),
(5, '', '', '', '<p>Yogya\r\nGokart memiliki 4 variasi track yang berbeda dengan tingkat kesulitan\r\nmasing – masing. Kami memiliki panjang lintasan kurang lebih 460\r\nmeter dengan panjang lintasan yang berbeda tergantung variasi track\r\nyang dipilih.\r\n</p><p>Lintasan\r\nkami terbuat dari aspal halus dengan diberi pengaman berupa ban\r\ndipinggir\r\njalan yang menikung sehingga sangat aman bagi anda yang baru belajar\r\ngokart sekalipun.</p><p></p>', NULL, 'home', 'track_content', '2016-07-29 08:57:33', NULL),
(6, '', '', '-', 'INGIN BERSENANG - SENANG DI KIDS FUN SEPANJANG TAHUN?', NULL, 'home', 'divider1', '2016-07-29 15:25:42', NULL),
(7, '', '', '-', 'INGIN BERMAIN DENGAN WAHANA-WAHANA MENYENANGKAN?', NULL, 'home', 'divider2', '2016-07-29 15:25:42', NULL),
(9, '', '', '-', 'KUNJUNGI WEBSITE PERUSAHAAN UNTUK INFORMASI LEBIH BANYAK', NULL, 'home', 'divider3', '2016-08-01 04:17:47', NULL),
(10, '', '', '-', 'MARI BERGABUNG DENGAN KOMUNITAS GOKART KAMI !', NULL, 'home', 'divider4', '2016-08-01 04:18:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kart`
--

CREATE TABLE `kart` (
  `kart_id` int(11) NOT NULL,
  `kart_img` varchar(100) DEFAULT NULL,
  `kart_name` varchar(100) NOT NULL,
  `kart_spec` text NOT NULL,
  `kart_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kart`
--

INSERT INTO `kart` (`kart_id`, `kart_img`, `kart_name`, `kart_spec`, `kart_date`) VALUES
(1, 'kart_29072016075856.jpg', 'Gokart Double', 'Driver Adult child passenger;head rest & safety belt;Iron body;350CC;4 strokes, single cylinder;8,4 hp/3600 rpm; 60 km/h', '2016-07-29 07:58:56'),
(2, 'kart_29072016080140.jpg', 'Gokart Talon', 'Driver Adult child passenger ;head rest & safety belt;Iron body;200CC;4 strokes, single cylinder;8,4 hp/3600 rpm; 60 km/h', '2016-07-29 08:01:40'),
(3, 'kart_29072016081001.jpg', 'Gokart Eagle', 'Driver Adult child passenger ;head rest & safety belt;Iron body;350CC;4 strokes, single cylinder;8,4 hp/3600 rpm; 60 km/h', '2016-07-29 08:10:01'),
(4, 'kart_29072016081759.jpg', 'Gokart Pro', 'Driver Adult child passenger ;head rest & safety belt;Iron body;350CC;4 strokes, single cylinder;8,4 hp/3600 rpm; 60 km/h', '2016-07-29 08:17:59');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `media_id` int(11) NOT NULL,
  `media_title_en` varchar(100) NOT NULL,
  `media_title_in` varchar(100) NOT NULL,
  `media_content_en` text NOT NULL,
  `media_content_in` text NOT NULL,
  `media_url` varchar(100) NOT NULL,
  `media_page` varchar(100) NOT NULL,
  `media_section` varchar(100) NOT NULL,
  `media_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `temp_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`media_id`, `media_title_en`, `media_title_in`, `media_content_en`, `media_content_in`, `media_url`, `media_page`, `media_section`, `media_date`, `temp_id`) VALUES
(2, '', '', '', '', 'video_poster_28072016113329.jpg', 'home', 'video_poster', '2016-07-28 11:33:30', 0),
(5, '', '', '', '', 'video_28072016120031.mp4', 'home', 'video', '2016-07-28 12:00:31', 0),
(6, '', '', '460m', '', 'track_29072016094116.jpg', 'home', 'track_img', '2016-07-29 09:41:17', NULL),
(7, '', '', '420m', '', 'track_29072016094331.jpg', 'home', 'track_img', '2016-07-29 09:43:31', NULL),
(8, '', '', '442m', '', 'track_29072016094226.jpg', 'home', 'track_img', '2016-07-29 09:42:27', NULL),
(9, '', '', '402m', '', 'track_29072016094243.jpg', 'home', 'track_img', '2016-07-29 09:42:43', NULL),
(17, '', '', '', '', 'img_01082016094756.jpg', 'home', 'gallery', '2016-08-01 09:47:57', NULL),
(18, '', '', '', '', 'img_01082016094808.jpg', 'home', 'gallery', '2016-08-01 09:48:09', NULL),
(19, '', '', '<p>A cup of coffee</p>', '<p>Secangkir kopi</p>', 'img_01082016103526.jpg', 'home', 'gallery', '2016-08-01 10:35:51', NULL),
(20, '', '', '', '', 'img_01082016095212.jpg', 'home', 'gallery', '2016-08-01 09:52:13', NULL),
(21, '', '', '', '', 'img_01082016112744.jpg', 'home', 'gallery', '2016-08-01 11:27:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `profile_id` int(11) NOT NULL,
  `profile_photo` varchar(50) NOT NULL,
  `profile_phone` varchar(12) NOT NULL,
  `profile_pin_bb` varchar(10) NOT NULL,
  `profile_address` text NOT NULL,
  `profile_postcode` varchar(5) NOT NULL,
  `profile_city` varchar(50) NOT NULL,
  `temp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`profile_id`, `profile_photo`, `profile_phone`, `profile_pin_bb`, `profile_address`, `profile_postcode`, `profile_city`, `temp_id`) VALUES
(13, 'pilot_28072016050648.jpg', '1234567890', 'AB1234CD', 'Jl. Mangkuyudan No. 9', '54321', 'Yogyakarta', 16),
(14, 'pilot_02082016092017.jpg', '345345345', 'AB1234CD', 'Jl. Mangkuyudan No. 9', '55739', 'Sleman', 17),
(15, 'pilot_03082016052957.jpg', '1234567890', 'AB1234CD', 'Jl. Mangkuyudan No. 9', '55739', 'Kulon Progo', 18),
(16, 'pilot_03082016053044.jpg', '1234567890', 'AB1234CD', 'Jl. Mangkuyudan No. 9', '55739', 'Gunung Kidul', 19),
(17, 'pilot_03082016053128.jpg', '1234567890', 'AB1234CD', 'Jl. Mangkuyudan No. 9', '55739', 'Bantul', 20);

-- --------------------------------------------------------

--
-- Table structure for table `race`
--

CREATE TABLE `race` (
  `race_id` int(11) NOT NULL,
  `race_pilot_id` int(11) NOT NULL,
  `race_time` int(11) NOT NULL,
  `race_best_lap` int(11) NOT NULL,
  `race_type` varchar(15) NOT NULL,
  `race_date` datetime NOT NULL,
  `race_info` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `race`
--

INSERT INTO `race` (`race_id`, `race_pilot_id`, `race_time`, `race_best_lap`, `race_type`, `race_date`, `race_info`) VALUES
(13, 16, 83000, 23000, 'pro', '2016-08-03 00:00:00', ''),
(14, 17, 84000, 24000, 'pro', '2016-08-03 00:00:00', ''),
(15, 18, 85000, 25000, 'pro', '2016-08-03 00:00:00', ''),
(16, 16, 83000, 23000, 'casual', '2016-08-04 00:00:00', ''),
(17, 17, 84000, 24000, 'casual', '2016-08-04 00:00:00', ''),
(18, 18, 85000, 25000, 'casual', '2016-08-04 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `statistic`
--

CREATE TABLE `statistic` (
  `stat_id` int(11) NOT NULL,
  `stat_win` int(11) NOT NULL DEFAULT '0',
  `temp_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statistic`
--

INSERT INTO `statistic` (`stat_id`, `stat_win`, `temp_id`) VALUES
(1, 2, 16);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `user_pass` varchar(40) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_full_name` varchar(50) NOT NULL,
  `user_level` enum('1','2','3','') NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_pass`, `user_email`, `user_full_name`, `user_level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'info@kidsfun.co.id', 'Admin Yogya Gokart', '1'),
(2, 'operator', '4b583376b2767b923c3e1da60d10de59', 'info@kidsfun.co.id', 'Operator', '2'),
(16, 'CeriseIT', '827ccb0eea8a706c4c34a16891f84e7b', 'cerise@gmail.com', 'CeriseIT', '3'),
(17, 'Clasher', '827ccb0eea8a706c4c34a16891f84e7b', 'clasher@gmail.com', 'Clasher', '3'),
(18, 'DarkRacer', '827ccb0eea8a706c4c34a16891f84e7b', 'darkracer@gmail.com', 'DarkRacer', '3'),
(19, 'DoomHammer', '827ccb0eea8a706c4c34a16891f84e7b', 'dhammer@gmail.com', 'DoomHammer', '3'),
(20, 'Exkalibur', '827ccb0eea8a706c4c34a16891f84e7b', 'exkalibur@gmail.com', 'Exkalibur', '3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `championship`
--
ALTER TABLE `championship`
  ADD PRIMARY KEY (`champ_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`general_id`);

--
-- Indexes for table `kart`
--
ALTER TABLE `kart`
  ADD PRIMARY KEY (`kart_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`media_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`profile_id`);

--
-- Indexes for table `race`
--
ALTER TABLE `race`
  ADD PRIMARY KEY (`race_id`);

--
-- Indexes for table `statistic`
--
ALTER TABLE `statistic`
  ADD PRIMARY KEY (`stat_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `championship`
--
ALTER TABLE `championship`
  MODIFY `champ_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
  MODIFY `general_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kart`
--
ALTER TABLE `kart`
  MODIFY `kart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `media_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `profile_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `race`
--
ALTER TABLE `race`
  MODIFY `race_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `statistic`
--
ALTER TABLE `statistic`
  MODIFY `stat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
